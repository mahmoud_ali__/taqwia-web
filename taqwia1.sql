-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 04, 2018 at 07:14 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taqwia1`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(2, 'مواد دراسية', '2018-09-25 03:52:30', '2018-09-25 03:52:30'),
(3, 'مواد علميه', '2018-09-25 03:52:38', '2018-09-25 03:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `certificate_id` int(11) NOT NULL,
  `certificate_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`certificate_id`, `certificate_name`) VALUES
(2, 'Marketing Programs'),
(3, 'Data Science'),
(4, 'Leadership'),
(5, 'Project Management'),
(6, 'Human Resources'),
(7, 'Finance And Business'),
(8, 'Healthcare'),
(9, 'Hospitality'),
(10, 'Nutrition'),
(11, 'Real Estate'),
(12, 'Engineering'),
(13, 'Partner'),
(14, 'Beekeeping'),
(15, 'Veterinary'),
(16, 'SSL');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'yousef', 'yousef.ahmed.mohammed5030@gmail.com', 'sasasas', 'asasasasaafa fefewf ewf e', '2018-09-23 19:14:21', '2018-09-23 19:14:21'),
(2, 'Yousef Ahmed', 'yousef.ahmed.mohammed5030@gmail.com', 'dewdwe', 'dewdewdwe', '2018-09-23 19:15:25', '2018-09-23 19:15:25'),
(3, 'ew', 'yousef.ahmed.mohammed5030@gmail.com', 'ew', 'ewew', '2018-09-23 19:15:53', '2018-09-23 19:15:53'),
(4, 'fewfew', 'yousef.ahmed.mohammed5030@gmail.com', 'fewf', 'ewfewfw', '2018-09-23 19:18:20', '2018-09-23 19:18:20'),
(5, 'dwedwed', 'yousef.ahmed.mohammed5030@gmail.com', 'dewdwe', 'dewdewd', '2018-09-23 19:18:38', '2018-09-23 19:18:38'),
(6, 'Yousef Ahmed', 'admin@admin.com', 'dede', 'dedede', '2018-09-23 19:19:37', '2018-09-23 19:19:37'),
(7, 'ddd', 'test@test.com', 'sdsdsd', 'sdsds', '2018-09-23 19:19:52', '2018-09-23 19:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People\'s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'SS', 'South Sudan'),
(203, 'ES', 'Spain'),
(204, 'LK', 'Sri Lanka'),
(205, 'SH', 'St. Helena'),
(206, 'PM', 'St. Pierre and Miquelon'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syrian Arab Republic'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania, United Republic of'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad and Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks and Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States minor outlying islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (U.S.)'),
(241, 'WF', 'Wallis and Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `experience_certificate_image`
--

CREATE TABLE `experience_certificate_image` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experience_certificate_image`
--

INSERT INTO `experience_certificate_image` (`user_id`, `image`) VALUES
('24', '092620182032225babecd6ecd3f.jpg'),
('24', '092620182032225babecd6ed723.jpg'),
('26', '092620182100525babf3843ecd1.jpg'),
('26', '092620182100525babf3843f5a5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'We\'re ready to start now', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei\r\n                                temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim exercitation\r\n                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute dolor in\r\n                                reprehenderit in voluptate velit esse cillum dolore', '2018-09-22 23:54:20', '2018-09-23 00:00:24'),
(2, 'Sedeiusmod tempor inccsetetur ?', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei\r\n                                temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim exercitation\r\n                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute dolor in\r\n                                reprehenderit in voluptate velit esse cillum dolore', '2018-09-22 23:54:43', '2018-09-22 23:54:43'),
(3, 'Sedeiusmod inccsetetur aliquatraiy?', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei\r\n                                temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim exercitation\r\n                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute dolor in\r\n                                reprehenderit in voluptate velit esse cillum dolore', '2018-09-22 23:55:11', '2018-09-22 23:55:11'),
(5, 'Tempor inccsetetur aliquatraiy?', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei\r\n                                temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim exercitation\r\n                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute dolor in\r\n                                reprehenderit in voluptate velit esse cillum dolore', '2018-09-23 00:00:49', '2018-09-23 00:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `landpage`
--

CREATE TABLE `landpage` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_description` text COLLATE utf8mb4_unicode_ci,
  `google_store_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apple_store_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_background` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description1` text COLLATE utf8mb4_unicode_ci,
  `about_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description2` text COLLATE utf8mb4_unicode_ci,
  `about_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description3` text COLLATE utf8mb4_unicode_ci,
  `about_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `screenshot_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `screenshot_description` text COLLATE utf8mb4_unicode_ci,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` text COLLATE utf8mb4_unicode_ci,
  `total_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loyal_customer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_achivement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_ratting` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci,
  `faq_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_description` text COLLATE utf8mb4_unicode_ci,
  `download_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landpage`
--

INSERT INTO `landpage` (`id`, `home_title`, `home_description`, `google_store_link`, `apple_store_link`, `home_background`, `awesome_feature_title`, `awesome_feature_description`, `awesome_feature_title1`, `awesome_feature_description1`, `awesome_feature_image1`, `awesome_feature_title2`, `awesome_feature_description2`, `awesome_feature_image2`, `awesome_feature_title3`, `awesome_feature_description3`, `awesome_feature_image3`, `awesome_feature_title4`, `awesome_feature_description4`, `awesome_feature_image4`, `awesome_feature_title5`, `awesome_feature_description5`, `awesome_feature_image5`, `awesome_feature_title6`, `awesome_feature_description6`, `awesome_feature_image6`, `feature_title1`, `feature_description1`, `feature_title2`, `feature_description2`, `feature_title3`, `feature_description3`, `feature_image1`, `feature_image2`, `feature_image3`, `about_title`, `about_description`, `about_title1`, `about_description1`, `about_image1`, `about_title2`, `about_description2`, `about_image2`, `about_title3`, `about_description3`, `about_image3`, `awesome_feature_video`, `screenshot_title`, `screenshot_description`, `team_title`, `team_description`, `total_user`, `loyal_customer`, `total_achivement`, `app_ratting`, `testimonial_title`, `testimonial_description`, `faq_title`, `faq_description`, `download_title`, `download_description`) VALUES
(1, 'It’s all about Promote your Business', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.', 'https://play.google.com', 'https://itunes.apple.com', '092020181959585ba3fc3ec92d5.jpg', 'Awesome Features', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.', 'High Resolution', 'Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!', '092020181957235ba3fba3bccec.svg', 'Creative Design', 'Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!', '092020181957235ba3fba3bd18d.svg', 'Pixel Parfect', 'Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!', '092020181957235ba3fba3bd3ee.svg', 'Clean Codes', 'Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!', '092020181957235ba3fba3bd66c.svg', 'Ratina Ready', 'Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!', '092020181957235ba3fba3bd8ce.svg', 'Full Chat Free', 'Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!', '092020181957235ba3fba3bdb01.svg', 'Creative Design', 'Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt !', '2 Month Free Trail', 'Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt !', '24/7 Hour Support', 'Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt !', '092020182008005ba3fe20a8e08.svg', '092020182008005ba3fe20a9028.svg', '092020182008005ba3fe20a929e.svg', 'How Appton Work?', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.', 'Best User Interface Design', 'Lorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor sereit amet, consectetur\r\n\r\nLorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor.', '092020182010305ba3feb6c49be.png', 'Best User Interface Design', 'Lorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor sereit amet, consectetur\r\n\r\nLorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor.', '092020182010305ba3feb6c4dd5.png', 'Data Analysis', 'Lorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor sereit amet, consectetur\r\n\r\nLorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor.', '092020182010305ba3feb6c5112.png', 'LTEfxsWdZzQ', 'App Screenshot', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.', 'Team Member', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.', '1350', '1300', '1150', '1000', 'Testimonials', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.', 'Frequently Asked Questions', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.', 'Download Appton Today', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.');

-- --------------------------------------------------------

--
-- Table structure for table `lectures`
--

CREATE TABLE `lectures` (
  `lecture_id` int(10) UNSIGNED NOT NULL,
  `lecture_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lecture_hour` int(11) DEFAULT NULL,
  `lecture_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_category_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lectures`
--

INSERT INTO `lectures` (`lecture_id`, `lecture_title`, `lecture_hour`, `lecture_description`, `created_at`, `updated_at`, `user_id`, `category_id`, `sub_category_id`) VALUES
(1, 'How To Learn Php', 100, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2018-10-02 04:16:38', '2018-10-02 04:43:07', '24', '3', '3');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_20_130831_landpagehome', 1),
(4, '2018_09_20_183207_landpageawesomefeature', 1),
(5, '2018_09_20_183207_landpagefeature', 1),
(6, '2018_09_20_184450_landpagefeature1', 1),
(7, '2018_09_20_190126_landpageabout', 1),
(8, '2018_09_20_201753_awesomefeature1', 2),
(9, '2018_09_20_202611_landpage2', 3),
(10, '2018_09_20_204034_screenshot', 3),
(11, '2018_09_20_213501_team', 4),
(12, '2018_09_20_221646_landpage3', 5),
(13, '2018_09_21_155054_create_testimonials_table', 6),
(15, '2018_09_22_150159_ss', 7),
(17, '2018_09_22_155507_create_faqs_table', 8),
(18, '2018_09_22_170827_create_subscribes_table', 9),
(19, '2018_09_23_120848_create_contacts_table', 10),
(21, '2018_09_23_123532_create_settings_table', 11),
(22, '2018_09_24_163456_addnewfieldtouser', 12),
(26, '2018_09_24_203934_create_categories_table', 13),
(27, '2018_09_24_210401_subcategory', 14),
(30, '2018_09_26_222004_create_packages_table', 15),
(31, '2018_09_29_211638_acc_type', 16),
(32, '2018_10_01_163119_create_lectures_table', 17),
(33, '2018_10_01_221218_terms', 18);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `package_id` int(10) UNSIGNED NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`package_id`, `package_name`, `package_hours`, `package_price`, `created_at`, `updated_at`) VALUES
(1, 'Main Package', '10', '100', '2018-09-27 06:09:35', '2018-09-27 06:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `previous_work`
--

CREATE TABLE `previous_work` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jobTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `previous_work`
--

INSERT INTO `previous_work` (`user_id`, `place`, `from_year`, `to_year`, `jobTitle`) VALUES
('24', 'place 1', '2015', '2017', 'rgegre'),
('26', 'place 1', '2015', '2017', 'sadadadsa');

-- --------------------------------------------------------

--
-- Table structure for table `scientific_certificate_image`
--

CREATE TABLE `scientific_certificate_image` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scientific_certificate_image`
--

INSERT INTO `scientific_certificate_image` (`user_id`, `image`) VALUES
('24', '092620182032225babecd6d925e.jpg'),
('24', '092620182032225babecd6d97f6.jpg'),
('26', '092620182100525babf384164c2.jpg'),
('26', '092620182100525babf3841716b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `screenshots`
--

CREATE TABLE `screenshots` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `screenshots`
--

INSERT INTO `screenshots` (`id`, `image`) VALUES
(9, '092020182116435ba40e3b76398.jpg'),
(10, '092020182116485ba40e400faf9.jpg'),
(11, '092020182116525ba40e4486c0e.jpg'),
(12, '092020182116575ba40e49bfb47.jpg'),
(13, '092020182117025ba40e4e90c18.jpg'),
(14, '092020182117075ba40e5333570.jpg'),
(15, '092020182117135ba40e599a813.jpg'),
(16, '092020182117215ba40e61292bc.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `phone`, `address`, `email`, `facebook`, `twitter`, `linkedin`, `pinterest`, `google`, `created_at`, `updated_at`, `terms`) VALUES
(1, '+0044 545 989 62698', '28 Green Tower, Street Name New York City, USA', 'yourmail@gmail.com', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.linkedin.com', 'https://www.pinterest.com/', 'https://www.google.com/', NULL, '2018-10-02 05:16:06', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'email@example.com', '2018-09-23 18:54:50', '2018-09-23 18:54:50');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `sub_category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `sub_category_name`, `category_id`) VALUES
(3, 'Php', '3'),
(4, 'Math', '2'),
(5, 'C ++', '3'),
(6, 'Java', '3'),
(7, 'Object Oriented', '3');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `image`, `name`, `job`, `facebook`, `twitter`, `pinterest`, `linkedin`) VALUES
(2, '092020182205565ba419c420cb7.jpg', 'Jonshon Adams', 'Asst Designer', 'https://www.facebook.com', 'https://twitter.com', 'https://www.pinterest.com', 'https://www.linkedin.com'),
(3, '092020182200115ba4186b7111a.jpg', 'Jonshon Adams', 'Asst Designer', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.pinterest.com/', 'https://www.linkedin.com'),
(4, '092020182200455ba4188dd0c12.jpg', 'Jonshon Adams', 'Asst Designer', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.pinterest.com/', 'https://www.linkedin.com'),
(5, '092020182201045ba418a063886.jpg', 'Jonshon Adams', 'Asst Designer', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.pinterest.com/', 'https://www.linkedin.com');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `author`, `image`, `link_text`, `link`, `description`, `created_at`, `updated_at`) VALUES
(4, 'Michael Clarke', '092220181454255ba657a1aed0d.jpg', 'themeforest.com', '#', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli\r\n                                incidit labore Lorem ipsum amet madolor sit amet Lorem ipsum madolor sit amet,\r\n                                consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet\r\n                                madolor sit amet', '2018-09-22 21:54:25', '2018-09-22 21:54:25'),
(5, 'Michael Clarke', '092220181454585ba657c278637.jpg', 'themeforest.com', '#', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli\r\n                                incidit labore Lorem ipsum amet madolor sit amet Lorem ipsum madolor sit amet,\r\n                                consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet\r\n                                madolor sit amet', '2018-09-22 21:54:58', '2018-09-22 21:54:58'),
(6, 'Michael Clarke', '092220181455255ba657dd2cfbd.jpg', 'themeforest.com', '#', 'Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli\r\n                                incidit labore Lorem ipsum amet madolor sit amet Lorem ipsum madolor sit amet,\r\n                                consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet\r\n                                madolor sit amet', '2018-09-22 21:55:25', '2018-09-22 21:55:25'),
(7, NULL, NULL, NULL, NULL, NULL, '2018-09-22 23:53:11', '2018-09-22 23:53:11');

-- --------------------------------------------------------

--
-- Table structure for table `training_course`
--

CREATE TABLE `training_course` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appreciation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_course`
--

INSERT INTO `training_course` (`user_id`, `name`, `place`, `hours`, `appreciation`) VALUES
('24', 'course 1', 'course place 1', 'hour 1', 'COURSE APPRECIATION 1'),
('26', 'course 1', 'course place 1', 'hour 1', 'COURSE APPRECIATION 1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1 : Admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `open` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `religion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `handGraduation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'جهة التخرج',
  `graduationYear` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generalAppreciation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'التقدير العام',
  `jobTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'جهة العمل',
  `personalImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accountType` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 : Student ; 2 : Parent',
  `perHOur` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`, `open`, `phone`, `birthDate`, `religion`, `handGraduation`, `graduationYear`, `generalAppreciation`, `jobTitle`, `employer`, `personalImage`, `country_id`, `nationality_id`, `accountType`, `perHOur`, `salary`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$VCb6XYrSi5xZ0i2.YEDHIegbAR3JyBfY/zr5IuAV2j5cm8ibfDUjW', '1', 'W9xoqWLSFhIOKqeydrylkkI9l574goB7JGK2LNQOsIuSRgNTIw3KztfpHvh4', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Yousef Ahmed', 'yousef.ahmed.mohammed5030@gmail.com', '$2y$10$vRV1SCB4abhr8MXDrYKnoeeOjlMOxXlLz4t3x/yBJZ3MbRA1MzRQi', '2', NULL, '2018-09-27 03:32:22', '2018-10-02 11:53:18', '1', '01273975420', '2018-09-25', 'wwwww', 'sdsd', '2016', 'ddddddd', 'cd', 'dddd', '092620182032225babecd6bdb04.jpg', '3', '14', NULL, '55', NULL),
(26, 'Aya Ismail', 'aya@admin.com', '$2y$10$pwlcdMAyjQOLBxOA0D//wua3.r81j2zIYc.uR41gA3KOBWAi1UHcO', '2', NULL, '2018-09-27 04:00:51', '2018-10-02 11:54:12', '1', '01273975420', '2018-09-25', 'wwwww', 'HAND GRADUATION', '2016', 'GENERAL APPRECIATION', 'Developer', 'ssssdeed', '092620182213305bac048ac1b34.jpg', '36', '62', NULL, '22', NULL),
(31, 'yousef', 'parent@email.com', '$2y$10$.wagdTIvReZuR8itTz/QA.98YTJZzf/.ttseM0IULS8XX.7AJxjlK', '3', NULL, '2018-09-30 05:42:53', '2018-10-02 01:15:54', '1', '122589562', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '4', '2', NULL, NULL),
(32, 'Ismail dd', 'dd@yahoo.com', '$2y$10$QSuaD6q8Qldzq068mccT4.xSGgzYFrD.x5V/GOzj/fnMmh6az2fDW', '3', NULL, '2018-10-02 12:39:53', '2018-10-02 14:02:24', '0', '1222252355', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4', '13', '1', NULL, NULL),
(35, 'hddjfj', 'bxhd@fjnc.d', '$2y$10$rWcraYHdKI52Wof9JX3xw.BkiSwrtsAKVnYkmaJLyYpSvAEr6y4EW', '3', NULL, '2018-10-03 05:29:22', '2018-10-03 05:29:22', '1', '64648484', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', '5', '1', NULL, NULL),
(36, 'ahmed kamal', 'ahmed.kamal775@gmail.com', '$2y$10$bpHTrjpLKB4yhNmpFwmMFu.PO6th6ysU.FVh2GF9YgIdCXQUpXzsu', '3', NULL, '2018-10-03 17:33:34', '2018-10-03 17:33:34', '1', '201016562195', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', '3', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_certificate`
--

CREATE TABLE `user_certificate` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_certificate`
--

INSERT INTO `user_certificate` (`user_id`, `certificate_id`) VALUES
('24', '2'),
('26', '14');

-- --------------------------------------------------------

--
-- Table structure for table `user_country`
--

CREATE TABLE `user_country` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_country`
--

INSERT INTO `user_country` (`user_id`, `country_id`) VALUES
('11', '2'),
('12', '62');

-- --------------------------------------------------------

--
-- Table structure for table `user_nationality`
--

CREATE TABLE `user_nationality` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_nationality`
--

INSERT INTO `user_nationality` (`user_id`, `country_id`) VALUES
('11', '2'),
('12', '62');

-- --------------------------------------------------------

--
-- Table structure for table `user_package`
--

CREATE TABLE `user_package` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
  ADD KEY `certificate_id` (`certificate_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landpage`
--
ALTER TABLE `landpage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lectures`
--
ALTER TABLE `lectures`
  ADD PRIMARY KEY (`lecture_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `screenshots`
--
ALTER TABLE `screenshots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
  MODIFY `certificate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `landpage`
--
ALTER TABLE `landpage`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lectures`
--
ALTER TABLE `lectures`
  MODIFY `lecture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `screenshots`
--
ALTER TABLE `screenshots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
