$Ajax = function (method, oVals) {
    oVals = JSON.stringify(oVals);
    $.ajax({
        type: "POST",
        url: base_url + '/ajax',
        data: {method: method, oVals: oVals, _token: _token},
        cache: false,
        success: function (html) {
            $('body').append(html);
        }
    });
}

$('#category').change(function () {
    var oVals = {};
    oVals['id'] = $(this).val();
    $Ajax('getSubCategory', oVals);
});