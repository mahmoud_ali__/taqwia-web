<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets') }}/plugins/images/favicon.png">
    <title>@yield('title')</title>
    <link href="{{ asset('assets') }}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/animate.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/style.css" rel="stylesheet">
    <!-- Wizard CSS -->

    <link href="{{ asset('assets') }}/plugins/bower_components/register-steps/steps.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/colors/default.css" id="theme" rel="stylesheet">
    <link id="bsdp-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet">
    @yield('style')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

@yield('content')

<div class="previous_work" style="display: none">
    <div class="current-row">
        <div class="col-md-4">
            <label>Place Name</label>
            <input class="form-control" type="text" name="previous_place[]"
                   required=""
                   placeholder="Place Name">
        </div>

        <div class="col-md-2">
            <label>From Year</label>
            <input class="form-control" type="text" name="from_year[]"
                   required=""
                   placeholder="From Year">
        </div>

        <div class="col-md-2">
            <label>To Year</label>
            <input class="form-control" type="text" name="to_year[]"
                   required=""
                   placeholder="To Year">
        </div>

        <div class="col-md-3">
            <label>job Title</label>
            <input class="form-control" type="text" name="previous_jobTitle[]"
                   required=""
                   placeholder="job Title">
        </div>

        {{--<div class="col-md-1">--}}
            {{--<label>Remove</label>--}}
            {{--<a href="" class="remove">--}}
                {{--<i class="fa fa-remove fa-3x"></i>--}}
            {{--</a>--}}
        {{--</div>--}}
    </div>

    <div class="current-row-course">
        <div class="col-md-3">
            <label>Course Name</label>
            <input class="form-control" type="text" name="course_name[]"
                   required="" placeholder="Course Name">
        </div>

        <div class="col-md-3">
            <label>Course Place</label>
            <input class="form-control" type="text" name="course_place[]"
                   required="" placeholder="Course Place">
        </div>

        <div class="col-md-3">
            <label>Course Hour</label>
            <input class="form-control" type="text" name="course_hour[]"
                   required=""
                   placeholder="Course Hour">
        </div>

        <div class="col-md-2">
            <label>Course appreciation</label>
            <input class="form-control" type="text" name="course_appreciation[]"
                   required=""
                   placeholder="Course appreciation">
        </div>

        {{--<div class="col-md-1">--}}
            {{--<label>Remove</label>--}}
            {{--<a href="" class="remove">--}}
                {{--<i class="fa fa-remove fa-3x"></i>--}}
            {{--</a>--}}
        {{--</div>--}}
    </div>

</div>

<script src="{{ asset('assets') }}/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="{{ asset('assets') }}/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.slimscroll.js"></script>
<script src="{{ asset('assets') }}/js/waves.js"></script>
<script src="{{ asset('assets') }}/js/custom.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/register-steps/jquery.easing.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/register-steps/register-init.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
    });
</script>
@yield('script')
</body>
</html>
