<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets') }}/plugins/images/favicon.png">
    <title> @yield('title') </title>
    <link href="{{ asset('assets') }}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"
          rel="stylesheet">
    @yield('style')
    <link href="{{ asset('assets') }}/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet"/>
    <link href="{{ asset('assets') }}/css/animate.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/style.css" rel="stylesheet">
    <link href="{{ asset('assets') }}/css/colors/megna-dark.css" id="theme" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        var base_url = '{{ url('') }}';
        var _token = '{{ csrf_token() }}';
    </script>
</head>

<body class="fix-header">

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>

<div id="wrapper">

    @include('partial.navbar')
    @include('partial.sidebar')

    <div id="page-wrapper">
        <div class="container-fluid">
            @yield('content')
            @include('partial.right-sidebar')
        </div>
        <footer class="footer text-center"> 2018 &copy; Taqwia Admin brought to you by themedesigner.in</footer>
    </div>
</div>

<script src="{{ asset('assets') }}/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="{{ asset('assets') }}/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.slimscroll.js"></script>
<script src="{{ asset('assets') }}/js/waves.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/raphael/raphael-min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/morrisjs/morris.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/chartist-js/dist/chartist.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/moment/moment.js"></script>
<script src='{{ asset('assets') }}/plugins/bower_components/calendar/dist/fullcalendar.min.js'></script>
<script src="{{ asset('assets') }}/plugins/bower_components/calendar/dist/cal-init.js"></script>
<script src="{{ asset('assets') }}/js/custom.min.js"></script>
<script src="{{ asset('assets') }}/js/dashboard1.js"></script>
<script src="{{ asset('assets') }}/js/cbpFWTabs.js"></script>
<script type="text/javascript">
    (function () {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function (el) {
            new CBPFWTabs(el);
        });
    })();
</script>
<script src="{{ asset('assets') }}/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<script src="{{ asset('assets') }}/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

@stack('scripts')

</body>
</html>
