<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->
<script>
    var base_url = '{{ url('') }}';
</script>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="APPTON HTML5 Template is a simple Smooth Personal App Landing Template"/>
    <meta name="keywords" content="App, Landing, Business, Onepage, Html, Business"/>
    <title>@lang('landpage.taqwia_app')</title>

    <link rel="shortcut icon" type="image/ico" href="{{ asset('landpage/assest') }}/img/favicon.png"/>
    <link href="{{ asset('landpage/assest') }}/css/plugins.css" rel="stylesheet">
    <link href="{{ asset('landpage/assest') }}/css/theme.css" rel="stylesheet">
    <link href="{{ asset('landpage/assest') }}/css/icons.css" rel="stylesheet">
    <link href="{{ asset('landpage') }}/style.css" rel="stylesheet">
    @if(session()->get('user_lang')=='ar')
      <link rel="stylesheet" type="text/css" href="{{asset('landpage/assest/')}}/rtl.css">
    @endif
    <link href="{{ asset('landpage/assest') }}/css/responsive.css" rel="stylesheet">
    <script src="{{ asset('landpage/assest') }}/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="home-particle" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="preeloader">
    <div class="preloader-spinner"></div>
</div>

<!--SCROLL TO TOP-->
<a href="#scroolup" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

<!--START TOP AREA-->
<header class="top-area" id="home">
    <div class="area-bg"
         style="background: rgba(0, 0, 0, 0) url('{{ asset('upload/'.$data->home_background) }}') no-repeat scroll center center / cover"></div>
    <div class="header-top-area" id="scroolup">
        <!--MAINMENU AREA-->
        <div class="mainmenu-area" id="mainmenu-area">
            <div class="mainmenu-area-bg"></div>
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-header">
                        <a href="#home" class="navbar-brand"><img src="{{ asset('assets') }}/Taqwaia-mov.png" alt="logo"
                                                                  style="height: 55px;"></a>
                    </div>
                    <div id="main-nav" class="stellarnav">
                        <ul id="nav" class="nav navbar-nav pull-right">
                            <li class="active"><a href="#home">@lang('landpage.home')</a>
                                <!--  <ul>
                                     <li><a href="index-shadow.html">Home Effective</a></li>
                                     <li><a href="index.html">Home Default 1</a></li>
                                     <li><a href="index-2.html">Home Default 2</a></li>
                                     <li><a href="index-gradient.html">Home Gradient</a></li>
                                     <li><a href="index-video.html">Home Video</a></li>
                                     <li><a href="index-particle.html">Home Particle</a></li>
                                     <li><a href="index-graph.html">Home Graph</a></li>
                                     <li><a href="index-ribbon.html">Home Ribbon</a></li>
                                 </ul> -->
                            </li>
                            <li><a href="#features">@lang('landpage.features')</a></li>
                            <li><a href="#about">@lang('landpage.about')</a></li>
                            <li><a href="#screenshot">@lang('landpage.screenshot')</a></li>
                            <li><a href="#team">@lang('landpage.team')</a></li>
                            <li><a href="#pricing">@lang('landpage.pricing')</a></li>
                            <li><a href="#faqs">@lang('landpage.faqs')</a></li>
                            {{--<li><a href="#news">Blog</a></li>--}}
                            <li><a href="#contact">@lang('admin.contact')</a></li>
                            @if(session()->get('user_lang')=='ar')
                                <li><a href="{{ url('userLang/en') }}">@lang('admin.english')</a></li>
                            @else
                                <li><a href="{{ url('userLang/ar') }}">@lang('admin.arabic')</a></li>
                            @endif
                            <li><a class="btn-login" href="{{url('login')}}"><p>@lang('login.login')</p></a></li>
                            <li><a class="btn-login" href="{{url('register')}}"><p>@lang('login.register')</p></a></li>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <!--END MAINMENU AREA END-->
    </div>


    <div class="welcome-text-area white">
        <div id="particle-canvas"></div>
        <div class="welcome-area">
            <div class="container">
                <div class="row flex-v-center">
                    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                        <div class="welcome-text">
                            <h1 class="wow fadeInUp" data-wow-delay="0.1s">
                                @if(session()->get('user_lang')=='ar')
                                    {{ $data->home_title }}
                                @else
                                    {{ $data->home_title_english }}
                                @endif
                            </h1>
                            <p class="wow fadeInUp" data-wow-delay="0.2s">
                                @if(session()->get('user_lang')=='ar')
                                    {{ $data->home_description }}
                                @else
                                    {{ $data->home_description_english }}
                                @endif
                            </p>
                            <div class="download-button mt60 xs-mt40 wow fadeInUp" data-wow-delay="0.3s">

                                <a class="left" href="{{ $data->google_store_link }}">
                                    <i class="fa fa-android"></i>
                                    <div>
                                        <p class="font14">@lang('landpage.avilable_on')</p>
                                        <span class="font18 font700">@lang('landpage.google_store')</span>
                                    </div>
                                </a>

                                <a class="left active" href="{{ $data->apple_store_link }}">
                                    <i class="fa fa-apple"></i>
                                    <div>
                                        <p class="font14">@lang('landpage.avilable_on')</p>
                                        <span class="font18 font700">@lang('landpage.apple_store')</span>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <div class="welcome-mockup right wow fadeInRight">
                            <img src="{{ asset('landpage/assest') }}/img/home/home-mockup.png" alt="">
                            <div class="home-mockup-slider">
                                <div class="single-mockups">
                                    <img src="{{ asset('landpage/assest') }}/img/screenshot/top-screen-1.png" alt="">
                                </div>
                                <div class="single-mockups">
                                    <img src="{{ asset('landpage/assest') }}/img/screenshot/top-screen-2.png" alt="">
                                </div>
                                <div class="single-mockups">
                                    <img src="{{ asset('landpage/assest') }}/img/screenshot/top-screen-3.png" alt="">
                                </div>
                                <div class="single-mockups">
                                    <img src="{{ asset('landpage/assest') }}/img/screenshot/top-screen-4.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--END TOP AREA-->

<!--FEATURES TOP AREA-->
<section class="features-top-area padding-100-30" id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box mb50 center wow fadeInUp padding20 box-hover-1" data-wow-delay="0.1s">
                    <div class="box-img-icon features-box-icon">
                        <img src="{{ asset('upload/'.$data->feature_image1) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->feature_title1  }}</h3>
                        <p>{{ $data->feature_description1 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->feature_title1_english  }}</h3>
                        <p>{{ $data->feature_description1_english }}</p>
                    @endif

                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 center  wow fadeInUp padding20 box-hover-1"
                     data-wow-delay="0.2s">
                    <div class="box-img-icon features-box-icon">
                        <img src="{{ asset('upload/'.$data->feature_image2) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->feature_title2  }}</h3>
                        <p>{{ $data->feature_description2 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->feature_title2_english  }}</h3>
                        <p>{{ $data->feature_description2_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 center wow fadeInUp padding20 box-hover-1"
                     data-wow-delay="0.2s">
                    <div class="box-img-icon features-box-icon">
                        <img src="{{ asset('upload/'.$data->feature_image3) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->feature_title3  }}</h3>
                        <p>{{ $data->feature_description3 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->feature_title3_english  }}</h3>
                        <p>{{ $data->feature_description3_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 center wow fadeInUp padding20 box-hover-1 visible-sm"
                     data-wow-delay="0.1s">
                    <div class="box-img-icon features-box-icon">
                        <img src="{{ asset('landpage/assest') }}/img/icon/quality.svg" alt="">
                    </div>
                    <h3 class="box-title">Quality Service</h3>
                    <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt
                        !</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FEATURES TOP AREA END-->

<!--ABOUT AREA-->
<section class="about-area sky-gray-bg section-padding" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2>{{ $data->about_title }}</h2>
                        <p>{!! $data->about_description !!}</p>
                    @else
                        <h2>{{ $data->about_title_english }}</h2>
                        <p>{!! $data->about_description_english !!}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row flex-v-center">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="about-content xs-mb50 xs-center">
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="mb30">{{ $data->about_title1 }}</h3>
                        <p>{!! nl2br($data->about_description1) !!}</p>
                    @else
                        <h3 class="mb30">{{ $data->about_title1_english }}</h3>
                        <p>{!! nl2br($data->about_description1_english) !!}</p>
                    @endif


                    {{--<a href="#" class="read-more mt30 inline-block">Read More</a>--}}
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="about-mockup right wow fadeIn">
                    <img src="{{ asset('upload/'.$data->about_image1) }}" alt="">
                </div>
            </div>
        </div>
        <div class="row flex-v-center padding-top">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="about-mockup wow fadeIn xs-mb50">
                    <img src="{{ asset('upload/'.$data->about_image2) }}" alt="">
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="about-content xs-center">
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="mb30">{{ $data->about_title2 }}</h3>
                        <p>{!! nl2br($data->about_description2) !!}</p>
                    @else
                        <h3 class="mb30">{{ $data->about_title2_english }}</h3>
                        <p>{!! nl2br($data->about_description2_english) !!}</p>
                    @endif

                    {{--<a href="#" class="read-more mt30 inline-block">Read More</a>--}}
                </div>
            </div>
        </div>
        <div class="row flex-v-center padding-top">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="about-content xs-center">
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="mb30">{{ $data->about_title3 }}</h3>
                        <p>{!! nl2br($data->about_description3) !!}</p>
                    @else
                        <h3 class="mb30">{{ $data->about_title3_english }}</h3>
                        <p>{!! nl2br($data->about_description3_english) !!}</p>
                    @endif
                    {{--<a href="#" class="read-more mt30 inline-block">Read More</a>--}}
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                <div class="about-mockup right wow fadeIn xs-mt50">
                    <img src="{{ asset('upload/'.$data->about_image3) }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!--ABOUT AREA END-->

<!--FEATURES AREA-->
<section class="features-area padding-100-50">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2>{{ $data->awesome_feature_title }}</h2>
                        <p>{{ $data->awesome_feature_description }}</p>
                    @else
                        <h2>{{ $data->awesome_feature_title_english }}</h2>
                        <p>{{ $data->awesome_feature_description_english }}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 wow fadeInUp padding20 box-hover-1" data-wow-delay="0.1s">
                    <div class="box-img-icon">
                        <img src="{{ asset('upload/'.$data->awesome_feature_image1) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->awesome_feature_title1  }}</h3>
                        <p>{{ $data->awesome_feature_description1 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->awesome_feature_title1_english  }}</h3>
                        <p>{{ $data->awesome_feature_description1_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 wow fadeInUp padding20 box-hover-1" data-wow-delay="0.2s">
                    <div class="box-img-icon">
                        <img src="{{ asset('upload/'.$data->awesome_feature_image2) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->awesome_feature_title2  }}</h3>
                        <p>{{ $data->awesome_feature_description2 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->awesome_feature_title2_english  }}</h3>
                        <p>{{ $data->awesome_feature_description2_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 wow fadeInUp padding20 box-hover-1" data-wow-delay="0.3s">
                    <div class="box-img-icon">
                        <img src="{{ asset('upload/'.$data->awesome_feature_image3) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->awesome_feature_title3  }}</h3>
                        <p>{{ $data->awesome_feature_description3 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->awesome_feature_title3_english  }}</h3>
                        <p>{{ $data->awesome_feature_description3_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 wow fadeInUp padding20 box-hover-1" data-wow-delay="0.1s">
                    <div class="box-img-icon">
                        <img src="{{ asset('upload/'.$data->awesome_feature_image4) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->awesome_feature_title4  }}</h3>
                        <p>{{ $data->awesome_feature_description4 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->awesome_feature_title4_english  }}</h3>
                        <p>{{ $data->awesome_feature_description4_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 wow fadeInUp padding20 box-hover-1" data-wow-delay="0.2s">
                    <div class="box-img-icon">
                        <img src="{{ asset('upload/'.$data->awesome_feature_image5) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->awesome_feature_title5  }}</h3>
                        <p>{{ $data->awesome_feature_description5 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->awesome_feature_title5_english  }}</h3>
                        <p>{{ $data->awesome_feature_description5_english }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                <div class="text-icon-box relative mb50 wow fadeInUp padding20 box-hover-1" data-wow-delay="0.3s">
                    <div class="box-img-icon">
                        <img src="{{ asset('upload/'.$data->awesome_feature_image6) }}" alt="">
                    </div>
                    @if(session()->get('user_lang')=='ar')
                        <h3 class="box-title">{{ $data->awesome_feature_title6  }}</h3>
                        <p>{{ $data->awesome_feature_description6 }}</p>
                    @else
                        <h3 class="box-title">{{ $data->awesome_feature_title6_english  }}</h3>
                        <p>{{ $data->awesome_feature_description6_english }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--FEATURES AREA END-->

<!--VIDEO PROMO AREA-->
<section class="video-area" id="video">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="video-promo-details white relative center wow fadeIn">
                    <div class="area-bg"></div>
                    <img src="{{ asset('landpage/assest') }}/img/video-bg.jpg" alt="">
                    <div class="video-promo-content">
                        <span data-video-id="{{ $data->awesome_feature_video }}" class="video-area-popup mb30"><i
                                    class="fl-esn-play-button-1"></i></span>
                        <h3 class="mb0">@lang('landpage.watch_video')</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--VIDEO PROMO AREA END-->

<!--SCREENSHOT AREA-->
<section class="screenshot-area section-padding" id="screenshot">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2><span>{{ $data->screenshot_title }}</span></h2>
                        <p>{!! nl2br($data->screenshot_description) !!}</p>
                    @else
                        <h2><span>{{ $data->screenshot_title_english }}</span></h2>
                        <p>{!! nl2br($data->screenshot_description_english) !!}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="screenshot-slider-area wow fadeIn">
                    <div class="screenshot-slider">
                        @foreach($screenshot as $value)
                            <div class="single-screenshot">
                                <img src="{{ asset('upload/'.$value->image) }}" alt="">
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--SCREENSHOT AREA END-->

<!--TEAM AREA-->
<section class="team-area padding-100-70 sky-gray-bg" id="team">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2><span>{{ $data->team_title }}</span></h2>
                        <p>{!! nl2br($data->team_description) !!}</p>
                    @else
                        <h2><span>{{ $data->team_title_english }}</span></h2>
                        <p>{!! nl2br($data->team_description_english) !!}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($team as $value)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team team-style-1 mb30 center wow fadeInUp" data-wow-delay="0.1s">
                        <div class="member-image">
                            <img src="{{ asset('upload/'.$value->image) }}" alt="">
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>{{ $value->name }}</h4>
                                <p>{{ $value->job }}</p>
                            </div>
                            <div class="social-bookmark">
                                <ul>
                                    <li><a href="{{ $value->facebook }}"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="{{ $value->twitter }}"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="{{ $value->pinterest }}"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="{{ $value->linkedin }}"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!--TEAM AREA END-->

<!--TESTMONIAL AREA AREA-->
<section class="testmonial-area section-padding" id="testmonial">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2>{{ $data->testimonial_title }}</h2>
                        <p>{!! nl2br($data->testimonial_description) !!}</p>
                    @else
                        <h2>{{ $data->testimonial_title_english }}</h2>
                        <p>{!! nl2br($data->testimonial_description_english) !!}</p>
                    @endif

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="testmonial-slider">
                    @foreach($testimonials as $value)
                        <div class="single-testmonial">
                            <div class="author-content padding40 mb50 xs-mb30">
                                <div class="testmonial-quote"></div>
                                @if(session()->get('user_lang')=='ar')
                                    <p>{!! nl2br($value->description) !!}</p>
                                @else
                                    <p>{!! nl2br($value->description_english) !!}</p>
                                @endif
                            </div>
                            <div class="author-name-image relative">
                                <div class="author-img">
                                    <img src="{{ asset('upload/'.$value->image) }}" alt="">
                                </div>
                                <h4>{{ $value->author }}</h4>
                                <a href="{{ $value->link }}">
                                    @if(session()->get('user_lang')=='ar')
                                        {{ $value->link_text }}
                                    @else
                                        {{ $value->link_text_english }}
                                    @endif
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!--TESTMONIAL AREA AREA END-->

<!--FUN FACT AREA AREA-->
<section class="fun-fact-area center white relative padding-100-70" id="fact">
    <div class="area-bg" data-stellar-background-ratio="0.6"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="fact-icon font36 mb20">
                        <i class="ti ti-user"></i>
                    </div>
                    <h3 class="font400 font36 xs-font26"><span class="counter">{{ $data->total_user }}</span></h3>
                    <p class="font600">@lang('landpage.total_user')</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="fact-icon font36 mb20">
                        <i class="ti ti-heart"></i>
                    </div>
                    <h3 class="font400 font36 xs-font26"><span class="counter">{{ $data->loyal_customer }}</span></h3>
                    <p class="font600">@lang('landpage.loyal_customer')</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="fact-icon font36 mb20">
                        <i class="ti ti-cup"></i>
                    </div>
                    <h3 class="font400 font36 xs-font26"><span class="counter">{{ $data->total_achivement }}</span></h3>
                    <p class="font600">@lang('landpage.total_achivement')</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="fact-icon font36 mb20">
                        <i class="ti ti-star"></i>
                    </div>
                    <h3 class="font400 font36 xs-font26"><span class="counter">{{ $data->app_ratting }}</span></h3>
                    <p class="font600">@lang('landpage.app_ratting')</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FUN FACT AREA AREA END-->

<!--PRICING AREA AREA-->
<section class="pricing-area padding-top" id="pricing">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    <h2>@lang('landpage.best')<span>@lang('landpage.pricing')</span> @lang('landpage.plan')</h2>
                    <p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit
                        labore Lorem ipsum amet madolor sit amet.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="single-price border-radious5 center sm-mb50 xs-mb50 wow fadeIn box-hover-21">
                    <div class="area-bg"></div>
                    <div class="price-hidding padding70">
                        <h3 class="font30">@lang('landpage.basic')</h3>
                    </div>
                    <div class="price-rate">
                        <h3 class="font42"><sup>$</sup>19
                            <sub>/@lang('landpage.anully')</sub>
                        </h3>
                    </div>
                    <div class="price-details mb20">
                        <ul>
                            <li>100mb Disk Space</li>
                            <li>2 Subdomains</li>
                            <li>5 Email Accounts</li>
                            <li>Webmail Support</li>
                        </ul>
                    </div>
                    <div class="price-button padding50 font14 uppercase">
                        <a href="#" class="purchase-button">@lang('landpage.purchase')</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-price active border-radious5 center sm-mb50 xs-mb50 wow fadeIn box-hover-21">
                    <div class="area-bg"></div>
                    <div class="price-hidding padding70">
                        <h3 class="font30">@lang('landpage.Standard')</h3>
                    </div>
                    <div class="price-rate">
                        <h3 class="font42"><sup>$</sup>29
                            <sub>/@lang('landpage.anully')</sub>
                        </h3>
                    </div>
                    <div class="price-details mb20">
                        <ul>
                            <li>500mb Disk Space</li>
                            <li>10 Subdomains</li>
                            <li>20 Email Accounts</li>
                            <li>Webmail Support</li>
                        </ul>
                    </div>
                    <div class="price-button padding50 font14 uppercase">
                        <a href="#" class="purchase-button">@lang('landpage.purchase')</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-price border-radious5 center wow fadeIn box-hover-21">
                    <div class="area-bg"></div>
                    <div class="price-hidding padding70">
                        <h3 class="font30">@lang('landpage.Premium')</h3>
                    </div>
                    <div class="price-rate">
                        <h3 class="font42"><sup>$</sup>19
                            <sub>/@lang('landpage.anully')</sub>
                        </h3>
                    </div>
                    <div class="price-details mb20">
                        <ul>
                            <li>1000mb Disk Space</li>
                            <li>50 Subdomains</li>
                            <li>100 Email Accounts</li>
                            <li>Webmail Support</li>
                        </ul>
                    </div>
                    <div class="price-button padding50 font14 uppercase">
                        <a href="#" class="purchase-button">@lang('landpage.purchase')</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-price visible-sm border-radious5 center wow fadeIn box-hover-21">
                    <div class="area-bg"></div>
                    <div class="price-hidding padding70">
                        <h3 class="font30">@lang('landpage.Ultimate')</h3>
                    </div>
                    <div class="price-rate">
                        <h3 class="font42"><sup>$</sup>19
                            <sub>/@lang('landpage.anully')</sub>
                        </h3>
                    </div>
                    <div class="price-details mb20">
                        <ul>
                            <li>1000mb Disk Space</li>
                            <li>50 Subdomains</li>
                            <li>100 Email Accounts</li>
                            <li>Webmail Support</li>
                        </ul>
                    </div>
                    <div class="price-button padding50 font14 uppercase">
                        <a href="#" class="purchase-button">@lang('landpage.purchase')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="faqs-area padding-top" id="faqs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2><span>{{ $data->faq_title }}</span></h2>
                        <p>{!! nl2br($data->faq_description) !!}</p>
                    @else
                        <h2><span>{{ $data->faq_title_english }}</span></h2>
                        <p>{!! nl2br($data->faq_description_english) !!}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row flex-v-center">
            <div class="col-md-4">
                <div class="faqs-mockup hidden-sm xs-center xs-mb50">
                    <img src="{{ asset('landpage/assest') }}/img/faqs-mockup.png" alt="">
                </div>
            </div>
            <div class="col-md-offset-1 col-md-7">
                <div class="panel-group sm-mb50 xs-mb50" id="accordion-main">

                    @foreach($faq as $key=>$value)
                        <div class="panel panel-default {{ $key==0 ? 'active':'' }}">
                            <div class="panel-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main"
                                   href="#collapse{{ $key }}">
                                    <div class="panel-title">
                                        <span>
                                            {{ $key+1 <10 ?  '0'.($key+1) : $key+1 }}
                                        </span>
                                        <span>
                                            @if(session()->get('user_lang')=='ar')
                                                {{ $value->question }}
                                            @else
                                                {{ $value->question_english }}
                                            @endif
                                        </span>
                                        <i class="pull-right fa fa-angle-down"></i>
                                    </div>
                                </a>
                            </div>
                            <div id="collapse{{ $key }}" class="panel-collapse collapse {{ $key==0 ? 'in':'' }}">
                                <div class="panel-body">
                                    @if(session()->get('user_lang')=='ar')
                                        {!! nl2br($value->answer) !!}
                                    @else
                                        {!! nl2br($value->answer_english) !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section class="download-area sky-gray-bg section-padding" id="download">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="subscriber-form-area mb70 wow fadeIn">
                    <form id="subform" method="get" action="" class="subscriber-form mb100">
                        {{ csrf_field() }}
                        <label class="mt10" for="mc-email"></label>
                        <input type="email" name="email" required id="mc-email" placeholder="email@example.com">
                        <button type="submit" id="subscribe" class="plus-btn">@lang('landpage.Subscribe')</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="area-title text-center wow fadeIn">
                    @if(session()->get('user_lang')=='ar')
                        <h2><span>{{ $data->download_title }}</span></h2>
                        <p>{!! nl2br($data->download_description) !!}</p>
                    @else
                        <h2><span>{{ $data->download_title_english }}</span></h2>
                        <p>{!! nl2br($data->download_description_english) !!}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                <div class="download-content center wow fadeIn">
                    <div class="download-button m50 xs-mt20">
                        <a class="left" href="{{ $data->google_store_link }}">
                            <i class="fa fa-android"></i>
                            <div>
                                <p class="font14">@lang('landpage.avilable_on')</p>
                                <span class="font18 font700">@lang('landpage.google_store')</span>
                            </div>
                        </a>
                        <a class="active left" href="{{ $data->apple_store_link }}">
                            <i class="fa fa-apple"></i>
                            <div>
                                <p class="font14">@lang('landpage.avilable_on')</p>
                                <span class="font18 font700">@lang('landpage.apple_store')</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--DOWNLOAD AREA AREA END-->

<!--BLOG AREA-->
{{--<section class="blog-feed-area padding-100-50" id="news">--}}
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">--}}
{{--<div class="area-title text-center wow fadeIn">--}}
{{--<h2>Blog <span>Posts</span></h2>--}}
{{--<p>Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit--}}
{{--labore Lorem ipsum amet madolor sit amet.</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">--}}
{{--<div class="single-blog-item mb50 wow fadeIn">--}}
{{--<div class="blog-thumb mb30">--}}
{{--<a href="blog.html"><img src="{{ asset('landpage/assest') }}/img/blog/blog-1.jpg" alt=""></a>--}}
{{--</div>--}}
{{--<div class="blog-details">--}}
{{--<p class="blog-meta font14 mb20"><a href="#">Feb 01, 2016 </a> by <a href="#">Admin</a></p>--}}
{{--<h3 class="blog-title mb30"><a href="blog.html">Beautiful Place for your Great Journey</a></h3>--}}
{{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt--}}
{{--elauta labore eta.</p>--}}
{{--<a class="readmore font14 mt30 uppercase" href="blog.html">Read More</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">--}}
{{--<div class="single-blog-item mb50 wow fadeIn">--}}
{{--<div class="blog-thumb mb30">--}}
{{--<a href="blog.html"><img src="{{ asset('landpage/assest') }}/img/blog/blog-2.jpg" alt=""></a>--}}
{{--</div>--}}
{{--<div class="blog-details">--}}
{{--<p class="blog-meta font14 mb20"><a href="#">Feb 01, 2016 </a> by <a href="#">Admin</a></p>--}}
{{--<h3 class="blog-title mb30"><a href="blog.html">Beautiful Place for your Great Journey</a></h3>--}}
{{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt--}}
{{--elauta labore eta.</p>--}}
{{--<a class="readmore font14 mt30 uppercase" href="blog.html">Read More</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">--}}
{{--<div class="single-blog-item mb50 wow fadeIn">--}}
{{--<div class="blog-thumb mb30">--}}
{{--<a href="blog.html"><img src="{{ asset('landpage/assest') }}/img/blog/blog-3.jpg" alt=""></a>--}}
{{--</div>--}}
{{--<div class="blog-details">--}}
{{--<p class="blog-meta font14 mb20"><a href="#">Feb 01, 2016 </a> by <a href="#">Admin</a></p>--}}
{{--<h3 class="blog-title mb30"><a href="blog.html">Beautiful Place for your Great Journey</a></h3>--}}
{{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt--}}
{{--elauta labore eta.</p>--}}
{{--<a class="readmore font14 mt30 uppercase" href="blog.html">Read More</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">--}}
{{--<div class="single-blog-item mb50 wow fadeIn visible-sm">--}}
{{--<div class="blog-thumb mb30">--}}
{{--<a href="blog.html"><img src="{{ asset('landpage/assest') }}/img/blog/blog-1.jpg" alt=""></a>--}}
{{--</div>--}}
{{--<div class="blog-details">--}}
{{--<p class="blog-meta font14 mb20"><a href="#">Feb 01, 2016 </a> by <a href="#">Admin</a></p>--}}
{{--<h3 class="blog-title mb30"><a href="blog.html">Beautiful Place for your Great Journey</a></h3>--}}
{{--<p>Lorem dolor sit amet, consectetur floralm adipisicing elit, sed do eiusmod tem aincididunt--}}
{{--elauta labore eta.</p>--}}
{{--<a class="readmore font14 mt30 uppercase" href="blog.html">Read More</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}
<!--BLOG AREA END-->

<!--CONTACT US AREA-->
<section class="contact-area padding-bottom-50 sky-gray-bg" id="contact">
    <div class="map-area relative">
        <div id="map" style="width: 100%; height: 600px;"></div>
    </div>
    <div class="contact-form-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="contact-form-content padding50 xs-padding20 mb100 sm-mb80 xs-mb50">
                        <div class="contact-title text-center mb50 wow fadeIn">
                            <h3 class="font32">@lang('landpage.get_in_touch')</h3>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('msg'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{{ session()->get('msg') }}</li>
                                </ul>
                            </div>
                        @endif

                        <div class="contact-form wow fadeIn">
                            <form action="{{ url('contact') }}" method="post">

                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="form-name" name="name"
                                                       placeholder="@lang('landpage.Name').." required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="email-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="form-email"
                                                       name="email" placeholder="@lang('landpage.email').." required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" required class="form-control" id="form-phone"
                                                       name="subject" placeholder="@lang('landpage.Subject')..">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="message-field">
                                            <div class="form-input">
                                                <textarea class="form-control" rows="6" id="form-message"
                                                          name="message"
                                                          required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group center mb0">
                                            <button type="submit">@lang('landpage.send_message')</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="text-icon-box mb50 center wow fadeIn">
                        <div class="box-icon">
                            <i class="ti-headphone-alt"></i>
                        </div>
                        <p>{{ $setting->phone }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="text-icon-box relative mb50 center  wow fadeIn">
                        <div class="box-icon">
                            <i class="ti-location-pin"></i>
                        </div>
                        <p>{{ $setting->address }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="text-icon-box relative mb50 xs-mb0 center wow fadeIn">
                        <div class="box-icon">
                            <i class="ti-email"></i>
                        </div>
                        <p>{{ $setting->email }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--CONTACT US AREA END-->

<!--FOOER AREA-->
<footer class="footer-area sky-gray-bg relative">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                <div class="footer-social-bookmark text-center wow fadeIn">
                    <div class="footer-logo mb50">
                        <a href="#"><img src="{{ asset('landpage/assest') }}/img/logo.png" alt=""></a>
                    </div>
                    <ul class="social-bookmark mt50">
                        <li><a href="{{ $setting->facebook }}"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{ $setting->twitter }}"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="{{ $setting->linkedin }}"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="{{ $setting->pinterest }}"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="{{ $setting->google }}"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="footer-copyright text-center wow fadeIn">
                    <p>Copyright &copy; <a href="http://themeforest.net/user/bdexpert"> BDEXPERT</a> All Right Reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--FOOER AREA END-->


<!--====== SCRIPTS JS ======-->
<script src="{{ asset('landpage/assest') }}/js/vendor/jquery-1.12.4.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/vendor/bootstrap.min.js"></script>

<!--====== PLUGINS JS ======-->
<script src="{{ asset('landpage/assest') }}/js/vendor/jquery.easing.1.3.js"></script>
<script src="{{ asset('landpage/assest') }}/js/vendor/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/vendor/jquery.appear.js"></script>
<script src="{{ asset('landpage/assest') }}/js/owl.carousel.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/stellar.js"></script>
<script src="{{ asset('landpage/assest') }}/js/waypoints.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/jquery.counterup.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/wow.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/jquery-modal-video.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/stellarnav.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/placeholdem.min.js"></script>
<script src="{{ asset('landpage/assest') }}/js/contact-form.js"></script>
<script src="{{ asset('landpage/assest') }}/js/jquery.ajaxchimp.js"></script>
<script src="{{ asset('landpage/assest') }}/js/jquery.sticky.js"></script>
<script src="{{ asset('landpage/assest') }}/js/particle.js"></script>
<script src="{{ asset('landpage/assest') }}/js/app.js"></script>

<!--===== ACTIVE JS=====-->
<script src="{{ asset('landpage/assest') }}/js/main.js"></script>

<!--===== MAPS JS=====-->
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&amp;sensor=false"></script>
<script src="{{ asset('landpage/assest') }}/js/maps.active.js"></script>
</body>

<style type="text/css">
    .btn-login p {
        background: #8256ff;
        color: white;
        padding: 10px;
        margin-top: -10px;
        border-radius: 10px;
    }

    .btn-login p:hover {
        background: white;
        color: #8256ff;
    }

    ul#nav li a {
        padding: 40px 10px;
    }
</style>
<!-- Mirrored from quomodosoft.com/html/appton/demo/index-particle.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 Aug 2018 12:31:32 GMT -->
</html>
