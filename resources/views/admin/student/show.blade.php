@extends('layouts.master')

@section('title')
    {{ trans('admin.student') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.student') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('student.index') }}">{{ trans('admin.student') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>

    {{--<button class="fcbtn btn btn-success btn-1d"--}}
    {{--onclick="window.location='{{ route('student.create') }}'">{{ trans('admin.add') }}</button>--}}
    {{--<br>--}}
    {{--<br>--}}

    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.student') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            {{--<th>{{ trans('admin.image') }}</th>--}}
                            <th>{{ trans('admin.name') }}</th>
                            <th>{{ trans('admin.email') }}</th>
                            <th>{{ __('login.country') }}</th>
                            <th>{{ __('login.nationality') }}</th>
                            <th>{{ __('admin.account_type') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>1</td>
                            {{--<td>--}}
                            {{--<img style="height: 200px;width: 200px"--}}
                            {{--src="{{ asset('upload/'.$data->personalImage) }}">--}}
                            {{--</td>--}}
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->country->country_name }}</td>
                            <td>{{ $data->nationality->country_name }}</td>
                            <td>{{ $data->accountType == 1 ? 'Student' : 'Parent' }}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>


@endsection