@extends('layouts.master')

@section('title')
    {{ trans('admin.student') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.student') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('student.index') }}">{{ trans('admin.student') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>


    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.student') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.name') }}</th>
                            <th>{{ trans('admin.email') }}</th>
                            <th>{{ trans('admin.account_type') }}</th>
                            <th class="text-nowrap">{{ trans('admin.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <a href="{{ route('student.show',[$value->id]) }}">{{ $value->name }}</a>
                                </td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->accountType == 1 ? 'Studen' : 'Parent' }}</td>
                                <td class="text-nowrap">

                                    <a href="{{ route('student.edit',[$value->id]) }}" data-toggle="tooltip"
                                       data-original-title="Edit">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i>
                                    </a>

                                    @if($value->open==1)
                                        <a class="fcbtn btn btn-danger btn-outline btn-1b"
                                           href="{{ route('student.inactive',[$value->id]) }}">
                                            @lang('admin.inactive')
                                        </a>
                                    @else
                                        <a class="fcbtn btn btn-success btn-outline btn-1b"
                                           href="{{ route('student.active',[$value->id]) }}">
                                            @lang('admin.active')
                                        </a>
                                    @endif


                                    <a href=""
                                       onclick=" event.preventDefault();
                                               var r = confirm('are you sure?');
                                               if (r==true){document.getElementById('delete{{$value->id}}').submit();}"
                                       data-toggle="tooltip" data-original-title="Close">
                                        <i class="fa fa-close text-danger"></i>
                                    </a>

                                    <form method="post"
                                          id="delete{{$value->id}}"
                                          action="{{ route('student.destroy',[$value->id]) }}"
                                          style="display: none;">
                                        <input name="_method" type="hidden" value="DELETE">
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection