@extends('layouts.master')

@section('title')
    {{ trans('admin.student') }}
@endsection

@section('content')
    <div class="row bg-title">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.student') }}</h4>
        </div>

        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>

            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('student.index') }}">{{ trans('admin.student') }}</a>
                </li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.student') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="{{ route('student.update',[$data->id]) }}"
                      enctype="multipart/form-data">

                    {{csrf_field()}}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.name') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('name',$data->name) }}" class="form-control"
                                   name="name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.email') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('email',$data->email) }}" class="form-control"
                                   name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('login.phone') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('phone',$data->phone) }}" class="form-control"
                                   name="phone">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('login.country') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" name="country_id">
                                @foreach($country as $value)
                                    <option {{ $data->country_id == $value->country_id ? 'selected' : '' }} value="{{ $value->country_id }}">
                                        {{ $value->country_name }}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.nationality') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" name="nationality_id">
                                @foreach($country as $value)
                                    <option {{ $data->nationality_id == $value->country_id ? 'selected' : '' }} value="{{ $value->country_id }}">
                                        {{ $value->country_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection