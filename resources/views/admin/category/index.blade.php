@extends('layouts.master')

@section('title')
    {{ trans('admin.category') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.category') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('category.index') }}">{{ trans('admin.category') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>

    <button class="fcbtn btn btn-success btn-1d"
            onclick="window.location='{{ route('category.create') }}'">{{ trans('admin.add') }}</button>
    <br>
    <br>

    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.category') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.name') }}</th>
                            <th>{{ trans('admin.name_english') }}</th>
                            <th class="text-nowrap">{{ trans('admin.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->category_name }}</td>
                                <td>{{ $value->category_name_english }}</td>
                                <td class="text-nowrap">

                                    <a href="{{ route('category.edit',[$value->category_id]) }}"
                                       data-toggle="tooltip"
                                       data-original-title="Edit">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i>
                                    </a>

                                    <a href=""
                                       onclick=" event.preventDefault();
                                               var r = confirm('are you sure?');
                                               if (r==true){document.getElementById('delete{{$value->category_id}}').submit();}"
                                       data-toggle="tooltip" data-original-title="Close">
                                        <i class="fa fa-close text-danger"></i>
                                    </a>

                                    <form method="post"
                                          id="delete{{$value->category_id}}"
                                          action="{{ route('category.destroy',[$value->category_id]) }}"
                                          style="display: none;">
                                        <input name="_method" type="hidden" value="DELETE">
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>


@endsection