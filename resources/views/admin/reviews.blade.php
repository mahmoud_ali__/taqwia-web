@extends('layouts.master')

@section('title')
    {{ trans('admin.reviews') }}
@endsection

@section('content')

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">{{ trans('admin.reviews') }}</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
            <i class="ti-settings text-white"></i>
        </button>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.review') }}">{{ trans('admin.reviews') }}</a></li>
            <li class="active">{{ trans('admin.show') }}</li>
        </ol>
    </div>
</div>

<div class="row">

    <div class="col-sm-12">

        <div class="white-box">
            <h3 class="box-title m-b-0">{{ trans('admin.reviews') }}</h3>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('admin.name') }}</th>
                        <th>{{ trans('admin.title') }}</th>
                        <th>{{ trans('admin.description') }}</th>
                        <th>{{ trans('admin.rank') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews as $key=>$rev)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $rev->name }}</td>
                            <td width="200px">{{ $rev->title }}</td>
                            <td width="500px">{{ $rev->description }}</td>
                            <td width="20px">{{ $rev->rank }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection