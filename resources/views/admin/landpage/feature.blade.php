@extends('layouts.master')

@section('title')
    {{ trans('landpage.feature') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('landpage.feature') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('landpage.home') }}">{{ trans('admin.landpage') }}</a></li>
                <li><a href="{{ route('landpage.feature') }}">{{ trans('landpage.feature') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('landpage.feature') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }} 1"
                                   name="feature_title1" value="{{ old('feature_title1',$data->feature_title1) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }} 1"
                                   name="feature_title1_english" value="{{ old('feature_title1_english',$data->feature_title1_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description') }} 1"
                                   name="feature_description1"
                                   value="{{ old('feature_description1',$data->feature_description1) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description_english') }} 1"
                                   name="feature_description1_english"
                                   value="{{ old('feature_description1_english',$data->feature_description1_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="feature_image1">
                            <br>
                            <img style="height: 200px" src="{{ asset('upload/'.$data->feature_image1) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }} 2"
                                   name="feature_title2" value="{{ old('feature_title2',$data->feature_title2) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }} 2"
                                   name="feature_title2_english" value="{{ old('feature_title2_english',$data->feature_title2_english) }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description') }} 2"
                                   name="feature_description2"
                                   value="{{ old('feature_description2',$data->feature_description2) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description_english') }} 2"
                                   name="feature_description2_english"
                                   value="{{ old('feature_description2_english',$data->feature_description2_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="feature_image2">
                            <br>
                            <img style="height: 200px" src="{{ asset('upload/'.$data->feature_image2) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }} 3"
                                   name="feature_title3" value="{{ old('feature_title3',$data->feature_title3) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }} 3"
                                   name="feature_title3_english" value="{{ old('feature_title3_english',$data->feature_title3_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description') }} 3"
                                   name="feature_description3"
                                   value="{{ old('feature_description3',$data->feature_description3) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description_english') }} 3"
                                   name="feature_description3_english"
                                   value="{{ old('feature_description3_english',$data->feature_description3_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="feature_image3">
                            <br>
                            <img style="height: 200px" src="{{ asset('upload/'.$data->feature_image3) }}">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>

                </form>
            </div>
        </div>
    </div>
@endsection