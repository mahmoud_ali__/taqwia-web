@extends('layouts.master')

@section('title')
    {{ trans('landpage.general') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('landpage.general') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('landpage.general') }}">{{ trans('landpage.general') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('landpage.general') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.screenshot_title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.screenshot_title') }}"
                                   name="screenshot_title"
                                   value="{{ old('screenshot_title',$data->screenshot_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="screenshot_title_english"
                                   value="{{ old('screenshot_title_english',$data->screenshot_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.screenshot_description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.screenshot_description') }}"
                                      name="screenshot_description">{{ old('screenshot_description',$data->screenshot_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }}"
                                      name="screenshot_description_english">{{ old('screenshot_description_english',$data->screenshot_description_english) }}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.team_title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.team_title') }}"
                                   name="team_title" value="{{ old('team_title',$data->team_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="team_title_english" value="{{ old('team_title',$data->team_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.team_description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.team_description') }}"
                                      name="team_description">{{ old('team_description',$data->team_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }}"
                                      name="team_description_english">{{ old('team_description_english',$data->team_description_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{ trans('landpage.total_user') }}</label>
                        <div class="col-md-1">
                            <input type="text" class="form-control" placeholder="{{ trans('landpage.total_user') }}"
                                   name="total_user" value="{{ old('total_user',$data->total_user) }}">
                        </div>

                        <label class="col-md-2">{{ trans('landpage.loyal_customer') }}</label>
                        <div class="col-md-1">
                            <input type="text" class="form-control" placeholder="{{ trans('landpage.loyal_customer') }}"
                                   name="loyal_customer" value="{{ old('loyal_customer',$data->loyal_customer) }}">
                        </div>

                        <label class="col-md-2">{{ trans('landpage.total_achivement') }}</label>
                        <div class="col-md-1">
                            <input type="text" class="form-control"
                                   placeholder="{{ trans('landpage.total_achivement') }}"
                                   name="total_achivement"
                                   value="{{ old('total_achivement',$data->total_achivement) }}">
                        </div>

                        <label class="col-md-2">{{ trans('landpage.app_ratting') }}</label>
                        <div class="col-md-1">
                            <input type="text" class="form-control" placeholder="{{ trans('landpage.app_ratting') }}"
                                   name="app_ratting" value="{{ old('app_ratting',$data->app_ratting) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.testimonial_title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.testimonial_title') }}"
                                   name="testimonial_title"
                                   value="{{ old('testimonial_title',$data->testimonial_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="testimonial_title_english"
                                   value="{{ old('testimonial_title_english',$data->testimonial_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.description') }}"
                                      name="testimonial_description">{{ old('testimonial_description',$data->testimonial_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }}"
                                      name="testimonial_description_english">{{ old('testimonial_description_english',$data->testimonial_description_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.faq_title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.faq_title') }}"
                                   name="faq_title" value="{{ old('faq_title',$data->faq_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="faq_title_english"
                                   value="{{ old('faq_title_english',$data->faq_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.faq_description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.faq_description') }}"
                                      name="faq_description">{{ old('faq_description',$data->faq_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }}"
                                      name="faq_description_english">{{ old('faq_description_english',$data->faq_description_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.download_title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.download_title') }}"
                                   name="download_title" value="{{ old('download_title',$data->download_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="download_title_english"
                                   value="{{ old('download_title_english',$data->download_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.download_description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.download_description') }}"
                                      name="download_description">{{ old('download_description',$data->download_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }}"
                                      name="download_description_english">{{ old('download_description_english',$data->download_description_english) }}</textarea>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>

                </form>
            </div>
        </div>
    </div>
@endsection