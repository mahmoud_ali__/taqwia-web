@extends('layouts.master')

@section('title')
    {{ trans('landpage.about') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('landpage.about') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('landpage.about') }}">{{ trans('landpage.about') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('landpage.about') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }}"
                                   name="about_title" value="{{ old('about_title',$data->about_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="about_title_english"
                                   value="{{ old('about_title_english',$data->about_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.description') }}"
                                   name="about_description"
                                   value="{{ old('about_description',$data->about_description) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control"
                                   placeholder="{{ trans('admin.description_english') }}"
                                   name="about_description_english"
                                   value="{{ old('about_description_english',$data->about_description_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }} 1"
                                   name="about_title1" value="{{ old('about_title1',$data->about_title1) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }} 1"
                                   name="about_title1_english"
                                   value="{{ old('about_title1_english',$data->about_title1_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.description') }} 1"
                                      name="about_description1">{{ old('about_description1',$data->about_description1) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }} 1"
                                      name="about_description1_english">{{ old('about_description1_english',$data->about_description1_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="about_image1">
                            <br>
                            <img style="height: 200px" src="{{ asset('upload/'.$data->about_image1) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }} 2"
                                   name="about_title2" value="{{ old('about_title2',$data->about_title2) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }} 2"
                                   name="about_title2_english"
                                   value="{{ old('about_title2_english',$data->about_title2_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.description') }} 2"
                                      name="about_description2">{{ old('about_description2',$data->about_description2) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }} 2"
                                      name="about_description2_english">{{ old('about_description2_english',$data->about_description2_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="about_image2">
                            <br>
                            <img style="height: 200px" src="{{ asset('upload/'.$data->about_image2) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }} 3"
                                   name="about_title3" value="{{ old('about_title3',$data->about_title3) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }} 3"
                                   name="about_title3_english"
                                   value="{{ old('about_title3_english',$data->about_title3_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.description') }} 3"
                                      name="about_description3">{{ old('about_description3',$data->about_description3) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control"
                                      placeholder="{{ trans('admin.description_english') }} 3"
                                      name="about_description3_english">{{ old('about_description3_english',$data->about_description3_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="about_image3">
                            <br>
                            <img style="height: 200px" src="{{ asset('upload/'.$data->about_image3) }}">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>

                </form>
            </div>
        </div>
    </div>
@endsection