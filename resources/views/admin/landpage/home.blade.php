@extends('layouts.master')

@section('title')
    {{ trans('admin.landpage.home') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.landpage.home') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('landpage.home') }}">{{ trans('admin.landpage') }}</a></li>
                <li><a href="{{ route('landpage.home') }}">{{ trans('admin.landpage.home') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.landpage.home') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title') }}"
                                   name="home_title" value="{{ old('home_title',$data->home_title) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.title_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.title_english') }}"
                                   name="home_title_english" value="{{ old('home_title_english',$data->home_title_english) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <textarea class="form-control" name="home_description"
                                      placeholder="{{ trans('admin.description') }}"
                                      rows="5">{{ old('home_description',$data->home_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea class="form-control" name="home_description_english"
                                      placeholder="{{ trans('admin.description_english') }}"
                                      rows="5">{{ old('home_description_english',$data->home_description_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.google_store_link') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.google_store_link') }}"
                                   name="google_store_link"
                                   value="{{ old('google_store_link',$data->google_store_link) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.apple_store_link') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.apple_store_link') }}"
                                   name="apple_store_link"
                                   value="{{ old('apple_store_link',$data->apple_store_link) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.background') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="home_background">
                            <br>
                            <img style="height: 500px" src="{{ asset('upload/'.$data->home_background) }}">
                        </div>

                    </div>


                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>

                </form>
            </div>
        </div>
    </div>
@endsection