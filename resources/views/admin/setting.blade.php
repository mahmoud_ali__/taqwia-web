@extends('layouts.master')

@section('title')
    {{ trans('admin.setting') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.setting') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.setting') }}">{{ trans('admin.setting') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.setting') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.phone') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.phone') }}"
                                   name="phone" value="{{ old('phone',$data->phone) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.address') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.address') }}"
                                   name="address" value="{{ old('address',$data->address) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.email') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.email') }}"
                                   name="email" value="{{ old('email',$data->email) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.facebook') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.facebook') }}"
                                   name="facebook" value="{{ old('facebook',$data->facebook) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.twitter') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.twitter') }}"
                                   name="twitter" value="{{ old('twitter',$data->twitter) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.linkedin') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.linkedin') }}"
                                   name="linkedin" value="{{ old('linkedin',$data->linkedin) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.pinterest') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.pinterest') }}"
                                   name="pinterest" value="{{ old('pinterest',$data->pinterest) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.google') }}</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="{{ trans('admin.google') }}"
                                   name="google" value="{{ old('google',$data->google) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.terms') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.terms') }}"
                                      name="terms">{{ old('terms',$data->terms) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.terms_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.terms_english') }}"
                                      name="terms_english">{{ old('terms',$data->terms_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.about_us') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.about_us') }}"
                                      name="about_us">{{ old('about_us',$data->about_us) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.about_us_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.about_us_english') }}"
                                      name="about_us_english">{{ old('about_us_english',$data->about_us_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.privacy_policy') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.privacy_policy') }}"
                                      name="privacy_policy">{{ old('privacy_policy',$data->privacy_policy) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.privacy_policy_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.privacy_policy_english') }}"
                                      name="privacy_policy_english">{{ old('privacy_policy_english',$data->privacy_policy_english) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.help_center') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.help_center') }}"
                                      name="help_center">{{ old('help_center',$data->help_center) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.help_center_english') }}</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control" placeholder="{{ trans('admin.help_center_english') }}"
                                      name="help_center_english">{{ old('help_center_english',$data->help_center_english) }}</textarea>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>

                </form>
            </div>
        </div>
    </div>
@endsection