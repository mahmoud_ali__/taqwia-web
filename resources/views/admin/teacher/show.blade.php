@extends('layouts.master')

@section('title')
    {{ trans('admin.teacher') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.teacher') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('teacher.index') }}">{{ trans('admin.teacher') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>

    {{--<button class="fcbtn btn btn-success btn-1d"--}}
    {{--onclick="window.location='{{ route('teacher.create') }}'">{{ trans('admin.add') }}</button>--}}
    {{--<br>--}}
    {{--<br>--}}

    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.teacher') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.image') }}</th>
                            <th>{{ trans('admin.name') }}</th>
                            <th>{{ trans('admin.email') }}</th>
                            <th>{{ __('login.country') }}</th>
                            <th>{{ __('login.nationality') }}</th>
                            <th>{{ trans('login.certificate') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>1</td>
                            <td>
                                <img style="height: 200px;width: 200px"
                                     src="{{ asset('upload/'.$data->personalImage) }}">
                            </td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->country->country_name }}</td>
                            <td>{{ $data->nationality->country_name }}</td>
                            <td>
                                @foreach($data->certificate as $certificate)
                                    {{ $certificate->certificate_name }}
                                @endforeach
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('login.scientific_certificate_image')</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.image') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data->scientific_certificate_image as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <img style="height: 200px;width: 200px"
                                         src="{{ asset('upload/'.$value->image) }}">
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('login.experience_certificate_image')</h3>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.image') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data->experience_certificate_image as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <img style="height: 200px;width: 200px"
                                         src="{{ asset('upload/'.$value->image) }}">
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('login.previous_work')</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('login.place_name')</th>
                            <th>@lang('login.from_year')</th>
                            <th>@lang('login.to_year')</th>
                            <th>@lang('login.job_title')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data->previousWork as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->place }}</td>
                                <td>{{ $value->from_year }}</td>
                                <td>{{ $value->to_year }}</td>
                                <td>{{ $value->jobTitle }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('login.training_course')</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('login.course_name')</th>
                            <th>@lang('login.course_place')</th>
                            <th>@lang('login.course_hour')</th>
                            <th>@lang('login.course_appreciation')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data->trainingCourse as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->place }}</td>
                                <td>{{ $value->hours }}</td>
                                <td>{{ $value->appreciation }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection