@extends('layouts.master')

@section('title')
    {{ trans('admin.teacher') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.teacher') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('teacher.index') }}">{{ trans('admin.teacher') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>

    {{--<button class="fcbtn btn btn-success btn-1d"--}}
    {{--onclick="window.location='{{ route('teacher.create') }}'">{{ trans('admin.add') }}</button>--}}
    {{--<br>--}}
    {{--<br>--}}

    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.teacher') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.name') }}</th>
                            <th>{{ trans('admin.email') }}</th>
                            <th class="text-nowrap">{{ trans('admin.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <a href="{{ route('teacher.show',[$value->id]) }}">{{ $value->name }}</a>
                                </td>
                                <td>{{ $value->email }}</td>
                                <td class="text-nowrap">

                                    <a href="{{ route('teacher.edit',[$value->id]) }}" data-toggle="tooltip"
                                       data-original-title="Edit">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i>
                                    </a>

                                    @if($value->open==1)
                                        <a class="fcbtn btn btn-danger btn-outline btn-1b"
                                           href="{{ route('teacher.inactive',[$value->id]) }}">
                                            @lang('admin.inactive')
                                        </a>
                                    @else
                                        <a class="fcbtn btn btn-success btn-outline btn-1b"
                                           href="{{ route('teacher.active',[$value->id]) }}">
                                            @lang('admin.active')
                                        </a>
                                    @endif


                                    <a href=""
                                       onclick=" event.preventDefault();
                                               var r = confirm('are you sure?');
                                               if (r==true){document.getElementById('delete{{$value->id}}').submit();}"
                                       data-toggle="tooltip" data-original-title="Close">
                                        <i class="fa fa-close text-danger"></i>
                                    </a>

                                    <form method="post"
                                          id="delete{{$value->id}}"
                                          action="{{ route('teacher.destroy',[$value->id]) }}"
                                          style="display: none;">
                                        <input name="_method" type="hidden" value="DELETE">
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection