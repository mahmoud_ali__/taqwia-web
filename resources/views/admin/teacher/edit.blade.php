@extends('layouts.master')

@section('title')
    {{ trans('admin.teacher') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.teacher') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('teacher.index') }}">{{ trans('admin.teacher') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.teacher') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="{{ route('teacher.update',[$data->id]) }}"
                      enctype="multipart/form-data">

                    {{csrf_field()}}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.name_en') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('name',$data->name) }}" class="form-control"
                                   name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.name_ar') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('name_ar',$data->name_ar) }}" class="form-control"
                                   name="name_ar">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.perHOur') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('perHOur',$data->perHOur) }}" class="form-control"
                                   name="perHOur">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.email') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('email',$data->email) }}" class="form-control"
                                   name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.personal_image') }}</label>
                        <div class="col-md-12">
                            <input class="form-control" type="file" name="personalImage">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.phone') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('phone',$data->phone) }}" class="form-control"
                                   name="phone">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.birthDate') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('birthDate',$data->birthDate) }}" class="form-control"
                                   name="birthDate">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.religion') }}</label>
                        <div class="col-md-12">
                            <select name="religion" class="form-control">
                                @foreach($religion as $value)
                                    <option {{ $data->religion == $value->religion_id ? 'selected':'' }} value="{{ $value->religion_id }}"> 
                                        @if(session()->get('user_lang')=='ar')
                                            {{ $value->religion_name }}
                                        @else
                                            {{ $value->religion_name_english }}
                                        @endif
                                        </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.country') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" name="country_id">
                                @foreach($country as $value)
                                    <option {{ $data->country_id == $value->country_id ? 'selected' : '' }} value="{{ $value->country_id }}">
                                        {{ $value->country_name }}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.nationality') }}</label>
                        <div class="col-md-12">
                            <select name="nationality_id" class="form-control">
                                @foreach($nationality as $value)
                                    <option {{ $data->nationality_id == $value->country_id ? 'selected':'' }} value="{{ $value->country_id }}">{{ $value->country_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.certificate') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" name="certificate">
                                @foreach($certificate as $value)
                                    <option {{  in_array($value->certificate_id,$user_certificate) ? 'selected' : '' }} value="{{ $value->certificate_id }}">
                                        {{ $value->certificate_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.handGraduation') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('handGraduation',$data->handGraduation) }}" class="form-control"
                                   name="handGraduation">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.graduationYear') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('graduationYear',$data->graduationYear) }}" class="form-control"
                                   name="graduationYear">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.generalAppreciation') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('generalAppreciation',$data->generalAppreciation) }}" class="form-control"
                                   name="generalAppreciation">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.jobTitle') }}</label>
                        <div class="col-md-12">
                            <select name="jobTitle" class="form-control">
                                @foreach($job as $value)
                                    <option {{ $data->jobTitle == $value->job_id ? 'selected':'' }} value="{{ $value->job_id }}"> 
                                        @if(session()->get('user_lang')=='ar')
                                            {{ $value->job_name }}
                                        @else
                                            {{ $value->job_name_english }}
                                        @endif
                                        </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.employer') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('employer',$data->employer) }}" class="form-control"
                                   name="employer">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection