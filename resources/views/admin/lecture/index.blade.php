@extends('layouts.master')

@section('title')
    {{ trans('admin.lecture') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.lecture') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('lecture.index') }}">{{ trans('admin.lecture') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>


    @if(auth()->user()->role==2)
        <button class="fcbtn btn btn-success btn-1d"
                onclick="window.location='{{ route('lecture.create') }}'">{{ trans('admin.add') }}</button>
        <br>
        <br>
    @endif

    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.lecture') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            @if(auth()->user()->role==1)
                                <th>{{ trans('admin.name') }}</th>
                            @endif
                            <th>{{ trans('admin.subject') }}</th>
                            <th>{{ trans('admin.subject_description') }}</th>
                            <!-- <th>{{ trans('admin.subject_hours') }}</th> -->
                            @if(auth()->user()->role==2)
                                <th class="text-nowrap">{{ trans('admin.action') }}</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                @if(auth()->user()->role==1)
                                    <td>{{ $value->user->name }}</td>
                                @endif
                                <td width="200px">{{ $value->subject->subject_title }}</td>
                                <td width="500px">{{ $value->subject->subject_description }}</td>
                                <!-- <td width="20px">{{ $value->subject->subject_hour }}</td> -->
                                @if(auth()->user()->role==2)

                                    <td class="text-nowrap">

                                        <a href="{{ route('lecture.edit',[$value->lecture_id]) }}"
                                           data-toggle="tooltip"
                                           data-original-title="Edit">
                                            <i class="fa fa-pencil text-inverse m-r-10"></i>
                                        </a>

                                        {{--<a href=""--}}
                                        {{--onclick=" event.preventDefault();--}}
                                        {{--var r = confirm('are you sure?');--}}
                                        {{--if (r==true){document.getElementById('delete{{$value->lecture_id}}').submit();}"--}}
                                        {{--data-toggle="tooltip" data-original-title="Close">--}}
                                        {{--<i class="fa fa-close text-danger"></i>--}}
                                        {{--</a>--}}

                                        {{--<form method="post"--}}
                                        {{--id="delete{{$value->lecture_id}}"--}}
                                        {{--action="{{ route('lecture.destroy',[$value->lecture_id]) }}"--}}
                                        {{--style="display: none;">--}}
                                        {{--<input name="_method" type="hidden" value="DELETE">--}}
                                        {{--{{ csrf_field() }}--}}
                                        {{--</form>--}}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>


@endsection