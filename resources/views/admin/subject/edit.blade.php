@extends('layouts.master')

@section('title')
    {{ trans('admin.subject') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.subject') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('subject.index') }}">{{ trans('admin.subject') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.subject') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="{{ route('subject.update',[$data->subject_id]) }}"
                      enctype="multipart/form-data">

                    {{csrf_field()}}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.name') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('subject_title',$data->subject_title) }}"
                                   class="form-control"
                                   name="subject_title">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.name_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('subject_title_english',$data->subject_title_english) }}" class="form-control"
                                   name="subject_title_english">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.image') }}</label>
                        <div class="col-md-12">
                            <input class="form-control" type="file" name="image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.levels') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" id="education_level" name="education_level_id">
                                @foreach($education_level as $value)
                                    <option {{ $value->education_level_id == $data->education_level_id ? 'selected' : '' }} value="{{ $value->education_level_id }}">{{ $value->education_level_name_english }} - {{ $value->education_level_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.category') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" id="category" name="category_id">
                                @foreach($category as $value)
                                    <option {{ $value->category_id == $data->category_id ? 'selected' : '' }} value="{{ $value->category_id }}">{{ $value->category_name_english }} - {{ $value->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.subcategory') }}</label>
                        <div class="col-md-12">
                            <select class="form-control select2" id="sub_category" name="sub_category_id">
                                @foreach($subcategory as $value)
                                    <option {{ $value->sub_category_id == $data->sub_category_id ? 'selected' : '' }} value="{{ $value->sub_category_id }}">{{ $value->sub_category_name_english }} - {{ $value->sub_category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.subject_hours') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('subject_hour',$data->subject_hour) }}"
                                   class="form-control"
                                   name="subject_hour">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.subject_description') }}</label>
                        <div class="col-md-12">
                            <textarea class="form-control" rows="5"
                                      name="subject_description">{{ old('subject_description',$data->subject_description) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.subject_description_english') }}</label>
                        <div class="col-md-12">
                            <textarea class="form-control" rows="5"
                                      name="subject_description_english">{{ old('subject_description_english',$data->subject_description_english) }}</textarea>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/dev.js') }}"></script>
@endpush