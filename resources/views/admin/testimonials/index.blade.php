@extends('layouts.master')

@section('title')
    {{ trans('landpage.testimonials') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('landpage.testimonials') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('testimonials.index') }}">{{ trans('landpage.testimonials') }}</a></li>
                <li class="active">{{ trans('landpage.testimonials') }}</li>
            </ol>
        </div>
    </div>

    <button class="fcbtn btn btn-success btn-1d"
            onclick="window.location='{{ route('testimonials.create') }}'">{{ trans('admin.add') }}</button>
    <br>
    <br>

    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('landpage.testimonials') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('admin.image') }}</th>
                            <th>{{ trans('admin.author') }}</th>
                            <th>{{ trans('admin.link_text') }}</th>
                            <th>{{ trans('admin.link') }}</th>
                            <th>{{ trans('admin.description') }}</th>
                            <th class="text-nowrap">{{ trans('admin.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    <img style="height: 100px" src="{{ asset('upload/'.$value->image) }}">
                                </td>
                                <td>{{ $value->author }}</td>
                                <td>{{ $value->link_text }}</td>
                                <td>{{ $value->link }}</td>
                                <td>{{ $value->description }}</td>
                                <td class="text-nowrap">

                                    <a href="{{ route('testimonials.edit',[$value->id]) }}" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="fa fa-pencil text-inverse m-r-10"></i>
                                    </a>

                                    <a href=""
                                       onclick=" event.preventDefault();
                                               var r = confirm('are you sure?');
                                               if (r==true){document.getElementById('delete{{$value->id}}').submit();}"
                                       data-toggle="tooltip" data-original-title="Close">
                                        <i class="fa fa-close text-danger"></i>
                                    </a>

                                    <form method="post"
                                          id="delete{{$value->id}}"
                                          action="{{ route('testimonials.destroy',[$value->id]) }}"
                                          style="display: none;">
                                        <input name="_method" type="hidden" value="DELETE">
                                        {{ csrf_field() }}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection