@extends('layouts.master')

@section('title')
    {{ trans('landpage.testimonials') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('landpage.testimonials') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('testimonials.index') }}">{{ trans('landpage.testimonials') }}</a></li>
                <li class="active">{{ trans('landpage.testimonials') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('landpage.testimonials') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="{{ route('testimonials.store') }}"
                      enctype="multipart/form-data">

                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.image') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.author') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('author') }}" class="form-control" name="author">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.link_text') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('link_text') }}" class="form-control" name="link_text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.link_text_english') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('link_text_english') }}" class="form-control" name="link_text_english">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.link') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('link') }}" class="form-control" name="link">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description') }}</label>
                        <div class="col-md-12">
                            <textarea class="form-control" rows="5" name="description">{{ old('description') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.description_english') }}</label>
                        <div class="col-md-12">
                            <textarea class="form-control" rows="5" name="description_english">{{ old('description_english') }}</textarea>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection