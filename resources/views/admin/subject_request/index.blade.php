@extends('layouts.master')

@section('title')
    {{ trans('admin.subject_request') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.subject_request') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('subject.request') }}">{{ trans('admin.subject_request') }}</a></li>
                <li class="active">{{ trans('admin.show') }}</li>
            </ol>
        </div>
    </div>


    <div class="row">

        <div class="col-sm-12">

            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.subject_request') }}</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('admin.student')</th>
                            @if(auth()->user()->role==1)
                                <th class="text-nowrap">@lang('admin.teacher')</th>
                            @endif
                            <th>@lang('admin.subject')</th>
                            <th>@lang('admin.date')</th>
                            <th>@lang('admin.time')</th>
                            <th class="text-nowrap">{{ trans('admin.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->student->name }}</td>
                                @if(auth()->user()->role==1)
                                    <th class="text-nowrap">{{ $value->teacher->name }}</th>
                                @endif
                                <td>{{ $value->subject->subject_title }}</td>
                                <td>{{ $value->subject_request_date }}</td>
                                <td>{{ $value->selectedHour }}:00 {{ $value->time }}</td>
                                {{--@if(auth()->user()->role==2)--}}
                                <td class="text-nowrap">

                                    @if($value->subject_request_status==1 && auth()->user()->role==2)
                                        <a href="{{ route('subject.accept',[$value->subject_request_id]) }}"
                                           data-toggle="tooltip"
                                           data-original-title="Accept">
                                            <i class="fa fa-check text-inverse m-r-10"></i>
                                        </a>

                                        <a href="{{ route('subject.refuse',[$value->subject_request_id]) }}"
                                           data-toggle="tooltip" data-original-title="Refuse">
                                            <i class="fa fa-close text-danger"></i>
                                        </a>
                                    @elseif($value->subject_request_status==1 && auth()->user()->role==1)
                                        @lang('admin.wait')
                                    @elseif($value->subject_request_status==2)
                                        @lang('admin.accepted')
                                    @else
                                        @lang('admin.refused')
                                    @endif

                                </td>
                                {{--@endif--}}

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>


@endsection