@extends('layouts.master')

@section('title')
    {{ trans('admin.team') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.team') }}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20">
                <i class="ti-settings text-white"></i>
            </button>
            <ol class="breadcrumb">
                <li><a href="{{ route('team.index') }}">{{ trans('admin.team') }}</a></li>
                <li class="active">{{ trans('admin.add') }}</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ trans('admin.team') }}</h3>
                <p class="text-muted m-b-30 font-13"></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" method="post" action="{{ route('team.update',[$data->id]) }}"
                      enctype="multipart/form-data">

                    {{csrf_field()}}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.image') }}</label>
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.name') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('name',$data->name) }}" class="form-control" name="name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.job') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('job',$data->job) }}" class="form-control" name="job">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.facebook') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('facebook',$data->facebook) }}" class="form-control"
                                   name="facebook">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.pinterest') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('pinterest',$data->pinterest) }}" class="form-control"
                                   name="pinterest">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.twitter') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('twitter',$data->twitter) }}" class="form-control"
                                   name="twitter">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{ trans('admin.linkedin') }}</label>
                        <div class="col-md-12">
                            <input type="text" value="{{ old('linkedin',$data->linkedin) }}" class="form-control"
                                   name="linkedin">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">{{ trans('admin.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection