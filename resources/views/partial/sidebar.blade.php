<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">

        <div class="sidebar-head">
            <h3>
                <span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span>
                <span class="hide-menu">Navigation</span>
            </h3>
        </div>

        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ route('admin.home') }}" class="waves-effect">
                    <i class="ti-user fa-fw"></i>
                    <span class="hide-menu">{{ trans('admin.profile') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('lecture.index') }}" class="">
                    <i class="mdi mdi-settings fa-fw"></i>
                    <span class="">{{ trans('admin.lecture') }}</span>
                </a>
            </li>

            <li>
                <a href="{{ route('subject.request') }}" class="">
                    <i class="mdi mdi-settings fa-fw"></i>
                    <span class="">{{ trans('admin.subject_request') }}</span>
                </a>
            </li>

            @if(auth()->user()->role==2)
                <li>
                    <a href="{{ route('admin.review') }}" class="">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="">{{ trans('admin.reviews') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('terms') }}" target="_blank" class="">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="">{{ trans('admin.terms') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('teacher.edit',[auth()->user()->id]) }}" target="_blank" class="">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="">{{ trans('admin.setting') }}</span>
                    </a>
                </li>
            @endif

            @if(auth()->user()->role==1)
                <li>
                    <a href="{{ route('subject.index') }}" class="">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="">{{ trans('admin.subject') }}</span>
                    </a>
                </li>

                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="mdi mdi-av-timer fa-fw" data-icon="v"></i>
                        <span class="hide-menu">
                        {{ trans('admin.landpage') }}
                            <span class="fa arrow"></span>
                        <span class="label label-rouded label-info pull-right">4</span>
                    </span>
                    </a>

                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{ route('landpage.home') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.home') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('landpage.feature') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.features') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('landpage.awesomefeature') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.awesome_feature') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('landpage.about') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.about') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('screenshot.index') }}" class="waves-effect">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.screenshot') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('team.index') }}" class="waves-effect">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.team') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('testimonials.index') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.testimonials') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('faq.index') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.faqs') }}</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('landpage.general') }}">
                                <i class=" fa-fw"></i>
                                <span class="hide-menu">{{ trans('landpage.general') }}</span>
                            </a>
                        </li>


                    </ul>
                </li>

                <li class="devider"></li>

                <li>
                    <a href="{{ route('teacher.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.teacher') }}</span>
                    </a>
                </li>


                <li>
                    <a href="{{ route('student.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.student') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('certificate.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('login.certificate') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('education.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.education_level') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('grade.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.grade') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('slider.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.slider') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('relation.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.relation') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('religion.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.religion') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('job.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.job') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('category.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.category') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('subcategory.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.subcategory') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('package.index') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.package') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('landpage.contact') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.contact') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('landpage.subscribe') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.subscribe') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.setting') }}" class="waves-effect">
                        <i class="mdi mdi-settings fa-fw"></i>
                        <span class="hide-menu">{{ trans('admin.setting') }}</span>
                    </a>
                </li>
            @endif


        </ul>
    </div>
</div>