@extends('layouts.app')

@section('title')
    {{ trans('login.register') }}
@endsection

@section('content')
    <!-- <section id="wrapper" class="new-login-register">

        <div class="lg-info-panel">
            <div class="inner-panel">
                <a href="javascript:void(0)" class="p-20 di">
                    <img src="{{ asset('assets') }}/plugins/images/admin-logo.png">
                </a>
                <div class="lg-content">

                </div>
            </div>
        </div>

        <div class="new-login-box">
            <div class="white-box">
                @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
@foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
                            @endforeach
                </ul>
            </div>
@endif


            <h3 class="box-title m-b-0 text-center">Sign Up to Taqwia</h3>
            <form class="form-horizontal new-lg-form" id="loginform" action="" method="post" enctype="multipart/form-data">

{{ csrf_field() }}

            <div class="form-group  m-t-20 {{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-6">
                            <label>Name</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}" required=""
                                   placeholder="Name">
                            @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
            </div>

            <div class="col-md-6">
                <label>Email Address</label>
                <input class="form-control" type="text" name="email" value="{{ old('email') }}" required=""
                                   placeholder="Email Address">
                            @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
            </div>
        </div>


        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-4">
                            <label>Password</label>
                            <input class="form-control" type="password" name="password" required=""
                                   placeholder="Password">
                            @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
            </div>

            <div class="col-md-4">
                <label>Password</label>
                <input class="form-control" type="password" name="password_confirmation" required=""
                       placeholder="Password Confirmation">
            </div>

            <div class="col-md-4">
                <label>personal Image</label>
                <input class="form-control" type="file" name="personalImage" required=""
                       placeholder="personal Image">
            </div>
        </div>

        <div class="form-group  m-t-20">
            <div class="col-md-4">
                <label>Phone</label>
                <input class="form-control" type="text" name="phone" value="{{ old('phone') }}" required=""
                                   placeholder="Phone">
                        </div>

                        <div class="col-md-4">
                            <label>birthDate</label>
                            <input class="form-control datepicker" type="text" name="birthDate" value="{{ old('birthDate') }}"
                                   required=""
                                   placeholder="birthDate">
                        </div>

                        <div class="col-md-4">
                            <label>Religion</label>
                            <input class="form-control" type="text" name="religion" value="{{ old('religion') }}"
                                   required=""
                                   placeholder="religion">
                        </div>
                    </div>

                    <div class="form-group  m-t-20">
                        <div class="col-md-4">
                            <label>nationality</label>
                            <select name="nationality" class="form-control">
                                @foreach($country as $value)
        <option {{ old('nationality')==$value->country_id ? 'selected':'' }} value="{{ $value->country_id }}">{{ $value->country_name }}</option>
                                @endforeach
            </select>
        </div>

        <div class="col-md-4">
            <label>country</label>
            <select name="country" class="form-control">
@foreach($country as $value)
        <option {{ old('country')==$value->country_id ? 'selected':'' }} value="{{ $value->country_id }}">{{ $value->country_name }}</option>
                                @endforeach
            </select>
        </div>

        <div class="col-md-4">
            <label>certificate</label>
            <select name="certificate" class="form-control">
@foreach($certificate as $value)
        <option {{ old('certificate')==$value->certificate_id ? 'selected':'' }} value="{{ $value->certificate_id }}">{{ $value->certificate_name }}</option>
                                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group  m-t-20">
        <div class="col-md-4">
            <label>hand Graduation</label>
            <input class="form-control" type="text" name="handGraduation"
                   value="{{ old('handGraduation') }}" required=""
                                   placeholder="hand Graduation">
                        </div>

                        <div class="col-md-4">
                            <label>graduation Year</label>
                            <input class="form-control" type="text" name="graduationYear"
                                   value="{{ old('graduationYear') }}"
                                   required=""
                                   placeholder="graduation Year">
                        </div>

                        <div class="col-md-4">
                            <label>general Appreciation</label>
                            <input class="form-control" type="text" name="generalAppreciation"
                                   value="{{ old('generalAppreciation') }}"
                                   required=""
                                   placeholder="general Appreciation">
                        </div>
                    </div>

                    <div class="form-group  m-t-20">
                        <div class="col-md-3">
                            <label>job Title</label>
                            <input class="form-control" type="text" name="jobTitle" value="{{ old('jobTitle') }}"
                                   required=""
                                   placeholder="job Title">
                        </div>

                        <div class="col-md-3">
                            <label>scientific certificate image</label>
                            <input class="form-control" type="file" name="scientific_certificate_image[]" multiple required="">
                        </div>

                        <div class="col-md-3">
                            <label>experience certificate image</label>
                            <input class="form-control" type="file" name="experience_certificate_image[]" multiple required="">
                        </div>

                        <div class="col-md-3">
                            <label>employer</label>
                            <input class="form-control" type="text" name="employer" value="{{ old('employer') }}"
                                   required=""
                                   placeholder="employer">
                        </div>
                    </div>

                    <div class="form-group  m-t-20">
                        <h2 class="text-center">previous work</h2>
                        <br>
                        <div class="col-md-4">
                            <label>Place Name</label>
                            <input class="form-control" type="text" name="previous_place[]"
                                   required=""
                                   placeholder="Place Name">
                        </div>

                        <div class="col-md-2">
                            <label>From Year</label>
                            <input class="form-control" type="text" name="from_year[]"
                                   required=""
                                   placeholder="From Year">
                        </div>

                        <div class="col-md-2">
                            <label>To Year</label>
                            <input class="form-control" type="text" name="to_year[]"
                                   required=""
                                   placeholder="To Year">
                        </div>

                        <div class="col-md-3">
                            <label>job Title</label>
                            <input class="form-control" type="text" name="previous_jobTitle[]"
                                   required=""
                                   placeholder="job Title">
                        </div>


                        <div class="col-md-1">
                            <label>Add New</label>
                            <a href="" class="add">
                                <i class="fa fa-plus fa-3x"></i>
                            </a>
                        </div>

                        <div id="previous_work">

                        </div>
                    </div>

                    <div class="form-group  m-t-20">
                        <h2 class="text-center">training course</h2>
                        <br>
                        <div class="col-md-3">
                            <label>Course Name</label>
                            <input class="form-control" type="text" name="course_name[]"
                                   required="" placeholder="Course Name">
                        </div>

                        <div class="col-md-3">
                            <label>Course Place</label>
                            <input class="form-control" type="text" name="course_place[]"
                                   required="" placeholder="Course Place">
                        </div>

                        <div class="col-md-3">
                            <label>Course Hour</label>
                            <input class="form-control" type="text" name="course_hour[]"
                                   required=""
                                   placeholder="Course Hour">
                        </div>

                        <div class="col-md-2">
                            <label>Course appreciation</label>
                            <input class="form-control" type="text" name="course_appreciation[]"
                                   required=""
                                   placeholder="Course appreciation">
                        </div>


                        <div class="col-md-1">
                            <label>Add New</label>
                            <a href="" class="add-new-course">
                                <i class="fa fa-plus fa-3x"></i>
                            </a>
                        </div>

                        <div id="training_course">

                        </div>
                    </div>


                    <div class="form-group text-center m-t-20">
                        <div class="col-md-12">
                            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light"
                                    type="submit">Register
                            </button>
                        </div>
                    </div>


                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>have an account?
                                <a href="{{ route('login') }}" class="text-primary m-l-5">
                                    <b>Sign In</b>
                                </a>
                            </p>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </section> -->

    <section id="wrapper" class="step-register">
        <div class="register-box">
            <div class="">
                <a href="javascript:void(0)" class="text-center db m-b-40">
                    <img src="{{ asset('assets') }}/Taqwaia.png"
                         alt="Home" style="height: 90px;"/>
                </a>

                <!-- multistep form -->
                <form id="msform" action="" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- progressbar -->
                    <ul id="eliteregister">
                        <li class="active">{{ __('login.teacher_account_setup') }}</li>
                        <li>{{ __('login.study_data') }}</li>
                        <li>{{ __('login.finish_setup') }}</li>
                    </ul>
                    <!-- fieldsets -->
                    <fieldset>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <h2 class="fs-title">{{ __('login.create_account') }}</h2>
                        <h3 class="fs-subtitle">{{ __('login.step1') }}</h3>

                        <div class="form-group  m-t-20 {{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-md-3">
                                <label>{{ __('login.name_en') }}</label>
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}"
                                       required=""
                                       placeholder="{{ __('login.name_en') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <label>{{ __('login.name_ar') }}</label>
                                <input class="form-control" type="text" name="name_ar" value="{{ old('name_ar') }}"
                                       required=""
                                       placeholder="{{ __('login.name_ar') }}">
                                @if ($errors->has('name_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_ar') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <label>{{ __('login.email') }}</label>
                                <input class="form-control" type="text" name="email" value="{{ old('email') }}"
                                       required=""
                                       placeholder="{{ __('login.email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-4">
                                <label>{{ __('login.password') }}</label>
                                <input class="form-control" type="password" name="password" required=""
                                       placeholder="{{ __('login.password') }}">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                            </div>

                            <div class="col-md-4">
                                <label>{{ __('login.password_confirmation') }}</label>
                                <input class="form-control" type="password" name="password_confirmation" required=""
                                       placeholder="{{ __('login.password_confirmation') }}">
                            </div>

                            <div class="col-md-4">
                                <label>{{ __('login.personal_image') }}</label>
                                <input class="form-control" type="file" name="personalImage" required=""
                                       placeholder="personal Image">
                            </div>
                        </div>

                        <div class="form-group  m-t-20">
                            <div class="col-md-4">
                                <label>@lang('login.phone')</label>
                                <input class="form-control" type="text" name="phone" value="{{ old('phone') }}"
                                       required=""
                                       placeholder="@lang('login.phone')">
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.birth_date')</label>
                                <input class="form-control datepicker" type="text" name="birthDate"
                                       value="{{ old('birthDate') }}"
                                       required=""
                                       placeholder="@lang('login.birth_date')">
                            </div>
                            <div class="col-md-4">
                                <label>@lang('login.religion')</label>
                                <select name="religion" class="form-control">
                                    @foreach($religion as $value)
                                        <option {{ old('religion')==$value->religion_id ? 'selected':'' }} value="{{ $value->religion_id }}"> 
                                            @if(session()->get('user_lang')=='ar')
                                                {{ $value->religion_name }}
                                            @else
                                                {{ $value->religion_name_english }}
                                            @endif
                                            </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group  m-t-20">
                            <div class="col-md-6">
                                <label>@lang('login.about_en')</label>
                                <textarea class="form-control" name="about"
                                    required="">{{ old('about') }}
                                </textarea>
                            </div>
                            <div class="col-md-6">
                                <label>@lang('login.about_ar')</label>
                                <textarea class="form-control" name="about_ar"
                                    required="">{{ old('about_ar') }}
                                </textarea>
                            </div>
                        </div>
                        <input type="button" name="next" class="next action-button" value="@lang('login.next')"/>
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">@lang('login.study_data')</h2>
                        {{--<h3 class="fs-subtitle">Your presence on the Study Data</h3>--}}


                        <div class="form-group  m-t-20">
                            <div class="col-md-4">
                                <label>@lang('login.nationality')</label>
                                <select name="nationality" class="form-control">
                                    @foreach($nationality as $value)
                                        <option {{ old('nationality')==$value->country_id ? 'selected':'' }} value="{{ $value->country_id }}">{{ $value->country_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.country')</label>
                                <select name="country" class="form-control">
                                    @foreach($country as $value)
                                        <option {{ old('country')==$value->country_id ? 'selected':'' }} value="{{ $value->country_id }}">{{ $value->country_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.certificate')</label>
                                <select name="certificate" class="form-control">
                                    @foreach($certificate as $value)
                                        <option {{ old('certificate')==$value->certificate_id ? 'selected':'' }} value="{{ $value->certificate_id }}">{{ $value->certificate_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group  m-t-20">
                            <div class="col-md-4">
                                <label>@lang('login.hand_graduation')</label>
                                <input class="form-control" type="text" name="handGraduation"
                                       value="{{ old('handGraduation') }}" required=""
                                       placeholder="@lang('login.hand_graduation')">
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.graduation_year')</label>
                                <input class="form-control" type="text" name="graduationYear"
                                       value="{{ old('graduationYear') }}"
                                       required=""
                                       placeholder="@lang('login.graduation_year')">
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.general_appreciation')</label>
                                <input class="form-control" type="text" name="generalAppreciation"
                                       value="{{ old('generalAppreciation') }}"
                                       required=""
                                       placeholder="@lang('login.general_appreciation')">
                            </div>
                        </div>
                        <div class="form-group  m-t-20">
                            <div class="col-md-6">
                                <label>@lang('admin.grade')</label>
                                <select class="form-control select2" name="grade[]" multiple required>
                                    @foreach($grade as $value)
                                        <option {{ old('grade')==$value->grade_id ? 'selected':'' }} value="{{ $value->grade_id }}">                                       
                                        @if(session()->get('user_lang')=='ar')
                                            {{ $value->grade_name }}
                                        @else
                                            {{ $value->grade_name_english }}
                                        @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>@lang('admin.levels')</label>
                                <select class="form-control select2" name="education_level[]" multiple required>
                                    @foreach($education_level as $value)
                                        <option {{ old('education_level')==$value->education_level_id ? 'selected':'' }} value="{{ $value->education_level_id }}">
                                            @if(session()->get('user_lang')=='ar')
                                                {{ $value->education_level_name }}
                                            @else
                                                {{ $value->education_level_name_english }}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group  m-t-20" style="display: -webkit-inline-box;">
                            <div class="col-md-2">
                                <label>@lang('login.job_title')</label>
                                <select name="jobTitle" class="form-control">
                                    @foreach($job as $value)
                                        <option {{ old('jobTitle')==$value->job_id ? 'selected':'' }} value="{{ $value->job_id }}"> 
                                            @if(session()->get('user_lang')=='ar')
                                                {{ $value->job_name }}
                                            @else
                                                {{ $value->job_name_english }}
                                            @endif
                                            </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.scientific_certificate_image')</label>
                                <input class="form-control" type="file" name="scientific_certificate_image[]" multiple
                                       required="">
                            </div>

                            <div class="col-md-4">
                                <label>@lang('login.experience_certificate_image')</label>
                                <input class="form-control" type="file" name="experience_certificate_image[]" multiple
                                       required="">
                            </div>

                            <div class="col-md-2">
                                <label>@lang('login.employer')</label>
                                <input class="form-control" type="text" name="employer" value="{{ old('employer') }}"
                                       required=""
                                       placeholder="@lang('login.employer')">
                            </div>
                        </div>

                        <input type="button" name="previous" class="previous action-button"
                               value="@lang('login.previous')"/>
                        <input type="button" name="next" class="next action-button" value="@lang('login.next')"/>

                    </fieldset>

                    <fieldset>
                        <h2 class="fs-title">@lang('login.finish_setup')</h2>
                        {{--<h3 class="fs-subtitle">Complete your data</h3>--}}


                        <div class="form-group  m-t-20">
                            <h2 class="text-center">@lang('login.previous_work')</h2>
                            <br>
                            <div class="col-md-4">
                                <label>@lang('login.place_name')</label>
                                <input class="form-control" type="text" name="previous_place[]"
                                       required=""
                                       placeholder="@lang('login.place_name')">
                            </div>

                            <div class="col-md-2">
                                <label>@lang('login.from_year')</label>
                                <input class="form-control" type="text" name="from_year[]"
                                       required=""
                                       placeholder="@lang('login.from_year')">
                            </div>

                            <div class="col-md-2">
                                <label>@lang('login.to_year')</label>
                                <input class="form-control" type="text" name="to_year[]"
                                       required=""
                                       placeholder="@lang('login.to_year')">
                            </div>

                            <div class="col-md-3">
                                <label>@lang('login.job_title')</label>
                                <input class="form-control" type="text" name="previous_jobTitle[]"
                                       required=""
                                       placeholder="@lang('login.job_title')">
                            </div>


                            <div class="col-md-1">
                                <label>@lang('login.add_new')</label>
                                <a href="" class="add">
                                    <i class="fa fa-plus fa-3x"></i>
                                </a>
                            </div>

                            <div id="previous_work">

                            </div>
                        </div>

                        <div class="form-group  m-t-20">
                            <h2 class="text-center">@lang('login.training_course')</h2>
                            <br>
                            <div class="col-md-3">
                                <label>@lang('login.course_name')</label>
                                <input class="form-control" type="text" name="course_name[]"
                                       required="" placeholder="@lang('login.course_name')">
                            </div>

                            <div class="col-md-3">
                                <label>@lang('login.course_place')</label>
                                <input class="form-control" type="text" name="course_place[]"
                                       required="" placeholder="@lang('login.course_place')">
                            </div>

                            <div class="col-md-2">
                                <label>@lang('login.course_hour')</label>
                                <input class="form-control" type="text" name="course_hour[]"
                                       required=""
                                       placeholder="@lang('login.course_hour')">
                            </div>

                            <div class="col-md-3">
                                <label>@lang('login.course_appreciation')</label>
                                <input class="form-control" type="text" name="course_appreciation[]"
                                       required=""
                                       placeholder="@lang('login.course_appreciation')">
                            </div>


                            <div class="col-md-1">
                                <label>@lang('login.add_new')</label>
                                <a href="" class="add-new-course">
                                    <i class="fa fa-plus fa-3x"></i>
                                </a>
                            </div>


                            <div id="training_course">

                            </div>
                        </div>

                            <div class="">
                                <input id="checkbox-signup" type="checkbox" required>
                                <label for="checkbox-signup">
                                    @lang('login.agree_all')
                                    <a target="_blank" href="{{url('terms')}}">
                                        @lang('login.register_terms')
                                    </a>
                                </label>
                            </div>

                        <input type="button" name="previous" class="previous action-button"
                               value="@lang('login.previous')"/>
                        <button class="action-button" type="submit">
                            @lang('login.register_btn')
                        </button>
                    </fieldset>
                </form>
                <div class="clear"></div>
            </div>
        </div>
    </section>
@endsection

@section('style')
    <link href="{{ asset('assets') }}/css/my_style.css" id="theme" rel="stylesheet">
@endsection

@section('script')
    <script type="text/javascript">
        $('.add').click(function (e) {
            e.preventDefault();
            $(".previous_work .current-row").clone().appendTo("#previous_work");
        });

        $('.add-new-course').click(function (e) {
            e.preventDefault();
            $(".previous_work .current-row-course").clone().appendTo("#training_course");
        });

        // $('.remove').click(function (e) {
        //     e.preventDefault();
        //     // $(this).closest('.current-row').remove();
        // });
    </script>


@endsection

