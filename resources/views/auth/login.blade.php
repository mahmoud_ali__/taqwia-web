@extends('layouts.app')

@section('title')
    {{ trans('login.title') }}
@endsection

@section('content')
    <section id="wrapper" class="new-login-register">

        <div class="lg-info-panel">
            <div class="inner-panel">
                <a href="javascript:void(0)" class="p-20 di">
                    <img src="{{ asset('assets') }}/Taqwaia.png">
                </a>
                <div class="lg-content">

                </div>
            </div>
        </div>

        <div class="new-login-box">
            <div class="white-box">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session()->has('register'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ session()->get('register') }}</li>
                        </ul>
                    </div>
                @endif

                @if (session()->get('user_lang') == 'ar')
                    <a class="btn" href="{{ url('userLang/en') }}">{{ __('admin.english') }}</a>
                @else
                    <a class="btn" href="{{ url('userLang/ar') }}">{{ __('admin.arabic') }}</a>
                @endif

                <h3 class="box-title m-b-0">{{ __('login.sign_in') }}</h3>

                <form class="form-horizontal new-lg-form" id="loginform" action="" method="post">
                    {{ csrf_field() }}
                    <div class="form-group  m-t-20 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <label>{{ __('login.email') }}</label>
                            <input class="form-control" type="text" name="email" value="{{ old('email') }}"
                                   required=""
                                   placeholder="{{ __('login.email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <label>{{ __('login.password') }}</label>
                            <input class="form-control" type="password" name="password" required=""
                                   placeholder="{{ __('login.password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-info pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> {{ __('login.remember') }} </label>
                            </div>
                            {{--<a href="javascript:void(0)" id="to-recover" class="text-dark pull-right">--}}
                            {{--<i class="fa fa-lock m-r-5"></i> {{ __('login.forget') }}--}}
                            {{--</a>--}}
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light"
                                    type="submit">{{ __('login.login') }}
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a href="javascript:void(0)" class="btn  btn-facebook"
                                   data-toggle="tooltip" title="Login with Facebook">
                                    <i aria-hidden="true" class="fa fa-facebook"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"
                                   title="Login with Google">
                                    <i aria-hidden="true" class="fa fa-google-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>{{ __('login.dont_have') }}
                                <a href="{{ route('register') }}" class="text-primary m-l-5">
                                    <b>{{ __('login.sign_up_as') }}</b>
                                </a>
                            </p>
                        </div>
                    </div>
                </form>

                <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light"
                                    type="submit">Reset
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>


    </section>
@endsection