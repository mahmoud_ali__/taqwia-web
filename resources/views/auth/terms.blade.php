@extends('layouts.app')

@section('title')
    {{ trans('admin.terms') }}
@endsection

@section('content')
    <section id="wrapper" class="step-register">
        <div class="register-box">
            <div class="">
                <form id="msform" action="" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <h2>@lang('login.terms&con')</h2>
                        <br>
                        <h5>{{$setting}}</h5>
                    </fieldset>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('style')
    <link href="{{ asset('assets') }}/css/my_style.css" id="theme" rel="stylesheet">
@endsection

@section('script')
    <script type="text/javascript">
        $('.add').click(function (e) {
            e.preventDefault();
            $(".previous_work .current-row").clone().appendTo("#previous_work");
        });

        $('.add-new-course').click(function (e) {
            e.preventDefault();
            $(".previous_work .current-row-course").clone().appendTo("#training_course");
        });

        // $('.remove').click(function (e) {
        //     e.preventDefault();
        //     // $(this).closest('.current-row').remove();
        // });
    </script>


@endsection

