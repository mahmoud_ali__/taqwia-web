@extends('layouts.master')

@section('title')
    {{ trans('title.main') }}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{ trans('admin.dashboard') }}</h4></div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i
                        class="ti-settings text-white"></i></button>

            <ol class="breadcrumb">
                <li><a href="{{ url('admin') }}">{{ trans('admin.dashboard') }}</a></li>
                <li class="active">{{ trans('admin.dashboard') }} 1</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row row-in">
                    <div class="col-lg-4 col-sm-12 row-in-br">
                        <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-danger"><i class="ti-clipboard"></i></span>
                            </li>
                            <li class="col-last">
                                <h3 class="counter text-right m-t-15">23</h3></li>
                            <li class="col-middle">
                                <h4>{{ trans('admin.total_projects') }}</h4>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-sm-12 row-in-br  b-r-none">
                        <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-info"><i class="ti-wallet"></i></span>
                            </li>
                            <li class="col-last">
                                <h3 class="counter text-right m-t-15">76</h3></li>
                            <li class="col-middle">
                                <h4>{{ trans('admin.total_Earnings') }}</h4>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-sm-12  b-0">
                        <ul class="col-in">
                            <li>
                                <span class="circle circle-md bg-warning"><i class="fa fa-dollar"></i></span>
                            </li>
                            <li class="col-last">
                                <h3 class="counter text-right m-t-15">83</h3></li>
                            <li class="col-middle">
                                <h4>{{ trans('admin.total_Earnings') }}</h4>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection