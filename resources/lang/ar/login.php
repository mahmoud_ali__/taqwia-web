<?php

return [
    'agree_all' => 'أوافق على الكل',
    'register_terms' => 'تسجيل جديد الخصوصية',
    'terms&con' => 'الخصوصية',
    'title' => 'تسجيل الدخول',
    'register' => 'تسجيل',
    'register_btn' => 'تسجيل',
    'sign_in' => 'تسجيل الدخول لتقويه اب',
    'email' => 'البريد الإلكترونى',
    'password' => 'كلمه المرور',
    'remember' => 'تذكرنى',
    'forget' => 'نسيت كلمه المرور؟',
    'login' => 'تسجيل الدخول',
    'dont_have' => 'لا تملك حساب ؟',
    'sign_up_as' => 'التسجيل كمدرس ؟',
    'teacher_account_setup' => 'إنشاء حساب مدرس',
    'study_data' => 'بيانات الدراسه',
    'finish_setup' => 'إنهاء',
    'create_account' => 'إنشاء حسابك',
    'step1' => 'الخطوه 1',
    'name' => 'الإسم',
    'password_confirmation' => 'تأكيد كلمه المرور',
    'personal_image' => 'الصوره الشخصيه',
    'phone' => 'رقم الجوال',
    'birth_date' => 'تاريخ الميلاد',
    'religion' => 'الديانه',
    'about' => 'عنى',
    'next' => 'التالى',
    'previous' => 'السابق',
    'nationality' => 'الجنسيه',
    'country' => 'الدوله',
    'certificate' => 'شهاده',
    'hand_graduation' => 'جهة التخرج',
    'graduation_year' => 'سنه التخرج',
    'general_appreciation' => 'التقدير العام',
    'job_title' => 'المسمى الوظيفي',
    'scientific_certificate_image' => 'صور الشهادات العلمية',
    'experience_certificate_image' => 'صور شهادات الخبرة',
    'employer' => 'جهة العمل',
    'previous_work' => 'سابقة الاعمال',
    'place_name' => 'إسم المكان',
    'from_year' => 'من سنه',
    'to_year' => 'إلى سنه',
    'add_new' => 'إضافه جديد',
    'training_course' => 'الدورات التدريبية',
    'course_name' => 'اسم الدورة',
    'course_place' => 'مكان الدورة',
    'course_hour' => 'الساعات',
    'course_appreciation' => 'التقدير',
    'name_en' => 'الإسم بالإنجليزية',
    'name_ar' => 'الإسم بالعربية',
    'about_en' => 'عني بالإنجليزية',
    'about_ar' => 'عني بالعربية',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',

];