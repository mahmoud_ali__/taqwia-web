<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subjectrequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subject_request', function (Blueprint $table) {
            $table->increments('subject_request_id');
            $table->string('student_id')->nullable();
            $table->string('teacher_id')->nullable();
            $table->string('subject_id')->nullable();
            $table->char('subject_request_status')->comment("1 : wait ; 2 : Accept ; 3 : Refuse")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
