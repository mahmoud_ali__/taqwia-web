<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cat7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('awesome_feature_title_english')->nullable();
            $table->string('awesome_feature_title1_english')->nullable();
            $table->string('awesome_feature_title2_english')->nullable();
            $table->string('awesome_feature_title3_english')->nullable();
            $table->string('awesome_feature_title4_english')->nullable();
            $table->string('awesome_feature_title5_english')->nullable();
            $table->string('awesome_feature_title6_english')->nullable();
            $table->text('awesome_feature_description_english')->nullable();
            $table->text('awesome_feature_description1_english')->nullable();
            $table->text('awesome_feature_description2_english')->nullable();
            $table->text('awesome_feature_description3_english')->nullable();
            $table->text('awesome_feature_description4_english')->nullable();
            $table->text('awesome_feature_description5_english')->nullable();
            $table->text('awesome_feature_description6_english')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
