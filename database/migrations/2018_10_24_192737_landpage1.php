<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Landpage1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landpage1', function (Blueprint $table) {
            $table->increments('id');
            $table->string('landpage_id')->nullable();
            $table->string('about_title_english')->nullable();
            $table->text('about_description_english')->nullable();
            $table->string('about_title1_english')->nullable();
            $table->text('about_description1_english')->nullable();

            $table->string('about_title2_english')->nullable();
            $table->text('about_description2_english')->nullable();
            $table->string('about_title3_english')->nullable();
            $table->text('about_description3_english')->nullable();
            $table->string('screenshot_title_english')->nullable();
            $table->text('screenshot_description_english')->nullable();
            $table->string('team_title_english')->nullable();
            $table->text('team_description_english')->nullable();
            $table->string('testimonial_title_english')->nullable();
            $table->text('testimonial_description_english')->nullable();
            $table->string('faq_title_english')->nullable();
            $table->text('faq_description_english')->nullable();

            $table->string('download_title_english')->nullable();
            $table->text('download_description_english')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
