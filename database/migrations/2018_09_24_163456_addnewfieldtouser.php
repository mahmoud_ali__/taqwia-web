<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addnewfieldtouser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable();
            $table->date('birthDate')->nullable();
            $table->string('religion')->nullable();
            $table->string('handGraduation')->nullable()->comment("جهة التخرج");
            $table->date('graduationYear')->nullable();
            $table->string('generalAppreciation')->nullable()->comment("التقدير العام");
            $table->string('jobTitle')->nullable();
            $table->string('employer')->nullable()->comment("جهة العمل");
            $table->string('personalImage')->nullable();
            $table->string('country_id')->nullable();
            $table->string('nationality_id')->nullable();
        });

        Schema::create('training_course', function (Blueprint $table) {
            $table->string('user_id')->nullable();
            $table->string('name')->nullable();
            $table->string('place')->nullable();
            $table->string('hours')->nullable();
            $table->string('appreciation')->nullable();
        });

        Schema::create('scientific_certificate_image', function (Blueprint $table) {
            $table->string('user_id')->nullable();
            $table->string('image')->nullable();
        });

        Schema::create('experience_certificate_image', function (Blueprint $table) {
            $table->string('user_id')->nullable();
            $table->string('image')->nullable();
        });

        Schema::create('previous_work', function (Blueprint $table) {
            $table->string('user_id')->nullable();
            $table->string('place')->nullable();
            $table->string('from_year')->nullable();
            $table->string('to_year')->nullable();
            $table->string('jobTitle')->nullable();
        });


        Schema::create('certificate', function (Blueprint $table) {
            $table->increments('certificate_id');
            $table->string('certificate_name')->nullable();
        });

        Schema::create('user_certificate', function (Blueprint $table) {
            $table->string('user_id')->nullable();
            $table->string('certificate_id')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
