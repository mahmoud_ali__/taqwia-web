<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cat2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_levels', function (Blueprint $table) {
            $table->string('education_level_name_english')->nullable();
        });

        Schema::table('certificate', function (Blueprint $table) {
            $table->string('certificate_name_english')->nullable();
        });

        Schema::table('grades', function (Blueprint $table) {
            $table->string('grade_name_english')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
