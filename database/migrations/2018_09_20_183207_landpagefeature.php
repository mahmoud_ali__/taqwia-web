<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Landpagefeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('feature_title1')->nullable();
            $table->string('feature_description1')->nullable();
            $table->string('feature_title2')->nullable();
            $table->string('feature_description2')->nullable();
            $table->string('feature_title3')->nullable();
            $table->string('feature_description3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
