<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cat6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('feature_title1_english')->nullable();
            $table->string('feature_title2_english')->nullable();
            $table->string('feature_title3_english')->nullable();
            $table->text('feature_description1_english')->nullable();
            $table->text('feature_description2_english')->nullable();
            $table->text('feature_description3_english')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
