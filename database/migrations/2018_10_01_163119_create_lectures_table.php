<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures', function (Blueprint $table) {
            $table->increments('lecture_id');
            $table->string('user_id')->nullable();
            $table->string('subject_id')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('perHOur')->nullable();
            $table->integer('salary')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('lectures');
    }
}
