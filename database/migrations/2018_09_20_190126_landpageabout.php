<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Landpageabout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('about_title')->nullable();
            $table->string('about_description')->nullable();
            $table->string('about_title1')->nullable();
            $table->text('about_description1')->nullable();
            $table->string('about_image1')->nullable();
            $table->string('about_title2')->nullable();
            $table->text('about_description2')->nullable();
            $table->string('about_image2')->nullable();
            $table->string('about_title3')->nullable();
            $table->text('about_description3')->nullable();
            $table->string('about_image3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
