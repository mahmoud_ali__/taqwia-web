<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Landpagehome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landpage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('home_title')->nullable();
            $table->text('home_description')->nullable();
            $table->string('google_store_link')->nullable();
            $table->string('apple_store_link')->nullable();
            $table->string('home_background')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
