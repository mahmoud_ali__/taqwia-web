<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cat9 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('about_title_english')->nullable();
            $table->text('about_description_english')->nullable();
            $table->string('about_title1_english')->nullable();
            $table->text('about_description1_english')->nullable();
            $table->string('about_title2_english')->nullable();
            $table->text('about_description2_english')->nullable();
            $table->string('about_title3_english')->nullable();
            $table->text('about_description3_english')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
