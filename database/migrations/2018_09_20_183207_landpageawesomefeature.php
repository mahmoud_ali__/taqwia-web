<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Landpageawesomefeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('awesome_feature_title')->nullable();
            $table->string('awesome_feature_description')->nullable();

            $table->string('awesome_feature_title1')->nullable();
            $table->string('awesome_feature_description1')->nullable();
            $table->string('awesome_feature_image1')->nullable();

            $table->string('awesome_feature_title2')->nullable();
            $table->string('awesome_feature_description2')->nullable();
            $table->string('awesome_feature_image2')->nullable();

            $table->string('awesome_feature_title3')->nullable();
            $table->string('awesome_feature_description3')->nullable();
            $table->string('awesome_feature_image3')->nullable();

            $table->string('awesome_feature_title4')->nullable();
            $table->string('awesome_feature_description4')->nullable();
            $table->string('awesome_feature_image4')->nullable();

            $table->string('awesome_feature_title5')->nullable();
            $table->string('awesome_feature_description5')->nullable();
            $table->string('awesome_feature_image5')->nullable();

            $table->string('awesome_feature_title6')->nullable();
            $table->string('awesome_feature_description6')->nullable();
            $table->string('awesome_feature_image6')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
