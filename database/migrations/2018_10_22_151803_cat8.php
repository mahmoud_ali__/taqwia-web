<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cat8 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('landpage', function (Blueprint $table) {
            $table->string('screenshot_title_english')->nullable();
            $table->string('screenshot_description_english')->nullable();
            $table->string('team_title_english')->nullable();
            $table->string('team_description_english')->nullable();
            $table->string('testimonial_title_english')->nullable();
            $table->string('testimonial_description_english')->nullable();
            $table->string('faq_title_english')->nullable();
            $table->string('faq_description_english')->nullable();
            $table->string('download_title_english')->nullable();
            $table->string('download_description_english')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
