/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.1.19-MariaDB : Database - taqwia
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`taqwia` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `taqwia`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`category_id`,`category_name`,`created_at`,`updated_at`,`category_name_english`) values (2,'مواد دراسية','2018-09-24 20:52:30','2018-10-21 18:49:27','studying subjects'),(3,'مواد علميه','2018-09-24 20:52:38','2018-10-21 18:49:56','scientific subjects'),(4,'قسم جديد','2018-10-21 18:52:46','2018-10-21 18:52:46','New Department'),(6,'dd','2018-10-21 18:53:59','2018-10-21 18:53:59','fefe');

/*Table structure for table `certificate` */

DROP TABLE IF EXISTS `certificate`;

CREATE TABLE `certificate` (
  `certificate_id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `certificate_id` (`certificate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `certificate` */

insert  into `certificate`(`certificate_id`,`certificate_name`,`certificate_name_english`) values (2,'تسويق الكترونى','Marketing Programs'),(3,'Data Science',NULL),(4,'Leadership',NULL),(5,'Project Management',NULL),(6,'Human Resources',NULL),(7,'Finance And Business',NULL),(8,'Healthcare',NULL),(9,'Hospitality',NULL),(10,'Nutrition',NULL),(11,'Real Estate',NULL),(12,'Engineering',NULL),(13,'Partner',NULL),(14,'Beekeeping',NULL),(15,'Veterinary',NULL),(16,'SSL',NULL),(17,'شهاده 1','Cer');

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contacts` */

insert  into `contacts`(`id`,`name`,`email`,`subject`,`message`,`created_at`,`updated_at`) values (1,'yousef','yousef.ahmed.mohammed5030@gmail.com','sasasas','asasasasaafa fefewf ewf e','2018-09-23 12:14:21','2018-09-23 12:14:21'),(2,'Yousef Ahmed','yousef.ahmed.mohammed5030@gmail.com','dewdwe','dewdewdwe','2018-09-23 12:15:25','2018-09-23 12:15:25'),(3,'ew','yousef.ahmed.mohammed5030@gmail.com','ew','ewew','2018-09-23 12:15:53','2018-09-23 12:15:53'),(4,'fewfew','yousef.ahmed.mohammed5030@gmail.com','fewf','ewfewfw','2018-09-23 12:18:20','2018-09-23 12:18:20'),(5,'dwedwed','yousef.ahmed.mohammed5030@gmail.com','dewdwe','dewdewd','2018-09-23 12:18:38','2018-09-23 12:18:38'),(6,'Yousef Ahmed','admin@admin.com','dede','dedede','2018-09-23 12:19:37','2018-09-23 12:19:37'),(7,'ddd','test@test.com','sdsdsd','sdsds','2018-09-23 12:19:52','2018-09-23 12:19:52'),(8,'undefined','RRRR@email.com','undefined','undefined','2018-10-07 01:50:58','2018-10-07 01:50:58'),(9,'gggggg','ss@gtg.otkh','dfihhhhhhh','nosrh\n5h4t51g6h45t1g','2018-10-07 01:52:59','2018-10-07 01:52:59');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

/*Data for the table `countries` */

insert  into `countries`(`country_id`,`country_code`,`country_name`) values (1,'AF','Afghanistan'),(2,'AL','Albania'),(3,'DZ','Algeria'),(4,'DS','American Samoa'),(5,'AD','Andorra'),(6,'AO','Angola'),(7,'AI','Anguilla'),(8,'AQ','Antarctica'),(9,'AG','Antigua and Barbuda'),(10,'AR','Argentina'),(11,'AM','Armenia'),(12,'AW','Aruba'),(13,'AU','Australia'),(14,'AT','Austria'),(15,'AZ','Azerbaijan'),(16,'BS','Bahamas'),(17,'BH','Bahrain'),(18,'BD','Bangladesh'),(19,'BB','Barbados'),(20,'BY','Belarus'),(21,'BE','Belgium'),(22,'BZ','Belize'),(23,'BJ','Benin'),(24,'BM','Bermuda'),(25,'BT','Bhutan'),(26,'BO','Bolivia'),(27,'BA','Bosnia and Herzegovina'),(28,'BW','Botswana'),(29,'BV','Bouvet Island'),(30,'BR','Brazil'),(31,'IO','British Indian Ocean Territory'),(32,'BN','Brunei Darussalam'),(33,'BG','Bulgaria'),(34,'BF','Burkina Faso'),(35,'BI','Burundi'),(36,'KH','Cambodia'),(37,'CM','Cameroon'),(38,'CA','Canada'),(39,'CV','Cape Verde'),(40,'KY','Cayman Islands'),(41,'CF','Central African Republic'),(42,'TD','Chad'),(43,'CL','Chile'),(44,'CN','China'),(45,'CX','Christmas Island'),(46,'CC','Cocos (Keeling) Islands'),(47,'CO','Colombia'),(48,'KM','Comoros'),(49,'CG','Congo'),(50,'CK','Cook Islands'),(51,'CR','Costa Rica'),(52,'HR','Croatia (Hrvatska)'),(53,'CU','Cuba'),(54,'CY','Cyprus'),(55,'CZ','Czech Republic'),(56,'DK','Denmark'),(57,'DJ','Djibouti'),(58,'DM','Dominica'),(59,'DO','Dominican Republic'),(60,'TP','East Timor'),(61,'EC','Ecuador'),(62,'EG','Egypt'),(63,'SV','El Salvador'),(64,'GQ','Equatorial Guinea'),(65,'ER','Eritrea'),(66,'EE','Estonia'),(67,'ET','Ethiopia'),(68,'FK','Falkland Islands (Malvinas)'),(69,'FO','Faroe Islands'),(70,'FJ','Fiji'),(71,'FI','Finland'),(72,'FR','France'),(73,'FX','France, Metropolitan'),(74,'GF','French Guiana'),(75,'PF','French Polynesia'),(76,'TF','French Southern Territories'),(77,'GA','Gabon'),(78,'GM','Gambia'),(79,'GE','Georgia'),(80,'DE','Germany'),(81,'GH','Ghana'),(82,'GI','Gibraltar'),(83,'GK','Guernsey'),(84,'GR','Greece'),(85,'GL','Greenland'),(86,'GD','Grenada'),(87,'GP','Guadeloupe'),(88,'GU','Guam'),(89,'GT','Guatemala'),(90,'GN','Guinea'),(91,'GW','Guinea-Bissau'),(92,'GY','Guyana'),(93,'HT','Haiti'),(94,'HM','Heard and Mc Donald Islands'),(95,'HN','Honduras'),(96,'HK','Hong Kong'),(97,'HU','Hungary'),(98,'IS','Iceland'),(99,'IN','India'),(100,'IM','Isle of Man'),(101,'ID','Indonesia'),(102,'IR','Iran (Islamic Republic of)'),(103,'IQ','Iraq'),(104,'IE','Ireland'),(105,'IL','Israel'),(106,'IT','Italy'),(107,'CI','Ivory Coast'),(108,'JE','Jersey'),(109,'JM','Jamaica'),(110,'JP','Japan'),(111,'JO','Jordan'),(112,'KZ','Kazakhstan'),(113,'KE','Kenya'),(114,'KI','Kiribati'),(115,'KP','Korea, Democratic People\'s Republic of'),(116,'KR','Korea, Republic of'),(117,'XK','Kosovo'),(118,'KW','Kuwait'),(119,'KG','Kyrgyzstan'),(120,'LA','Lao People\'s Democratic Republic'),(121,'LV','Latvia'),(122,'LB','Lebanon'),(123,'LS','Lesotho'),(124,'LR','Liberia'),(125,'LY','Libyan Arab Jamahiriya'),(126,'LI','Liechtenstein'),(127,'LT','Lithuania'),(128,'LU','Luxembourg'),(129,'MO','Macau'),(130,'MK','Macedonia'),(131,'MG','Madagascar'),(132,'MW','Malawi'),(133,'MY','Malaysia'),(134,'MV','Maldives'),(135,'ML','Mali'),(136,'MT','Malta'),(137,'MH','Marshall Islands'),(138,'MQ','Martinique'),(139,'MR','Mauritania'),(140,'MU','Mauritius'),(141,'TY','Mayotte'),(142,'MX','Mexico'),(143,'FM','Micronesia, Federated States of'),(144,'MD','Moldova, Republic of'),(145,'MC','Monaco'),(146,'MN','Mongolia'),(147,'ME','Montenegro'),(148,'MS','Montserrat'),(149,'MA','Morocco'),(150,'MZ','Mozambique'),(151,'MM','Myanmar'),(152,'NA','Namibia'),(153,'NR','Nauru'),(154,'NP','Nepal'),(155,'NL','Netherlands'),(156,'AN','Netherlands Antilles'),(157,'NC','New Caledonia'),(158,'NZ','New Zealand'),(159,'NI','Nicaragua'),(160,'NE','Niger'),(161,'NG','Nigeria'),(162,'NU','Niue'),(163,'NF','Norfolk Island'),(164,'MP','Northern Mariana Islands'),(165,'NO','Norway'),(166,'OM','Oman'),(167,'PK','Pakistan'),(168,'PW','Palau'),(169,'PS','Palestine'),(170,'PA','Panama'),(171,'PG','Papua New Guinea'),(172,'PY','Paraguay'),(173,'PE','Peru'),(174,'PH','Philippines'),(175,'PN','Pitcairn'),(176,'PL','Poland'),(177,'PT','Portugal'),(178,'PR','Puerto Rico'),(179,'QA','Qatar'),(180,'RE','Reunion'),(181,'RO','Romania'),(182,'RU','Russian Federation'),(183,'RW','Rwanda'),(184,'KN','Saint Kitts and Nevis'),(185,'LC','Saint Lucia'),(186,'VC','Saint Vincent and the Grenadines'),(187,'WS','Samoa'),(188,'SM','San Marino'),(189,'ST','Sao Tome and Principe'),(190,'SA','Saudi Arabia'),(191,'SN','Senegal'),(192,'RS','Serbia'),(193,'SC','Seychelles'),(194,'SL','Sierra Leone'),(195,'SG','Singapore'),(196,'SK','Slovakia'),(197,'SI','Slovenia'),(198,'SB','Solomon Islands'),(199,'SO','Somalia'),(200,'ZA','South Africa'),(201,'GS','South Georgia South Sandwich Islands'),(202,'SS','South Sudan'),(203,'ES','Spain'),(204,'LK','Sri Lanka'),(205,'SH','St. Helena'),(206,'PM','St. Pierre and Miquelon'),(207,'SD','Sudan'),(208,'SR','Suriname'),(209,'SJ','Svalbard and Jan Mayen Islands'),(210,'SZ','Swaziland'),(211,'SE','Sweden'),(212,'CH','Switzerland'),(213,'SY','Syrian Arab Republic'),(214,'TW','Taiwan'),(215,'TJ','Tajikistan'),(216,'TZ','Tanzania, United Republic of'),(217,'TH','Thailand'),(218,'TG','Togo'),(219,'TK','Tokelau'),(220,'TO','Tonga'),(221,'TT','Trinidad and Tobago'),(222,'TN','Tunisia'),(223,'TR','Turkey'),(224,'TM','Turkmenistan'),(225,'TC','Turks and Caicos Islands'),(226,'TV','Tuvalu'),(227,'UG','Uganda'),(228,'UA','Ukraine'),(229,'AE','United Arab Emirates'),(230,'GB','United Kingdom'),(231,'US','United States'),(232,'UM','United States minor outlying islands'),(233,'UY','Uruguay'),(234,'UZ','Uzbekistan'),(235,'VU','Vanuatu'),(236,'VA','Vatican City State'),(237,'VE','Venezuela'),(238,'VN','Vietnam'),(239,'VG','Virgin Islands (British)'),(240,'VI','Virgin Islands (U.S.)'),(241,'WF','Wallis and Futuna Islands'),(242,'EH','Western Sahara'),(243,'YE','Yemen'),(244,'ZR','Zaire'),(245,'ZM','Zambia'),(246,'ZW','Zimbabwe');

/*Table structure for table `education_levels` */

DROP TABLE IF EXISTS `education_levels`;

CREATE TABLE `education_levels` (
  `education_level_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `education_level_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `education_level_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`education_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `education_levels` */

insert  into `education_levels`(`education_level_id`,`education_level_name`,`created_at`,`updated_at`,`education_level_name_english`) values (2,'المستوى1','2018-10-06 23:09:46','2018-10-21 20:58:08','Level 1'),(3,'Level 2','2018-10-06 23:09:50','2018-10-06 23:09:50',NULL),(4,'Level 3','2018-10-06 23:09:55','2018-10-06 23:09:55',NULL),(5,'Level 4','2018-10-06 23:10:03','2018-10-06 23:10:09',NULL),(6,'Level 5','2018-10-06 23:10:14','2018-10-06 23:10:14',NULL),(7,'المستوى الجديد','2018-10-21 20:58:30','2018-10-21 20:58:30','Level New');

/*Table structure for table `experience_certificate_image` */

DROP TABLE IF EXISTS `experience_certificate_image`;

CREATE TABLE `experience_certificate_image` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `experience_certificate_image` */

insert  into `experience_certificate_image`(`user_id`,`image`) values ('24','092620182032225babecd6ecd3f.jpg'),('24','092620182032225babecd6ed723.jpg'),('26','092620182100525babf3843ecd1.jpg'),('26','092620182100525babf3843f5a5.jpg');

/*Table structure for table `faqs` */

DROP TABLE IF EXISTS `faqs`;

CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `question_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer_english` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `faqs` */

insert  into `faqs`(`id`,`question`,`answer`,`created_at`,`updated_at`,`question_english`,`answer_english`) values (1,'نحن على استعداد للبدء الآن','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','2018-09-22 16:54:20','2018-10-22 17:56:11','We\'re ready to start now','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei\r\n                              temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim exercitation\r\n                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute dolor in\r\n                                reprehenderit in voluptate velit esse cillum dolore'),(5,'نحن على استعداد للبدء الآن','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','2018-09-22 17:00:49','2018-10-22 17:57:02','We\'re ready to start now','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do ei\r\n                                temporo incididunt ut labore et dolore magna aliqua. Ut enim ad minim exercitation\r\n                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute dolor in\r\n                                reprehenderit in voluptate velit esse cillum dolore'),(6,'السوال الاول','الاجابه الاولى','2018-10-22 17:51:36','2018-10-22 17:51:36','question 1','Answer 1');

/*Table structure for table `grades` */

DROP TABLE IF EXISTS `grades`;

CREATE TABLE `grades` (
  `grade_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `grade_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`grade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `grades` */

insert  into `grades`(`grade_id`,`grade_name`,`created_at`,`updated_at`,`grade_name_english`) values (1,'درجه 1','2018-10-06 23:17:41','2018-10-21 21:01:37','grade 1'),(2,'grade 2','2018-10-06 23:20:00','2018-10-06 23:20:00',NULL),(3,'grade 3','2018-10-06 23:20:05','2018-10-06 23:20:05',NULL),(4,'grade 4','2018-10-06 23:20:09','2018-10-06 23:20:09',NULL),(5,'grade 5','2018-10-06 23:20:15','2018-10-06 23:20:15',NULL),(6,'grade 6','2018-10-06 23:20:19','2018-10-06 23:20:19',NULL),(7,'grade 7','2018-10-06 23:20:24','2018-10-06 23:20:24',NULL),(8,'grade 8','2018-10-06 23:20:29','2018-10-06 23:20:29',NULL),(9,'grade 9','2018-10-06 23:20:33','2018-10-06 23:20:33',NULL),(11,'grade 10','2018-10-06 23:21:10','2018-10-06 23:21:10',NULL),(12,'درجه 252','2018-10-21 21:00:44','2018-10-21 21:00:44','Grade 52');

/*Table structure for table `landpage` */

DROP TABLE IF EXISTS `landpage`;

CREATE TABLE `landpage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `home_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_description` text COLLATE utf8mb4_unicode_ci,
  `google_store_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apple_store_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_background` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_image6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description3` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `screenshot_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `screenshot_description` text COLLATE utf8mb4_unicode_ci,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` text COLLATE utf8mb4_unicode_ci,
  `total_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loyal_customer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_achivement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_ratting` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci,
  `faq_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_description` text COLLATE utf8mb4_unicode_ci,
  `download_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_description_english` text COLLATE utf8mb4_unicode_ci,
  `feature_title1_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title2_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_title3_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_description1_english` text COLLATE utf8mb4_unicode_ci,
  `feature_description2_english` text COLLATE utf8mb4_unicode_ci,
  `feature_description3_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title1_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title2_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title3_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title4_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title5_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_title6_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awesome_feature_description_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_description1_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_description2_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_description3_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_description4_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_description5_english` text COLLATE utf8mb4_unicode_ci,
  `awesome_feature_description6_english` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;

/*Data for the table `landpage` */

insert  into `landpage`(`id`,`home_title`,`home_description`,`google_store_link`,`apple_store_link`,`home_background`,`awesome_feature_title`,`awesome_feature_description`,`awesome_feature_title1`,`awesome_feature_description1`,`awesome_feature_image1`,`awesome_feature_title2`,`awesome_feature_description2`,`awesome_feature_image2`,`awesome_feature_title3`,`awesome_feature_description3`,`awesome_feature_image3`,`awesome_feature_title4`,`awesome_feature_description4`,`awesome_feature_image4`,`awesome_feature_title5`,`awesome_feature_description5`,`awesome_feature_image5`,`awesome_feature_title6`,`awesome_feature_description6`,`awesome_feature_image6`,`feature_title1`,`feature_description1`,`feature_title2`,`feature_description2`,`feature_title3`,`feature_description3`,`feature_image1`,`feature_image2`,`feature_image3`,`about_title`,`about_description`,`about_title1`,`about_description1`,`about_image1`,`about_title2`,`about_description2`,`about_image2`,`about_title3`,`about_description3`,`about_image3`,`awesome_feature_video`,`screenshot_title`,`screenshot_description`,`team_title`,`team_description`,`total_user`,`loyal_customer`,`total_achivement`,`app_ratting`,`testimonial_title`,`testimonial_description`,`faq_title`,`faq_description`,`download_title`,`download_description`,`home_title_english`,`home_description_english`,`feature_title1_english`,`feature_title2_english`,`feature_title3_english`,`feature_description1_english`,`feature_description2_english`,`feature_description3_english`,`awesome_feature_title_english`,`awesome_feature_title1_english`,`awesome_feature_title2_english`,`awesome_feature_title3_english`,`awesome_feature_title4_english`,`awesome_feature_title5_english`,`awesome_feature_title6_english`,`awesome_feature_description_english`,`awesome_feature_description1_english`,`awesome_feature_description2_english`,`awesome_feature_description3_english`,`awesome_feature_description4_english`,`awesome_feature_description5_english`,`awesome_feature_description6_english`) values (1,'الأمر كله يتعلق بالترويج لنشاطك التجاري','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','https://play.google.com','https://itunes.apple.com','092020181959585ba3fc3ec92d5.jpg','ميزات رائعة','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','عالية الدقة','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020181957235ba3fba3bccec.svg','التصميم الإبداعي','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020181957235ba3fba3bd18d.svg','بكسل الكمال','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020181957235ba3fba3bd3ee.svg','رموز نظيفة','وريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020181957235ba3fba3bd66c.svg','راتينا جاهز','وريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020181957235ba3fba3bd8ce.svg','كامل الدردشة مجانا','وريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020181957235ba3fba3bdb01.svg','التصميم الإبداعي','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','تجربة مجانية لمدة شهرين','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','دعم على مدار الساعة طوال أيام الأسبوع','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر','092020182008005ba3fe20a8e08.svg','092020182008005ba3fe20a9028.svg','092020182008005ba3fe20a929e.svg','كيف تعمل Appton؟','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر','أفضل تصميم واجهة المستخدم','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة','092020182010305ba3feb6c49be.png','أفضل تصميم واجهة المستخدم','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة','092020182010305ba3feb6c4dd5.png','تحليل البيانات','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة','092020182010305ba3feb6c5112.png','LTEfxsWdZzQ','لقطة الشاشة','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النش','أعضاء الفريق','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النش','1350','1300','1150','1000','الشهادات - التوصيات','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النش','الأسئله الشائعه','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النش','تحميل التطبيق','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النش','It’s all about Promote your Business','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor anagi icdunt ut labore et dolore magna aliqua.','Creative Design','2 Month Free Trail','24/7 Hour Support','Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt !','Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt !','Lorem ipsum dolor sit amt, consectet adop adipisicing elit, semeid do eiusmod porara incididunt !','Awesome Features','High Resolution','Creative Design','Pixel Parfect','Clean Codes','Ratina Ready','Full Chat Free','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.','Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!','Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!','Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!','Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!','Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!','Lorem ipsum dolor sit amt, consectet op adipisicing elit, semeid do eiusmod porara incididunt lorem opsum damy!');

/*Table structure for table `landpage1` */

DROP TABLE IF EXISTS `landpage1`;

CREATE TABLE `landpage1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `landpage_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description_english` text COLLATE utf8mb4_unicode_ci,
  `about_title1_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description1_english` text COLLATE utf8mb4_unicode_ci,
  `about_title2_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description2_english` text COLLATE utf8mb4_unicode_ci,
  `about_title3_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description3_english` text COLLATE utf8mb4_unicode_ci,
  `screenshot_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `screenshot_description_english` text COLLATE utf8mb4_unicode_ci,
  `team_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description_english` text COLLATE utf8mb4_unicode_ci,
  `testimonial_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial_description_english` text COLLATE utf8mb4_unicode_ci,
  `faq_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_description_english` text COLLATE utf8mb4_unicode_ci,
  `download_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download_description_english` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `landpage1` */

insert  into `landpage1`(`id`,`landpage_id`,`about_title_english`,`about_description_english`,`about_title1_english`,`about_description1_english`,`about_title2_english`,`about_description2_english`,`about_title3_english`,`about_description3_english`,`screenshot_title_english`,`screenshot_description_english`,`team_title_english`,`team_description_english`,`testimonial_title_english`,`testimonial_description_english`,`faq_title_english`,`faq_description_english`,`download_title_english`,`download_description_english`) values (1,'1','How Appton Work?','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.','Best User Interface Design','Lorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor sereit amet, consectetur','Best User Interface Design','Lorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor sereit amet, consectetur','Data Analysis','Lorem ipsum madolor sit amet, consectetur adipisicing elitid do eiumod tempor coli incidit labore Lorem ipsum amet mdolor sit amet Lorem ipsum madolor sereit amet, consectetur','App Screenshot','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.','Team Member','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.','Testimonials','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.','Frequently Asked Questions','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.','Download Appton Today','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet madolor sit amet.');

/*Table structure for table `lectures` */

DROP TABLE IF EXISTS `lectures`;

CREATE TABLE `lectures` (
  `lecture_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lecture_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `lectures` */

insert  into `lectures`(`lecture_id`,`created_at`,`updated_at`,`user_id`,`subject_id`) values (3,'2018-10-08 03:58:32','2018-10-08 04:23:14','26','2'),(4,'2018-10-08 03:59:33','2018-10-08 03:59:33','26','1'),(5,'2018-10-08 04:19:09','2018-10-08 04:19:09','24','1'),(6,'2018-10-08 04:19:16','2018-10-08 04:19:16','24','2'),(7,'2018-10-08 06:01:02','2018-10-08 06:01:02','24','3');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_09_20_130831_landpagehome',1),(4,'2018_09_20_183207_landpageawesomefeature',1),(5,'2018_09_20_183207_landpagefeature',1),(6,'2018_09_20_184450_landpagefeature1',1),(7,'2018_09_20_190126_landpageabout',1),(8,'2018_09_20_201753_awesomefeature1',2),(9,'2018_09_20_202611_landpage2',3),(10,'2018_09_20_204034_screenshot',3),(11,'2018_09_20_213501_team',4),(12,'2018_09_20_221646_landpage3',5),(13,'2018_09_21_155054_create_testimonials_table',6),(15,'2018_09_22_150159_ss',7),(17,'2018_09_22_155507_create_faqs_table',8),(18,'2018_09_22_170827_create_subscribes_table',9),(19,'2018_09_23_120848_create_contacts_table',10),(21,'2018_09_23_123532_create_settings_table',11),(22,'2018_09_24_163456_addnewfieldtouser',12),(26,'2018_09_24_203934_create_categories_table',13),(27,'2018_09_24_210401_subcategory',14),(30,'2018_09_26_222004_create_packages_table',15),(31,'2018_09_29_211638_acc_type',16),(32,'2018_10_01_163119_create_lectures_table',17),(33,'2018_10_01_221218_terms',18),(34,'2018_10_06_225658_create_education_levels_table',19),(35,'2018_10_06_231151_create_grades_table',20),(36,'2018_10_06_232431_create_relations_table',21),(37,'2018_09_22_150159_sss',22),(38,'2018_09_22_150159_s1',23),(39,'2018_10_07_015548_create_sliders_table',24),(40,'2018_10_08_014910_create_subjects_table',25),(41,'2018_10_10_152405_student_subject',26),(42,'2018_10_10_153938_online',27),(43,'2018_10_11_232541_abotcol',28),(44,'2018_10_14_181932_subjectrequest',29),(45,'2018_09_22_150159_s2',30),(46,'2018_10_21_184313_cat1',31),(47,'2018_10_21_204945_cat2',32),(48,'2018_10_21_210259_cat3',33),(49,'2018_10_21_211917_cat4',34),(50,'2018_10_21_213430_cat5',35),(51,'2018_10_21_220520_cat6',36),(52,'2018_10_21_222038_cat7',37),(53,'2018_10_22_143326_cat9',38),(54,'2018_10_22_151803_cat8',39),(55,'2018_10_22_174543_cat10',40),(56,'2018_10_22_175808_cat11',41),(57,'2018_10_24_192737_landpage1',42),(58,'2018_10_29_173448_ccccc2',43);

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `package_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `package_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `packages` */

insert  into `packages`(`package_id`,`package_name`,`package_hours`,`package_price`,`created_at`,`updated_at`,`package_name_english`) values (1,'الباقه الرئيسيه','10','100','2018-09-26 23:09:35','2018-10-21 21:06:38','Main Package'),(2,'Main Package 2','5','100','2018-10-14 16:41:40','2018-10-14 16:41:40',NULL),(3,'الباقه الذهبيه','10','100','2018-10-21 21:07:02','2018-10-21 21:07:02','Gold Package');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `previous_work` */

DROP TABLE IF EXISTS `previous_work`;

CREATE TABLE `previous_work` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jobTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `previous_work` */

insert  into `previous_work`(`user_id`,`place`,`from_year`,`to_year`,`jobTitle`) values ('24','place 1','2015','2017','rgegre'),('26','place 1','2015','2017','sadadadsa');

/*Table structure for table `relations` */

DROP TABLE IF EXISTS `relations`;

CREATE TABLE `relations` (
  `relation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relation_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `relation_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `relations` */

insert  into `relations`(`relation_id`,`relation_name`,`created_at`,`updated_at`,`relation_name_english`) values (2,'علاقه 1','2018-10-06 23:33:49','2018-10-21 21:10:24','relation 1'),(3,'relation 2','2018-10-06 23:33:54','2018-10-06 23:33:54',NULL),(4,'relation 3','2018-10-06 23:33:59','2018-10-06 23:33:59',NULL),(5,'relation 4','2018-10-06 23:34:03','2018-10-06 23:34:03',NULL),(6,'relation 5','2018-10-06 23:34:08','2018-10-06 23:34:08',NULL),(7,'relation 6','2018-10-06 23:34:14','2018-10-06 23:34:14',NULL),(8,'relation 7','2018-10-06 23:34:21','2018-10-06 23:34:21',NULL),(9,'relation 8','2018-10-06 23:34:26','2018-10-06 23:34:26',NULL),(10,'relation 9','2018-10-06 23:34:30','2018-10-06 23:34:49',NULL),(11,'relation 10','2018-10-06 23:34:56','2018-10-06 23:34:56',NULL),(12,'الصله 5','2018-10-21 21:10:40','2018-10-21 21:10:40','R 55');

/*Table structure for table `scientific_certificate_image` */

DROP TABLE IF EXISTS `scientific_certificate_image`;

CREATE TABLE `scientific_certificate_image` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `scientific_certificate_image` */

insert  into `scientific_certificate_image`(`user_id`,`image`) values ('24','092620182032225babecd6d925e.jpg'),('24','092620182032225babecd6d97f6.jpg'),('26','092620182100525babf384164c2.jpg'),('26','092620182100525babf3841716b.jpg');

/*Table structure for table `screenshots` */

DROP TABLE IF EXISTS `screenshots`;

CREATE TABLE `screenshots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `screenshots` */

insert  into `screenshots`(`id`,`image`) values (9,'092020182116435ba40e3b76398.jpg'),(10,'092020182116485ba40e400faf9.jpg'),(11,'092020182116525ba40e4486c0e.jpg'),(12,'092020182116575ba40e49bfb47.jpg'),(13,'092020182117025ba40e4e90c18.jpg'),(14,'092020182117075ba40e5333570.jpg'),(15,'092020182117135ba40e599a813.jpg'),(16,'092020182117215ba40e61292bc.jpg');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci,
  `privacy_policy` text COLLATE utf8mb4_unicode_ci,
  `help_center` text COLLATE utf8mb4_unicode_ci,
  `terms_english` text COLLATE utf8mb4_unicode_ci,
  `about_us_english` text COLLATE utf8mb4_unicode_ci,
  `privacy_policy_english` text COLLATE utf8mb4_unicode_ci,
  `help_center_english` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`phone`,`address`,`email`,`facebook`,`twitter`,`linkedin`,`pinterest`,`google`,`created_at`,`updated_at`,`terms`,`about_us`,`privacy_policy`,`help_center`,`terms_english`,`about_us_english`,`privacy_policy_english`,`help_center_english`) values (1,'+0044 545 989 62698','28 Green Tower, Street Name New York City, USA','yourmail@gmail.com','https://www.facebook.com/','https://twitter.com/','https://www.linkedin.com','https://www.pinterest.com/','https://www.google.com/',NULL,'2018-10-29 17:52:13','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

/*Table structure for table `sliders` */

DROP TABLE IF EXISTS `sliders`;

CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sliders` */

insert  into `sliders`(`id`,`image`,`created_at`,`updated_at`) values (3,'100720180215175bb96c35ca62f.png','2018-10-07 02:08:30','2018-10-07 02:15:17'),(4,'100720180215265bb96c3e28062.png','2018-10-07 02:10:03','2018-10-07 02:15:26'),(5,'100720180215335bb96c4500698.png','2018-10-07 02:15:33','2018-10-07 02:15:33'),(7,'100720180215515bb96c57e39b6.png','2018-10-07 02:15:51','2018-10-07 02:15:51');

/*Table structure for table `student_subject` */

DROP TABLE IF EXISTS `student_subject`;

CREATE TABLE `student_subject` (
  `student_subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `student_subject_id` (`student_subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `student_subject` */

insert  into `student_subject`(`student_subject_id`,`user_id`,`subject_id`) values (4,'36','1'),(5,'36','2');

/*Table structure for table `sub_categories` */

DROP TABLE IF EXISTS `sub_categories`;

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_name_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sub_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sub_categories` */

insert  into `sub_categories`(`sub_category_id`,`sub_category_name`,`category_id`,`sub_category_name_english`) values (3,'بى اتش بى','3','php'),(4,'Math','2',NULL),(5,'C ++','3',NULL),(6,'Java','3',NULL),(7,'Object Oriented','3',NULL),(8,'قسم فرعى جديد','4','New Sub Department'),(9,'قسم فرعى','2','Sub category');

/*Table structure for table `subject_request` */

DROP TABLE IF EXISTS `subject_request`;

CREATE TABLE `subject_request` (
  `subject_request_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_request_status` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 : wait ; 2 : Accept ; 3 : Refuse',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `selectedHour` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_request_date` date DEFAULT NULL,
  PRIMARY KEY (`subject_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subject_request` */

insert  into `subject_request`(`subject_request_id`,`student_id`,`teacher_id`,`subject_id`,`subject_request_status`,`created_at`,`updated_at`,`selectedHour`,`time`,`subject_request_date`) values (1,'36','24','2','2','2018-10-17 14:17:04','2018-10-17 15:16:49','4','PM','2018-10-20'),(2,'36','26','1','3','2018-10-17 14:42:39','2018-10-17 15:21:34','7','AM','2018-10-23');

/*Table structure for table `subjects` */

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `subject_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_hour` int(11) DEFAULT NULL,
  `subject_description` text COLLATE utf8mb4_unicode_ci,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_category_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subject_title_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_description_english` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subjects` */

insert  into `subjects`(`subject_id`,`subject_title`,`subject_hour`,`subject_description`,`category_id`,`sub_category_id`,`created_at`,`updated_at`,`subject_title_english`,`subject_description_english`) values (1,'أساسيات بى اتش بى',2,'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','3','3','2018-10-08 02:09:22','2018-10-21 21:27:43','Php Syntax','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'),(2,'PHP 5 Strings',1,'PHP String Functions\r\nIn this chapter we will look at some commonly used functions to manipulate strings.','3','3','2018-10-08 03:59:19','2018-10-08 03:59:19',NULL,NULL),(3,'Math 2',5,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','2','4','2018-10-08 06:00:35','2018-10-08 06:00:35',NULL,NULL),(4,'ماده 545',58,'الوصف','2','4','2018-10-21 21:28:39','2018-10-21 21:28:39','Subj 25','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).');

/*Table structure for table `subscribes` */

DROP TABLE IF EXISTS `subscribes`;

CREATE TABLE `subscribes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `subscribes` */

insert  into `subscribes`(`id`,`email`,`created_at`,`updated_at`) values (1,'email@example.com','2018-09-23 11:54:50','2018-09-23 11:54:50');

/*Table structure for table `teams` */

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `teams` */

insert  into `teams`(`id`,`image`,`name`,`job`,`facebook`,`twitter`,`pinterest`,`linkedin`) values (2,'092020182205565ba419c420cb7.jpg','Jonshon Adams','Asst Designer','https://www.facebook.com','https://twitter.com','https://www.pinterest.com','https://www.linkedin.com'),(3,'092020182200115ba4186b7111a.jpg','Jonshon Adams','Asst Designer','https://www.facebook.com/','https://twitter.com','https://www.pinterest.com/','https://www.linkedin.com'),(4,'092020182200455ba4188dd0c12.jpg','Jonshon Adams','Asst Designer','https://www.facebook.com/','https://twitter.com','https://www.pinterest.com/','https://www.linkedin.com'),(5,'092020182201045ba418a063886.jpg','Jonshon Adams','Asst Designer','https://www.facebook.com/','https://twitter.com','https://www.pinterest.com/','https://www.linkedin.com');

/*Table structure for table `testimonials` */

DROP TABLE IF EXISTS `testimonials`;

CREATE TABLE `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `link_text_english` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_english` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `testimonials` */

insert  into `testimonials`(`id`,`author`,`image`,`link_text`,`link`,`description`,`created_at`,`updated_at`,`link_text_english`,`description_english`) values (4,'Michael Clarke','092220181454255ba657a1aed0d.jpg','ثيم فورست','#','لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.','2018-09-22 14:54:25','2018-10-22 18:13:13','themeforest.com','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli\r\n                                incidit labore Lorem ipsum amet madolor sit amet Lorem ipsum madolor sit amet,\r\n                                consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet\r\n                                madolor sit amet'),(5,'Michael Clarke','092220181454585ba657c278637.jpg','themeforest.com','#','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli\r\n                                incidit labore Lorem ipsum amet madolor sit amet Lorem ipsum madolor sit amet,\r\n                                consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet\r\n                                madolor sit amet','2018-09-22 14:54:58','2018-09-22 14:54:58',NULL,NULL),(6,'Michael Clarke','092220181455255ba657dd2cfbd.jpg','themeforest.com','#','Lorem ipsum madolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor coli\r\n                                incidit labore Lorem ipsum amet madolor sit amet Lorem ipsum madolor sit amet,\r\n                                consectetur adipisicing elit, sed do eiusmod tempor coli incidit labore Lorem ipsum amet\r\n                                madolor sit amet','2018-09-22 14:55:25','2018-09-22 14:55:25',NULL,NULL);

/*Table structure for table `training_course` */

DROP TABLE IF EXISTS `training_course`;

CREATE TABLE `training_course` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appreciation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `training_course` */

insert  into `training_course`(`user_id`,`name`,`place`,`hours`,`appreciation`) values ('24','course 1','course place 1','hour 1','COURSE APPRECIATION 1'),('26','course 1','course place 1','hour 1','COURSE APPRECIATION 1');

/*Table structure for table `user_certificate` */

DROP TABLE IF EXISTS `user_certificate`;

CREATE TABLE `user_certificate` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_certificate` */

insert  into `user_certificate`(`user_id`,`certificate_id`) values ('24','2'),('26','14');

/*Table structure for table `user_package` */

DROP TABLE IF EXISTS `user_package`;

CREATE TABLE `user_package` (
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_package` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1 : Admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `open` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `religion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `handGraduation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'جهة التخرج',
  `graduationYear` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generalAppreciation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'التقدير العام',
  `jobTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'جهة العمل',
  `personalImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accountType` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 : Student ; 2 : Parent',
  `perHOur` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` int(191) DEFAULT '0',
  `education_level_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relation_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 : Online ; 2 : Offline',
  `about` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`role`,`remember_token`,`created_at`,`updated_at`,`open`,`phone`,`birthDate`,`religion`,`handGraduation`,`graduationYear`,`generalAppreciation`,`jobTitle`,`employer`,`personalImage`,`country_id`,`nationality_id`,`accountType`,`perHOur`,`salary`,`education_level_id`,`grade_id`,`school_area`,`school_name`,`relation_id`,`online`,`about`) values (1,'admin','admin@admin.com','$2y$10$VCb6XYrSi5xZ0i2.YEDHIegbAR3JyBfY/zr5IuAV2j5cm8ibfDUjW','1','IdJeFeIeYuGxBZn101fFO74ecpZVIrCPU40HNeo7Ml2ZoYufn6ayMcTwWOK3',NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL),(24,'Yousef Ahmed','yousef.ahmed.mohammed5030@gmail.com','$2y$10$vRV1SCB4abhr8MXDrYKnoeeOjlMOxXlLz4t3x/yBJZ3MbRA1MzRQi','2','Db7PuYgNR84TATdwxM12fL10EypbMrPZeBRyy7eVJom2WQQ1OP0rYb0pQmcT','2018-09-26 20:32:22','2018-10-21 16:33:01','1','01273975420','2018-09-25','wwwww','sdsd','2016','ddddddd','cd','dddd','092620182032225babecd6bdb04.jpg','3','14',NULL,'55',NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL),(26,'Aya Ismail','aya@admin.com','$2y$10$pwlcdMAyjQOLBxOA0D//wua3.r81j2zIYc.uR41gA3KOBWAi1UHcO','2','lDhb6l0cDruciHdFV11EweBggSoJ1XKHjHLD3WAE7LbUGaXawc7gqQVnuBxp','2018-09-26 21:00:51','2018-10-21 16:33:02','0','01273975420','2018-09-25','wwwww','HAND GRADUATION','2016','GENERAL APPRECIATION','Developer','ssssdeed','092620182213305bac048ac1b34.jpg','36','62',NULL,'22',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL),(31,'yousef','parent@email.com','$2y$10$.wagdTIvReZuR8itTz/QA.98YTJZzf/.ttseM0IULS8XX.7AJxjlK','3',NULL,'2018-09-29 22:42:53','2018-10-21 16:41:16','1','122589562',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','4','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL),(32,'Ismail dd','dd@yahoo.com','$2y$10$QSuaD6q8Qldzq068mccT4.xSGgzYFrD.x5V/GOzj/fnMmh6az2fDW','3',NULL,'2018-10-02 05:39:53','2018-10-02 05:39:53','1','1222252355',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4','13','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL),(33,'student','student@gmail.com','$2y$10$r/pF/IIWISn2tLVuB.uhoeBKVJxZkpBg4lmDi1mJEJAbu9aRGX5ku','3',NULL,'2018-10-07 00:03:42','2018-10-07 04:03:12','1','451245',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4','1','2',NULL,NULL,'5','6','mmmmm','rrrrrr','9','',NULL),(34,'student subject','student@rr.com','$2y$10$mjP.WlTjCb5VVdIumBXLveK1h1isjBycr9PidRgQB74Ns8TUoPKZu','3',NULL,'2018-10-10 15:27:30','2018-10-10 15:27:30','1','596235',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL),(35,'student subject','studenft@rr.com','$2y$10$lIZYSZp.64JxyznU1D7oUenJWeXiOERjr1zdN44RlpvnlTVjpw9/y','3',NULL,'2018-10-10 15:28:45','2018-10-10 15:29:02','1','596235',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2','1',NULL,NULL,'3','4','hhhhh','sdfgnm','5','',NULL),(36,'salma subject','salma@email.com','$2y$10$lZw4ep15Hd4888HYvrn0gOg3eaOuVwpOK8bb3ZrvfHIiwWRe9gbk6','3',NULL,'2018-10-10 15:31:24','2018-10-14 17:48:31','1','596235',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2','1',NULL,15,'5','2','mra3y','ssss','6','',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
