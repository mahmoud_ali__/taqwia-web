<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model
{
    protected $table = 'education_levels';
    protected $primaryKey = 'education_level_id';
    protected $fillable = [
        'education_level_name',
        'education_level_name_english',
    ];
}
