<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Student extends Model
{

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'country_id',
        'nationality_id',
        'accountType',
        'education_level_id',
        'grade_id',
        'school_area',
        'school_name',
        'relation_id',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(function (Builder $builder) {
            $builder->where('role', 3);
        });
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt(request('password'));
    }

    public function subject()
    {
        return $this->belongsToMany(Subject::class, 'student_subject', 'user_id', 'subject_id');
    }
    public function reviews()
    {
        return $this->belongsToMany(Teacher::class, 'review', 'user_id', 'Teacher_id');
    }

    public function nationality()
    {
        return $this->belongsTo('App\Models\Country', 'nationality_id');
    }

    public function educationLevel()
    {
        return $this->belongsTo(EducationLevel::class, 'education_level_id');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class, 'grade_id');
    }

    public function relation()
    {
        return $this->belongsTo(Relation::class, 'relation_id');
    }
    

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }
}
