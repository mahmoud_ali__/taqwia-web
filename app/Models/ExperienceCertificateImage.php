<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExperienceCertificateImage extends Model
{
    protected $table = 'experience_certificate_image';
    protected $fillable = [
        'user_id',
        'image',
    ];

    public $timestamps=false;

}
