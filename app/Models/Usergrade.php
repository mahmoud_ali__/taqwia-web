<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usergrade extends Model
{
    protected $table = 'user_grades';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'grade',
    ];

    public $timestamps = false;

}
