<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingCourse extends Model
{
    protected $table = 'training_course';
    protected $fillable = [
        'user_id',
        'name',
        'place',
        'hours',
        'appreciation',
    ];

    public $timestamps=false;
}
