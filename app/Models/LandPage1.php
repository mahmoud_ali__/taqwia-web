<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandPage1 extends Model
{
    protected $table = 'landpage1';

    protected $fillable =
        [
            'about_title_english',
            'about_description_english',
            'about_title1_english',
            'about_title1_english',
            'about_description1_english',
            'about_title2_english',
            'about_description2_english',
            'about_title3_english',
            'about_description3_english',
            'screenshot_title_english',
            'screenshot_description_english',
            'team_title_english',
            'team_description_english',
            'testimonial_title_english',
            'testimonial_description_english',
            'faq_title_english',
            'faq_description_english',
            'download_title_english',
            'download_description_english',
        ];

    public $timestamps = false;


}
