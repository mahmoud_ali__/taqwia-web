<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';
    protected $primaryKey = 'sub_category_id';
    protected $fillable = [
        'sub_category_name',
        'sub_category_name_english',
        'category_id'
    ];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
