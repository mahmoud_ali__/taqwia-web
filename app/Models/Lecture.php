<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $table = 'lectures';
    protected $primaryKey = 'lecture_id';
    protected $fillable = [
        'subject_id',
    ];


    public function scopeSelf($query)
    {
        return $query->where('user_id', auth()->id());
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function user()
    {
        return $this->belongsTo(Teacher::class, 'user_id');
    }
}
