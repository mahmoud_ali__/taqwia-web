<?php

namespace App\Models;

use App\Classes\FileUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Teacher extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'name_ar',
        'email',
        'password',
        'phone',
        'birthDate',
        'religion',
        'handGraduation',
        'graduationYear',
        'generalAppreciation',
        'jobTitle',
        'employer',
        'personalImage',
        'country_id',
        'nationality_id',
        'about',
        'about_ar',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(function (Builder $builder) {
            $builder->where('role', 2);
        });
    }


    public function reviews()
    {
        return $this->belongsToMany(Student::class, 'review', 'subject_id', 'user_id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt(request('password'));
    }

    public function setPersonalImageAttribute()
    {
        $file = new FileUpload('personalImage');
        $this->attributes['personalImage'] = $file->getFileName();
    }

    public function scientific_certificate_image()
    {
        return $this->hasMany(ScientificCertificateImage::class, 'user_id');
    }

    public function experience_certificate_image()
    {
        return $this->hasMany(ExperienceCertificateImage::class, 'user_id');
    }

    public function nationality()
    {
        return $this->belongsTo('App\Models\Nationality', 'nationality_id');
    }

    public function certificate()
    {
        return $this->belongsToMany('App\Models\Certificate', 'user_certificate', 'user_id', 'certificate_id');
    }

    public function scientificCertificateImage()
    {
        return $this->hasMany(ScientificCertificateImage::class, 'user_id');
    }
    

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function job()
    {
        return $this->belongsTo('App\Models\Job', 'jobTitle');
    }

    public function experienceCertificateImage()
    {
        return $this->hasMany(ExperienceCertificateImage::class, 'user_id');
    }

    public function previousWork()
    {
        return $this->hasMany(PreviousWork::class, 'user_id');
    }

    public function trainingCourse()
    {
        return $this->hasMany(TrainingCourse::class, 'user_id');
    }

    public function usergrade()
    {
        return $this->hasMany(Usergrade::class, 'user_id');
    }

    public function userlevel()
    {
        return $this->hasMany(UserLevel::class, 'user_id');
    }

}
