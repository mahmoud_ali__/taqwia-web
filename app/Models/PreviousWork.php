<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreviousWork extends Model
{
    protected $table = 'previous_work';
    protected $fillable = [
        'user_id',
        'place',
        'from_year',
        'to_year',
        'jobTitle',
    ];

    public $timestamps=false;
}
