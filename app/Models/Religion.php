<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $table = 'religions';
    protected $primaryKey = 'religion_id';
    protected $fillable = [
        'religion_name',
        'religion_name_english',
    ];
}
