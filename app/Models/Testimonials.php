<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    protected $fillable = [
        'image',
        'author',
        'link_text',
        'link_text_english',
        'link',
        'description',
        'description_english',
    ];
}
