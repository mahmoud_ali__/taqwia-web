<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'category_name',
        'category_name_english',
    ];

    public function subCategory()
    {
        return $this->hasMany('App\Models\SubCategory', 'category_id');
    }

    public function subject()
    {
        return $this->hasMany(Subject::class, 'category_id');
    }
}
