<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectRequest extends Model
{
    protected $table = 'subject_request';
    protected $primaryKey = 'subject_request_id';
    protected $fillable = [
        'student_id',
        'teacher_id',
        'subject_id',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function scopeSelf($query)
    {
        return $query->where('teacher_id', auth()->id());
    }
}
