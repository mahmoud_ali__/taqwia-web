<?php

namespace App\Models;

use App\User;
use App\Classes\FileUpload;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    protected $primaryKey = 'subject_id';
    protected $fillable = [
        'subject_title',
        'subject_title_english',
        'subject_hour',
        'subject_description',
        'subject_description_english',
        'category_id',
        'sub_category_id',
        'education_level_id',
        'image',
    ];

    public function lecture()
    {
        return $this->hasMany(Lecture::class, 'subject_id');
    }

    public function student()
    {
        return $this->belongsToMany(Subject::class, 'student_subject', 'user_id', 'subject_id');
    }

    public function teacher()
    {
        return $this->belongsToMany(User::class, 'lectures', 'subject_id', 'user_id');
    }

    public function setImageAttribute($value)
    {
        $file = new FileUpload('image');
        $this->attributes['image'] = $file->getFileName();
    }

}
