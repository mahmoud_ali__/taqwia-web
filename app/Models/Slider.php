<?php

namespace App\Models;

use App\Classes\FileUpload;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'image'
    ];

    public function setImageAttribute($value)
    {
        $file = new FileUpload('image');
        $this->attributes['image'] = $file->getFileName();
    }
}
