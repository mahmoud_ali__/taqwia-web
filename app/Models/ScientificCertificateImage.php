<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScientificCertificateImage extends Model
{
    protected $table = 'scientific_certificate_image';
    protected $fillable = [
        'user_id',
        'image',
    ];

    public $timestamps=false;

}
