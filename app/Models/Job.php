<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';
    protected $primaryKey = 'job_id';
    protected $fillable = [
        'job_name',
        'job_name_english',
    ];

    public function users()
    {
        return $this->hasMany(Teacher::class);
    }
}
