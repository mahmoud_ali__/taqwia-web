<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'package_name',
        'package_name_english',
        'package_hours',
        'package_price',
    ];
    protected $primaryKey = 'package_id';

}
