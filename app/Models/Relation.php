<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    protected $table = 'relations';
    protected $primaryKey = 'relation_id';
    protected $fillable = [
        'relation_name',
        'relation_name_english',
    ];
}
