<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table = 'certificate';
    protected $primaryKey = 'certificate_id';
    protected $fillable = [
        'certificate_name',
        'certificate_name_english',
    ];

    public $timestamps = false;

}
