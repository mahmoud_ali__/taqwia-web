<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'phone',
        'address',
        'email',
        'facebook',
        'twitter',
        'linkedin',
        'pinterest',
        'google',
        'terms',
        'about_us',
        'privacy_policy',
        'help_center',
        'terms_english',
        'about_us_english',
        'privacy_policy_english',
        'help_center_english',
    ];
}
