<?php

namespace App\Classes;

class FileUpload
{
    public $fileName;
    public $fileKey;
    public $filePath;
    public $fileExtension;

    public function __construct($name)
    {
        $this->fileKey = $name;
        $this->filePath = public_path() . '/upload';
        $this->setFileExtension();
        $this->setFileName();
        $this->upload();
    }

    public function hasFile()
    {
        return request()->hasFile($this->fileKey);
    }

    public function upload()
    {
        request()->file($this->fileKey)->move($this->filePath, $this->getFileName());
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    public function setFileExtension()
    {
        $this->fileExtension = request()->file($this->fileKey)->getClientOriginalExtension();
    }

    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    public function setFileName()
    {
        $this->fileName = date('mdYHis') . uniqid() . '.' . $this->getFileExtension();
    }

}