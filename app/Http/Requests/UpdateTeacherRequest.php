<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . request()->segment(3),
            'personalImage' => 'image',
            'phone' => 'required|max:20',
            'birthDate' => 'required|date',
            'religion' => 'required|max:100',
            'nationality_id' => 'required|exists:countries,country_id',
            'country_id' => 'required|exists:countries,country_id',
            'certificate' => 'required|exists:certificate,certificate_id',
            'handGraduation' => 'required|max:190',
            'graduationYear' => 'required|date_format:Y',
            'generalAppreciation' => 'required|max:190',
            'jobTitle' => 'required|max:190',
            'employer' => 'required|max:190',
        ];
    }
}
