<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreLectureRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $unique =
            request()->method() === 'POST' ? Rule::unique('lectures')->where(function ($query) {
                return $query->where('user_id', auth()->id());
            }) : Rule::unique('lectures')->ignore(request()->segment(3), 'lecture_id')->where(function ($query) {
                return $query->where('user_id', auth()->id());
            });

        return [
            'subject_id' => [
                'required',
                'exists:subjects,subject_id',
                $unique
            ]
        ];
    }

    public function messages()
    {
        return [
            'subject_id.unique' => 'You Have This Subject ',
        ];
    }
}
