<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_title' => 'required|max:190',
            'subject_title_english' => 'required|max:190',
            'image' => 'required|image',
            'subject_hour' => 'required|numeric|digits_between:1,3',
            'subject_description' => 'required',
            'subject_description_english' => 'required',
            'category_id' => 'required|exists:categories,category_id',
            'sub_category_id' => 'required|exists:sub_categories,sub_category_id',
        ];
    }
}
