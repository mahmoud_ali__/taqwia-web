<?php

namespace App\Http\Controllers\Auth;

use App\Classes\FileUpload;
use App\Models\Certificate;
use App\Models\Country;
use App\Models\Nationality;
use App\Models\ExperienceCertificateImage;
use App\Models\PreviousWork;
use App\Models\ScientificCertificateImage;
use App\Models\Setting;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\EducationLevel;
use App\Models\Religion;
use App\Models\Job;
use App\Models\TrainingCourse;
use App\Models\UserCertificate;
use App\Models\UserCountry;
use App\Models\UserNationality;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    use RegistersUsers;
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $terms = 'terms_english';
        if (session('user_lang') == 'ar')
            $terms = 'terms';

        return view('auth.register', [
            'country' => Country::where('country_id',118)->get(),
            'nationality' => Nationality::all(),
            'religion' => Religion::all(),
            'job' => Job::all(),
            'certificate' => Certificate::all(),
            'grade' => Grade::all(),
            'education_level' => EducationLevel::all(),
            'setting' => Setting::pluck($terms)->first()
        ]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function register(Request $request)
    {
        $this->validate($request, $this->validate_rule());

        /* Save User certificate */
        $user = new Teacher(request()->all());
        $user->role = 2;
        $user->country_id = request('country');
        $user->nationality_id = request('nationality');
        $user->save();
        /* Save User certificate */

        /* Save User certificate */
        $user->certificate()->attach(['certificate_id' => $request->certificate]);
        /* Save User certificate */

        /* Save User scientific_certificate_image */
        if ($request->hasfile('scientific_certificate_image')) {
            foreach ($request->file('scientific_certificate_image') as $file) {
                $name = date('mdYHis') . uniqid() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/upload', $name);
                $certificateImage[]['image'] = $name;
            }
            $user->scientific_certificate_image()->createMany($certificateImage);
        }
        /* Save User scientific_certificate_image */

        /* Save User experience_certificate_image */
        if ($request->hasfile('experience_certificate_image')) {
            foreach ($request->file('experience_certificate_image') as $file) {
                $name = date('mdYHis') . uniqid() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/upload', $name);
                $experienceImage[]['image'] = $name;
            }
            $user->experience_certificate_image()->createMany($experienceImage);
        }
        /* Save User experience_certificate_image */

        /* Save User previous_work */
        foreach ($request->input('previous_place') as $key => $value) {
            $previousWork[] = [
                'place' => $value,
                'from_year' => $request->input('from_year.' . $key),
                'to_year' => $request->input('to_year.' . $key),
                'jobTitle' => $request->input('previous_jobTitle.' . $key),
            ];
        }
        $user->previousWork()->createMany($previousWork);
        /* Save User previous_work */

        /* Save User trainingCourse */
        foreach ($request->input('course_name') as $key => $value) {
            $trainingCourse[] = [
                'name' => $value,
                'place' => $request->input('course_place.' . $key),
                'hours' => $request->input('course_hour.' . $key),
                'appreciation' => $request->input('course_appreciation.' . $key),
            ];
        }
        $user->trainingCourse()->createMany($trainingCourse);
        /* Save User trainingCourse */

        /* Save User grade */
        foreach ($request->input('grade') as $key => $value) {
            $grades[] = [
                'grade' => $value,
            ];
        }
        $user->usergrade()->createMany($grades);
        /* Save User grade */

        /* Save User grade */
        foreach ($request->input('education_level') as $key => $value) {
            $education_levels[] = [
                'level_id' => $value,
            ];
        }
        $user->userlevel()->createMany($education_levels);
        /* Save User grade */

        return redirect()->route('login')->with(['register' => 'Register Done']);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    private function validate_rule()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'personalImage' => 'required|image',
            'phone' => 'required|max:20',
            'birthDate' => 'required|date',
            'religion' => 'required',
            'nationality' => 'required|exists:countries,country_id',
            'country' => 'required|exists:countries,country_id',
            'certificate' => 'required|exists:certificate,certificate_id',
            'handGraduation' => 'required|max:190',
            'graduationYear' => 'required|date_format:Y',
            'generalAppreciation' => 'required|max:190',
            'jobTitle' => 'required|max:190',
            'employer' => 'required|max:190',
            'previous_place.*' => 'required|max:190',
            'from_year.*' => 'required|date_format:Y',
            'to_year.*' => 'required|date_format:Y',
            'previous_jobTitle.*' => 'required|max:190',
            'course_name.*' => 'required|max:190',
            'course_place.*' => 'required|max:190',
            'course_hour.*' => 'required|max:190',
            'course_appreciation.*' => 'required|max:190',
            'about' => 'required',
            // 'grade.*' => 'required',
        ];
    }
}
