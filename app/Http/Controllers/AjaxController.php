<?php

namespace App\Http\Controllers;

use App\Models\SubCategory;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function index()
    {
        $func = $_POST['method'];
        return $this->$func(json_decode($_POST['oVals'], true));
    }

    public function ajaxCall($script)
    {
        echo "<script>" . $script . "</script>";
    }

    public function getSubCategory($oVals)
    {
        $data = SubCategory::where('category_id', $oVals['id'])->get();
        $result = '';
        $result .= '<option value="" selected disabled>Select</option>';
        foreach ($data as $value) {
            $result .= '<option value="' . $value->sub_category_id . '">' . $value->sub_category_name . '</option>';
        }
        $this->ajaxCall("$('#sub_category').html('$result');");
    }
}
