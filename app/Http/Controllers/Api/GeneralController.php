<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\Country;
use App\Models\EducationLevel;
use App\Models\Grade;
use App\Models\Lecture;
use App\Models\Package;
use App\Models\Relation;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Student;
use App\Models\Subject;
use App\Models\SubjectRequest;
use App\Models\Teacher;
use App\Models\Job;
use App\Models\Usergrade;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{

    public function buy(Request $request)
    {
        Student::where('id', $request->userId)->increment('salary', Package::where('package_id', $request->pakageId)->first()->package_hours);
        return response()->json(
            [
                'code' => 1,
                'msg' => 'Success',
            ],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function teacher_subject(Teacher $userId, Subject $subjectId)
    {
        $userId->rank = intval(Review::where('teacher_id', $userId->id)->avg('rank'));
        return response()->json(
            [
                'code' => 1,
                'msg' => 'Success',
                'data' => $userId,
                'subject' => $subjectId
            ],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function userSubjectReqest(Request $request)
    {
        return response()->json(
            [
                'code' => 1,
                'msg' => 'Success',
                'data' => SubjectRequest::with('teacher', 'subject')->where('student_id', $request->userId)->get(),
            ],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function book(Request $request)
    {
        $user = Student::find($request->userId);
        
        if($user) {

            $save = new SubjectRequest;
            $save->subject_request_date = date('Y-m-d', strtotime($request->date));
            $save->subject_request_status = 1;
            $save->selectedHour = $request->selectedHour;
            $save->time = $request->time;
            $save->student_id = $request->userId;
            $save->teacher_id = $request->teacherId;
            $save->subject_id = $request->subjectId;
            $save->duration = $request->duration;
            $save->location = $request->location;
            $save->price = $request->price;
            $save->save();

            return response()->json(
                [
                    'code' => 1,
                    'msg' => 'Success',
                ],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
        } else {
        	return response()->json(
                [
                    'code' => 0,
                    'msg' => 'faild',
                ],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
        }
    }
    
    public function review(Request $request)
    {
        $save = new Review;
        $save->rank= $request->rank;;
        $save->title= $request->title;
        $save->description = $request->description ;
        $save->user_id = $request->user_id;
        $save->teacher_id = $request->teacher_id;
        $save->save();


        return response()->json(
            [
                'code' => 1,
                'msg' => 'Success',
            ],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function getCategorySubject()
    {
        $category = Category::has('subject')->orderBy('category_id', 'ASC')->get();
        foreach ($category as $key => $value) {
            $value->subject = $value->subject()->withCount('lecture')->limit(5)->get();
        }

        return response()->json(
            [
                'code' => 1,
                'msg' => 'Success',
                'data' => $category
            ],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function getSubject()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Subject::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }
    
    public function getReviews($id)
    {
        
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Review::where('teacher_id',$id)->join('users','users.id','review.user_id')->get()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function getCountry()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Country::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function education()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => EducationLevel::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function grade()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Grade::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function relation()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Relation::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function package()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Package::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function slider()
    {
        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => Slider::all()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function user(Student $student)
    {
        $student->grade_name = $student->grade()->first()->grade_name;
        $student->grade_name_english = $student->grade()->first()->grade_name_english;
        $student->subject = $student->subject()->withcount('lecture')->get();
        $student->other_subject = Subject::withcount('lecture')->limit(5)->inRandomOrder()->get();
        $student->subjectRequest = SubjectRequest::with('teacher', 'subject')->where('student_id', $student->id)->where('subject_request_status',4)->get();

        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => $student],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function teacher()
    {
        $teachers = Teacher::with('job')->get();
        foreach ($teachers as $key => $teacher) {
            $teachers[$key]->rank = intval(Review::where('teacher_id', $teacher->id)->avg('rank'));
            $teachers[$key]->students = SubjectRequest::where('teacher_id', $teacher->id)->count();
        }

        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => $teachers],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function teacherOfSubject($subjectId)
    {
        $teachers = Subject::with('teacher')->where('subject_id', $subjectId)->get();
        foreach ($teachers[0]->teacher as $key => $teacher) {
            $teachers[0]->teacher[$key]->job = Job::find($teacher->jobTitle);
            $teachers[0]->teacher[$key]->rank = intval(Review::where('teacher_id', $teacher->id)->avg('rank'));
            $teachers[0]->teacher[$key]->students = SubjectRequest::where('teacher_id', $teacher->id)->count();
        }

        return response()->json(
            ['code' => 1, 'msg' => 'Success', 'data' => $teachers],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function teachershow(Teacher $userId)
    {
        $info = $userId->with('experience_certificate_image', 'country', 'nationality', 'job')->where('id', $userId->id)->get();

        $info[0]->students = $userId->join('subject_request', 'users.id', 'subject_request.teacher_id')->where('users.id', $userId->id)->count(); 

        $info[0]->sessions = $userId->join('subject_request', 'users.id', 'subject_request.teacher_id')->where('users.id', $userId->id)->where('subject_request.subject_request_status', 2)->count(); 

        $info[0]->rank = intval(Review::where('teacher_id', $userId->id)->avg('rank'));

        $info[0]->grades = Usergrade::join('grades','grade_id','user_grades.grade')->get();

        return response()->json(
            [
                'code' => 1,
                'msg' => 'Success',
                'data' => $info
            ],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function setting()
    {
        return response()->json(Setting::first(),
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

}
