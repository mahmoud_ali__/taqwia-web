<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use App\Models\Student;
use App\Models\Teacher;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;


class LoginController extends Controller
{
    public function login()
    {
        $user = Student::where('email', request('email'))->first();
        if ($user && Hash::check(request('password'), $user->password)) {
            return response()->json(
                ['code' => 1, 'msg' => 'Login Success', 'user' => $user],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
        } else
            return response()->json(
                ['code' => 0, 'msg' => 'Invalid data', 'user' => $user],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
    }

    public function contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email',
            'subject' => 'required|max:190',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['code' => 0, 'msg' => 'Invalid data'],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
        }

        (new Contact(request()->all()))->save();
        return response()->json(
            ['code' => 1, 'msg' => 'Message Send'],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|max:20',
            'country_id' => 'required|exists:countries,country_id',
            'nationality_id' => 'required|exists:countries,country_id',
            'accountType' => 'required',
            'subject_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['code' => 0, 'msg' => 'Invalid data'],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
        }

        $save = new Student(request()->all());
        $save->role = 3;
        $save->open = 1;
        $save->save();

        $save->subject()->attach(explode(',', $request->subject_id));

        return response()->json(
            ['code' => 1, 'msg' => 'Rgister Success', 'user' => $save],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }

    public function editUserData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'phone' => 'required|max:20',
            'education_level_id' => 'required',
            'grade_id' => 'required',
            'school_area' => 'required',
            'school_name' => 'required',
            'relation_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['code' => 0, 'msg' => 'Invalid data'],
                200,
                ['Access-Control-Allow-Origin' => '*'],
                JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
            );
        }

        $destinationPath='upload/';
        $img = $request->image;
        unset($request->image);
        $arr=explode(';base64,', $img);
        if(isset($arr[1])){
            $img=$arr[1];
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $name = 'taqwia' . rand(11111, 99999) . time() . '.png';
            $success = file_put_contents($destinationPath.$name, $data);
            $Student = Student::find($request->id);
            $Student->personalImage = $name;
            $Student->update();
        }
        

        $save = Student::find($request->id)->update($request->all());
        return response()->json(
            ['code' => 1, 'msg' => 'Edit Success', 'user' => Student::where('id', $request->id)->first()],
            200,
            ['Access-Control-Allow-Origin' => '*'],
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT
        );
    }


}
