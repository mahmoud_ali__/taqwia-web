<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Faq;
use App\Models\LandPage;
use App\Models\Screenshot;
use App\Models\Subscribe;
use App\Models\Team;
use App\Models\Testimonials;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{

    public function subscribe()
    {
        $this->validate(request(), [
            'email' => 'required|email'
        ]);

        $msg = 'Subscribe DOne';
        $save = Subscribe::firstOrCreate(['email' => request('email')]);
        if ($save)
            $msg = 'You Have Been Subscribe Before';
        return $msg;
    }

    public function contact()
    {
        $this->validate(request(), [
            'name' => 'required|max:190',
            'email' => 'required|email',
            'subject' => 'required|max:190',
            'message' => 'required|max:700',
        ]);

        (new Contact(request()->all()))->save();
        return redirect('/' . '#contact')->with(['msg' => 'Message Sent']);
    }

}
