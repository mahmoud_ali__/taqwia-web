<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubCategoryController extends Controller
{
    private $view = 'admin.subcategory.';
    private $route = 'subcategory.';

    public function index()
    {
        return view($this->view . 'index', ['data' => SubCategory::with('category')->paginate(10)]);
    }

    public function create()
    {
        return view($this->view . 'create', ['category' => Category::all()]);
    }

    public function store()
    {
        $this->validate(request(), [
            'sub_category_name' => 'required|max:190',
            'sub_category_name_english' => 'required|max:190',
            'category_id' => 'required|exists:categories,category_id'
        ]);
        (new SubCategory(request()->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function edit(SubCategory $subcategory)
    {
        return view($this->view . 'edit', ['data' => $subcategory, 'category' => Category::all()]);
    }

    public function update(SubCategory $subcategory)
    {
        $this->validate(request(), [
            'sub_category_name' => 'required|max:190',
            'sub_category_name_english' => 'required|max:190',
            'category_id' => 'required|exists:categories,category_id'
        ]);
        $subcategory->update(request()->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(SubCategory $subcategory)
    {
        $subcategory->delete();
        return redirect()->route($this->route . 'index');
    }
}
