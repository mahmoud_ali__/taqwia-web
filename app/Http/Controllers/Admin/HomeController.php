<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\Student;
use App\Models\Subject;
use App\Models\SubjectRequest;
use App\Models\Teacher;
use App\User;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    private $view = 'admin.';

    public function index()
    {
//        return SubjectRequest::with('student', 'teacher', 'subject')->get();
        return view('main');
    }

    public function online()
    {
        $teacher = Teacher::find(auth()->id());
        $teacher->online = 1;
        $teacher->update();
        return redirect()->route('admin.home');
    }

    public function offline()
    {
        $teacher = Teacher::find(auth()->id());
        $teacher->online = 2;
        $teacher->update();
        return redirect()->route('admin.home');
    }

    public function setting()
    {
        if (request()->method() === 'GET') {
            return view($this->view . 'setting', ['data' => Setting::first()]);
        } else {
            $this->validate(request(), [
                'phone' => 'required|max:30',
                'address' => 'required|max:190',
                'email' => 'required|email',
                'facebook' => 'required|max:190',
                'twitter' => 'required|max:190',
                'linkedin' => 'required|max:190',
                'pinterest' => 'required|max:190',
                'google' => 'required|max:190',
                'terms' => 'required',
                'about_us' => 'required',
                'privacy_policy' => 'required',
                'help_center' => 'required',
                'terms_english' => 'required',
                'about_us_english' => 'required',
                'privacy_policy_english' => 'required',
                'help_center_english' => 'required',
            ]);

            Setting::first()->update(request()->all());
            return redirect()->route('admin.setting');
        }
    }

    public function getReviews()
    {
        $id = auth()->id();

        return view($this->view . 'reviews', ['reviews' => Review::where('teacher_id',$id)->join('users','users.id','review.user_id')->get()]);
    }

}
