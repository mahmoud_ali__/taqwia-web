<?php

namespace App\Http\Controllers\Admin;

use App\Models\EducationLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EducationLevelController extends Controller
{

    private $view = 'admin.education.';
    private $route = 'education.';

    public function index()
    {
        return view($this->view . 'index', ['data' => EducationLevel::paginate()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'education_level_name' => 'required|max:190',
            'education_level_name_english' => 'required|max:190',
        ]);

        (new EducationLevel($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(EducationLevel $educationLevel)
    {

    }

    public function edit(EducationLevel $education)
    {
        return view($this->view . 'edit',['data'=>$education]);
    }

    public function update(Request $request, EducationLevel $education)
    {
        $this->validate($request, [
            'education_level_name' => 'required|max:190',
            'education_level_name_english' => 'required|max:190',
        ]);
        $education->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(EducationLevel $education)
    {
        $education->delete();
        return redirect()->route($this->route . 'index');
    }
}
