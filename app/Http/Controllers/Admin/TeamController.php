<?php

namespace App\Http\Controllers\Admin;

use App\Classes\FileUpload;
use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index()
    {
        return view('admin.team.index', ['data' => Team::all()]);
    }

    public function create()
    {
        return view('admin.team.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'image' => 'required|image',
            'name' => 'required|max:190',
            'job' => 'required|max:190',
            'facebook' => 'required',
            'twitter' => 'required',
            'pinterest' => 'required',
            'linkedin' => 'required',
        ]);

        $input = request()->all();
        $file = new FileUpload('image');
        $input['image'] = $file->getFileName();
        (new Team($input))->save();
        return redirect()->route('team.index');
    }

    public function edit(Team $team)
    {
        return view('admin.team.edit', ['data' => $team]);
    }

    public function update(Team $team)
    {
        $this->validate(request(), [
            'image' => 'image',
            'name' => 'required|max:190',
            'job' => 'required|max:190',
            'facebook' => 'required',
            'twitter' => 'required',
            'pinterest' => 'required',
            'linkedin' => 'required',
        ]);

        $input = request()->all();
        if (request()->hasFile('image')) {
            $file = new FileUpload('image');
            $input['image'] = $file->getFileName();
        }
        $team->update($input);
        return redirect()->route('team.index');
    }

    public function destroy(Team $team)
    {
        $team->delete();
        return redirect()->route('team.index');
    }
}
