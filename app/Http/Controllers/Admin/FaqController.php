<?php

namespace App\Http\Controllers\Admin;

use App\Classes\FileUpload;
use App\Models\Faq;
use App\Models\Testimonials;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    private $view = 'admin.faq.';
    private $route = 'faq.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Faq::all()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store()
    {
        $this->validate(request(), [
            'question' => 'required|max:190',
            'question_english' => 'required|max:190',
            'answer' => 'required',
            'answer_english' => 'required',
        ]);
        (new Faq(request()->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function edit(Faq $faq)
    {
        return view($this->view . 'edit', ['data' => $faq]);
    }

    public function update(Faq $faq)
    {
        $this->validate(request(), [
            'question' => 'required|max:190',
            'question_english' => 'required|max:190',
            'answer' => 'required',
            'answer_english' => 'required',
        ]);
        $faq->update(request()->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();
        return redirect()->route($this->route . 'index');
    }

}
