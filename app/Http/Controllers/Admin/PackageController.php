<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    private $view = 'admin.package.';
    private $route = 'package.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Package::all()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store()
    {
        $this->validate(request(), [
            'package_name' => 'required|max:190',
            'package_name_english' => 'required|max:190',
            'package_hours' => 'required|max:190',
            'package_price' => 'required|max:190',
        ]);
        Package::create(request()->all());
        return redirect()->route($this->route . 'index');
    }

    public function show(Package $package)
    {

    }

    public function edit(Package $package)
    {
        return view($this->view . 'edit',
            [
                'data' => $package
            ]
        );
    }

    public function update(Package $package)
    {
        $this->validate(request(), [
            'package_name' => 'required|max:190',
            'package_name_english' => 'required|max:190',
            'package_hours' => 'required|max:190',
            'package_price' => 'required|max:190',
        ]);
        $package->update(request()->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Package $package)
    {
        $package->delete();
        return redirect()->route($this->route . 'index');
    }
}
