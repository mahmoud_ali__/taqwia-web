<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateStudentRequest;
use App\Models\Country;
use App\Models\Student;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    private $view = 'admin.student.';
    private $route = 'student.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Student::all()]);
    }

    public function active(Student $student)
    {
        $student->open = 1;
        $student->update();
        return redirect()->route($this->route . 'index');
    }

    public function inactive(Student $student)
    {
        $student->open = 0;
        $student->update();
        return redirect()->route($this->route . 'index');
    }

    public function show(Student $student)
    {
        return view($this->view . 'show', ['data' => $student]);
    }

    public function edit(Student $student)
    {
        return view($this->view . 'edit',
            [
                'data' => $student,
                'country' => Country::all(),
            ]
        );
    }

    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route($this->route . 'index');
    }
}
