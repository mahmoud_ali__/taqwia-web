<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreSubjectRequest;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Subject;
use App\Models\EducationLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    private $view = 'admin.subject.';
    private $route = 'subject.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Subject::paginate()]);
    }

    public function create()
    {
        return view($this->view . 'create', 
            [
                'category' => Category::all(),
                'education_level' => EducationLevel::all()
            ]);
    }

    public function store(StoreSubjectRequest $request)
    {
        (new Subject($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }


    public function show(Subject $subject)
    {

    }

    public function edit(Subject $subject)
    {
        return view($this->view . 'edit',
            [
                'data' => $subject,
                'category' => Category::all(),
                'education_level' => EducationLevel::all(),
                'subcategory' => SubCategory::where('category_id', $subject->category_id)->get()
            ]
        );
    }


    public function update(StoreSubjectRequest $request, Subject $subject)
    {
        $subject->update($request->all());
        return redirect()->route($this->route . 'index');
    }


    public function destroy(Subject $subject)
    {
        $subject->delete();
        return redirect()->route($this->route . 'index');
    }
}
