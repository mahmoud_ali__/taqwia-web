<?php

namespace App\Http\Controllers\Admin;

use App\Classes\FileUpload;
use App\Models\LandPage;
use App\Models\Screenshot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScreenshotController extends Controller
{
    public function index()
    {
        return view('admin.screenshot.index', ['data' => Screenshot::all()]);
    }

    public function create()
    {
        return view('admin.screenshot.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'image' => 'required|image'
        ]);
        $file = new FileUpload('image');
        (new Screenshot(['image' => $file->getFileName()]))->save();
        return redirect()->route('screenshot.index');
    }

    public function destroy(Screenshot $screenshot)
    {
        $screenshot->delete();
        return redirect()->route('screenshot.index');
    }
}
