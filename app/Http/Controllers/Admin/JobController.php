<?php

namespace App\Http\Controllers\Admin;

use App\Models\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    private $view = 'admin.job.';
    private $route = 'job.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Job::paginate()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'job_name' => 'required|max:190',
            'job_name_english' => 'required|max:190',
        ]);

        (new Job($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Job $grade)
    {

    }

    public function edit(Job $job)
    {
        return view($this->view . 'edit', ['data' => $job]);
    }

    public function update(Request $request, Job $job)
    {
        $this->validate($request, [
            'job_name' => 'required|max:190',
            'job_name_english' => 'required|max:190',
        ]);
        $job->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Job $job)
    {
        $job->delete();
        return redirect()->route($this->route . 'index');
    }
}
