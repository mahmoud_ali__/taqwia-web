<?php

namespace App\Http\Controllers\Admin;

use App\Models\SubjectRequest;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectRequestController extends Controller
{
    private $view = 'admin.subject_request.';

    public function index()
    {
        if (auth()->user()->role == 1) {
            $data['data'] = SubjectRequest::with('student', 'teacher', 'subject')->paginate();
        } elseif (auth()->user()->role == 2) {
            $data['data'] = SubjectRequest::with('student', 'subject')->Self()->paginate();
        }
        return view($this->view . 'index', $data);
    }

    public function accept(SubjectRequest $id)
    {
        if (auth()->id() != $id->teacher_id || $id->subject_request_status != 1) {
            abort(404);
        }
        $id->subject_request_status = 2;
        $id->update();
        $student = Student::find($id->student_id);
        $data =array();
        $student_name = $student->name;
        $req_id = $id->subject_request_id;
        $data['email'] = $student->email;
        $msg = 'click here'.'<a href="http://taqwiaapp.com/paymednt'.$id->subject_request_id.'"></a>';
        
        $subject = 'Complete Your Payment';

                        $headers = "From: ". strip_tags('taqwiaapp.com') . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=320, target-densitydpi=device-dpi"/>
                <title>email confirmation</title>

                <style type="text/css">
                    /* Client-specific Styles */
                    #outlook a { padding: 0; }  /* Force Outlook to provide a "view in browser" button. */
                    body { width: 100% !important; }
                    .ReadMsgBody { width: 100%; } 
                    .ExternalClass { width: 100%; display:block !important; } /* Force Hotmail to display emails at full width */
                    .ExternalClass p{margin: 0px!important;}
                    /* Reset Styles */
                    /* Add 100px so mobile switch bar doesn\'t cover street address. */
                    body { background-color: #ececec; margin: 0; padding: 0; }
                    p{margin:0px!important;}

                    img { outline: none; text-decoration: none; display: block;}
                    br, strong br, b br, em br, i br { line-height:100%; }
                    h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
                    h1 a, h2 a, h3 a, h4 a, h5 a, h6 a { color: blue !important; }
                    h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active { color: red !important; }
                    /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
                    h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: purple !important; }
                    /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */  
                    table td, table tr { border-collapse: collapse; }
                    .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {
                        color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;
                    }   /* Body text color for the New Yahoo.  This example sets the font of Yahoo\'s Shortcuts to black. */
                    /* This most probably won\'t work in all email clients. Don\'t include code blocks in email. */
                    code {
                        white-space: normal;
                        word-break: break-all;
                    }




                    /* mystyle*/


                    @media only screen and (max-width: 600px) {
                        .content {width: 95% !important;}
                        .headerImg{margin:0 auto !important; padding-left:0px !important;}
                        .title{ font-size: 20px !important; font-weight: bold !important;}
                        .message{ padding-right: 15px !important; padding-left: 15px !important; font-weight: bold !important; color:#8A8989 !important; }
                        .Button{width: 90% !important;}
                        .ButtonContent a{font-weight: bold;}
                        .contact-mail{width:100% !important;}
                        .email{padding-left: 0!important;}

                        body[yahoo].content {width: 95% !important;
                                             padding-top:20px !important; padding-right: 10px !important; padding-bottom: 20px !important; padding-left: 10px !important;}
                        body[yahoo].headerImg{margin:0 auto !important; padding-left: 0px!important;}
                        body[yahoo].message{ padding-right: 15px !important; padding-left: 15px !important; font-weight: bold !important; color:#8A8989 !important;}
                        body[yahoo].title{font-size: 20px !important; font-weight: bold !important;}
                        body[yahoo].Button{width: 100% !important;}
                        body[yahoo].ButtonContent a{font-weight: bold;}
                        body[yahoo].contact-mail{width:100% !important;}
                        body[yahoo].email{padding-left: 0!important;}


                    }

                    body {margin:0; padding:0; min-width: 100%!important; background-color: #f3f3f3;}
                    .container{background-color:#f3f3f3; border-collapse: collapse; width:100%; table-layout: fixed; margin:0 auto;}
                    .content{width: 600px;  background-color:#f3f3f3;border-collapse: collapse;margin:0 auto;}  
                    .header{padding-top: 20px; padding-right: 10px; padding-bottom: 0px; padding-left: 10px; margin:0 auto; font-family:Arial, Helvetica, sans-serif; background-color:#f3f3f3;}
                    .header-content{padding-left:20px; padding-right:20px}
                    .headerImg{ width:90%; max-width: 160px; padding-bottom: 10px; padding-left: 20px;}
                    .bannerImg{  margin:0 auto; background-color: #fff;  padding-top: 0px; padding-right: 0px; padding-bottom: 20px; padding-left: 0px;}
                    .bannerImg img{width:100%;}
                    .title{font-family:Arial, Helvetica, sans-serif;  font-size: 20px; color:#036985; margin: 0 auto; padding-top: 0px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; background-color: #fff; text-align:center;}
                    .message{ font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #273554; background-color: #fff; padding-top: 5px; padding-right: 30px; padding-bottom: 5px; padding-left: 30px; line-height: 30px;}
                    .foter{font-family: Arial, Helvetica, sans-serif; font-size: 14px; color:#fff;background-color: #f3f3f3;  padding-top: 30px; padding-right: 0px; padding-bottom: 35px; padding-left: 0px; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px;}
                    .foter a{color:#fff;  text-decoration: none;}
                    .foter span {color:#fff; text-decoration: none;text-decoration: underline;border-bottom-left-radius: 3px; border-bottom-right-radius: 3px;-webkit-border-bottom-left-radius:3px; -webkit-border-bottom-right-radius:3px; -moz-border-bottom-left-radius:3px; -moz-border-bottom-right-radius:3px;}         
                    .mail{text-align: center; padding: 0 2px 0 0;}
                    .mail-address{text-align: center; padding: 0 0 0 2px;}
                    .copyrights{color:#535353; font-size: 11px; padding-top: 10px;padding-right: 15px;padding-left: 15px;}
                    .copyRights a {color:#8f8f95;  text-decoration: none;}
                    .copyrights span{color:#8f8f95; text-decoration: none;text-decoration: underline;}
                    .space{width: 5%;}
                    .Hspace{height:30px;}
                    .info{line-height: 150%;}

                </style>
                <!--[if (gte mso 9)|(IE)]>
           <style type="text/css">
               table {border-collapse: collapse;}
           </style>
           <![endif]-->
            </head>
            <body  yahoo="fix" bgcolor="#f3f3f3" style="margin: 0; padding: 0; min-width: 100%!important; background-color: #f3f3f3">
                <table  class="container" width="100%" bgcolor="#f3f3f3" border="0" cellpadding="0" cellspacing="0" style="background-color:#f3f3f3; border-collapse: collapse; table-layout: fixed; margin:0 auto;">
                    <tr>
                        <td>
                            <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
        <td>
        <![endif]-->



                            <table class="content" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 600px; background-color:#f3f3f3; border-collapse: collapse; margin:0 auto; ">
                                <tr>
                                    <td class="Hspace" style="background-color:#f3f3f3; height: 30px;">
                                        &nbsp;
                                    </td>
                                </tr>

                                <tr>
                                    <td  colspan="3" class="header" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;  font-family:Arial, Helvetica, sans-serif; background-color:#ffffff;">
                                        <img class="headerImg" src="http://taqwiaapp.com/assets/Taqwaia-mov.png"  width="90%"  height="auto" border="0" alt="EventXray" style="width:90%; max-width: 140px; padding-bottom: 0px; padding-left: 20px;" />
                                    </td>                          
                                </tr>
                                <tr>
                                    <td  class="bannerImg" align="center"  style=" margin:0 auto; background-color: #fff;  padding-top: 0px; padding-right: 0px; padding-bottom: 20px; padding-left: 0px;">
                                        <img  src="https://thebiddest.com/pics/mail.png"  align="bottom"   border="0" alt="header" width="100%" />

                                    </td>
                                </tr>
                                <tr>
                                    <td class="title" align="center" style="font-family:Arial, Helvetica, sans-serif;  font-size: 35px; font-weight: bold; color:#606060; margin: 0 auto; padding-top: 0px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; background-color: #fff;">
                                        Complete Payment Proccess

                                    </td>
                                </tr>

                                <tr>
                                    <td  class="message" align="center" style=" font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #273554; background-color: #fff; padding-top: 0px; padding-right: 30px; padding-bottom: 20px; padding-left: 30px; line-height: 30px;">

                                    <h3>
                                    <b>Dear Student,</b>

                                    </h3>
                                
            
                                
                                    </td>

                                </tr>
                                <tr>
                                    <td  class="message" align="center" style=" font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #273554; background-color: #fff; padding-top: 0px; padding-right: 30px; padding-bottom: 20px; padding-left: 30px; line-height: 30px;">

                                    <h3>
                                    <p>'.$student_name.'</p>

                                    </h3>
                                
            
                                
                                    </td>

                                </tr>
                                <tr>
                                    <td  class="message" align="center" style=" font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #273554; background-color: #fff; padding-top: 0px; padding-right: 30px; padding-bottom: 20px; padding-left: 30px; line-height: 30px;">

                                    <h4>
                                    <p>'.'<a href="http://taqwiaapp.com/admin/payment/'.$id->subject_request_id.'">Click Here To Complete Payment Proccess</a>'.'</p>

                                    </h4>
                                
            
                                
                                    </td>

                                </tr>


                             

                            </table>







                            <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
                        </td>
                    </tr>
                </table>
            </body>
        </html>';

                        mail($data['email'], $subject, $message, $headers);
        
        
        
        return redirect()->route('subject.request');
    }

    public function refuse(SubjectRequest $id)
    {
        if (auth()->id() != $id->teacher_id || $id->subject_request_status != 1) {
            abort(404);
        }
        $id->subject_request_status = 3;
        $id->update();
        return redirect()->route('subject.request');
    }
    
    public function payment(SubjectRequest $id)
    {
        $student = Student::find($id->student_id);
        $data['name'] = $student->name;
        $data['id'] = $id->subject_request_id;
        $data['price'] = $id->price;
        
        return view('payment',$data);
    }
    public function response(){

        if ($_GET['Status'] == 1) {
           $save = SubjectRequest::find($_GET['Variable1']);
           $save->subject_request_status = 4;
	   $save->update();
	        } else {
	return redirect('/');
	}

        
        return view('response');
    }

}
