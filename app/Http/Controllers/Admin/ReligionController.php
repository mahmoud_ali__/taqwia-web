<?php

namespace App\Http\Controllers\Admin;

use App\Models\Religion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReligionController extends Controller
{
    private $view = 'admin.religion.';
    private $route = 'religion.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Religion::paginate()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'religion_name' => 'required|max:190',
            'religion_name_english' => 'required|max:190',
        ]);

        (new Religion($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Religion $grade)
    {

    }

    public function edit(Religion $religion)
    {
        return view($this->view . 'edit', ['data' => $religion]);
    }

    public function update(Request $request, Religion $religion)
    {
        $this->validate($request, [
            'religion_name' => 'required|max:190',
            'religion_name_english' => 'required|max:190',
        ]);
        $religion->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Religion $religion)
    {
        $religion->delete();
        return redirect()->route($this->route . 'index');
    }
}
