<?php

namespace App\Http\Controllers\Admin;

use App\Classes\FileUpload;
use App\Models\Testimonials;
use App\Http\Controllers\Controller;

class TestimonialsController extends Controller
{
    private $view = 'admin.testimonials.';
    private $route = 'testimonials.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Testimonials::all()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store()
    {
        $this->validate(request(), [
            'image' => 'required|image',
            'author' => 'required|max:190',
            'link_text' => 'required|max:190',
            'link_text_english' => 'required|max:190',
            'link' => 'required|max:190',
            'description' => 'required',
            'description_english' => 'required',
        ]);

        $input = request()->all();
        $file = new FileUpload('image');
        $input['image'] = $file->getFileName();
        (new Testimonials($input))->save();
        return redirect()->route($this->route . 'index');
    }

    public function edit($id)
    {
        return view($this->view . 'edit', ['data' => Testimonials::findOrFail($id)]);
    }

    public function update($id)
    {
        $this->validate(request(), [
            'image' => 'image',
            'author' => 'required|max:190',
            'link_text' => 'required|max:190',
            'link_text_english' => 'required|max:190',
            'link' => 'required|max:190',
            'description' => 'required',
            'description_english' => 'required',
        ]);

        $input = request()->all();
        if (request()->has('image')) {
            $file = new FileUpload('image');
            $input['image'] = $file->getFileName();
        }
        Testimonials::findOrFail($id)->update($input);
        return redirect()->route($this->route . 'index');
    }

    public function destroy($id)
    {
        Testimonials::findOrFail($id)->delete();
        return redirect()->route($this->route . 'index');
    }

}
