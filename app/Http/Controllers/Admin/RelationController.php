<?php

namespace App\Http\Controllers\Admin;

use App\Models\Relation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelationController extends Controller
{

    private $view = 'admin.relation.';
    private $route = 'relation.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Relation::paginate()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'relation_name' => 'required|max:190',
            'relation_name_english' => 'required|max:190',
        ]);

        (new Relation($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Relation $grade)
    {

    }

    public function edit(Relation $relation)
    {
        return view($this->view . 'edit', ['data' => $relation]);
    }

    public function update(Request $request, Relation $relation)
    {
        $this->validate($request, [
            'relation_name' => 'required|max:190',
            'relation_name_english' => 'required|max:190',
        ]);
        $relation->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Relation $relation)
    {
        $relation->delete();
        return redirect()->route($this->route . 'index');
    }

}
