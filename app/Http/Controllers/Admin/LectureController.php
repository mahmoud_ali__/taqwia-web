<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreLectureRequest;
use App\Models\Category;
use App\Models\Lecture;
use App\Models\SubCategory;
use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LectureController extends Controller
{
    private $view = 'admin.lecture.';
    private $route = 'lecture.';

    public function __construct()
    {
        $this->middleware('teacher')->only(['create', 'store']);
    }

    public function index()
    {
        if (auth()->user()->role == 1) {
            $data['data'] = Lecture::with(['user', 'subject'])->paginate();
        } elseif (auth()->user()->role == 2) {
            $data['data'] = Lecture::with('subject')->Self()->paginate();
        }
        return view($this->view . 'index', $data);
    }

    public function create()
    {
        return view($this->view . 'create', ['subject' => Subject::all()]);
    }

    public function store(StoreLectureRequest $request)
    {
        $save = new Lecture($request->all());
        $save->user_id = auth()->id();
        $save->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Lecture $lecture)
    {

    }

    public function edit(Lecture $lecture)
    {
        if (auth()->user()->role != 1 && $lecture->user_id != auth()->id()) {
            abort(404);
        }
        return view($this->view . 'edit',
            [
                'data' => $lecture,
                'subject' => Subject::all()
            ]
        );
    }

    public function update(StoreLectureRequest $request, Lecture $lecture)
    {
        if (auth()->user()->role != 1 && $lecture->user_id != auth()->id()) {
            abort(404);
        }
        $lecture->update($request->all());
        return redirect()->route($this->route . 'index');
    }


    public function destroy(Lecture $lecture)
    {

    }
}
