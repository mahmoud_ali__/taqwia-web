<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateTeacherRequest;
use App\Models\Certificate;
use App\Models\Country;
use App\Models\Nationality;
use App\Models\Job;
use App\Models\Teacher;
use App\Models\Lecture;
use App\Models\Religion;
use App\Models\SubjectRequest;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    private $view = 'admin.teacher.';
    private $route = 'teacher.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Teacher::all()]);
    }

    public function active(Teacher $teacher)
    {
        $teacher->open = 1;
        $teacher->update();
        return redirect()->route($this->route . 'index');
    }

    public function inactive(Teacher $teacher)
    {
        $teacher->open = 0;
        $teacher->update();
        return redirect()->route($this->route . 'index');
    }

    public function show(Teacher $teacher)
    {
//        https://appdividend.com/2018/02/09/laravel-multiple-files-upload-tutorial-example/
        return view($this->view . 'show', ['data' => $teacher]);
    }

    public function edit(Teacher $teacher)
    {
        if (auth()->user()->role == 2 && auth()->user()->id != $teacher->id) {
            return redirect()->back();
        }
        return view($this->view . 'edit',
            [
                'data' => $teacher,
                'religion' => Religion::all(),
                // 'country' => Country::all(),
                'country' => Country::where('country_id',118)->get(),
                'nationality' => Nationality::all(),
                'job' => Job::all(),
                'certificate' => Certificate::all(),
                'user_certificate' => $teacher->certificate()->pluck('user_certificate.certificate_id')->toArray(),
            ]
        );
    }

    public function update(UpdateTeacherRequest $request, Teacher $teacher)
    {
        $teacher->setAttribute('perHOur',request('perHOur') );
        $teacher->update($request->all());
        if (auth()->user()->role = 2) {
            return redirect()->back();
        }
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Teacher $teacher)
    {
        Lecture::where('user_id', $teacher->id)->delete();
        SubjectRequest::where('teacher_id', $teacher->id)->delete();
        $teacher->delete();
        return redirect()->route($this->route . 'index');
    }
}
