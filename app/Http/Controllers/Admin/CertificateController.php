<?php

namespace App\Http\Controllers\Admin;

use App\Models\Certificate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CertificateController extends Controller
{
    private $view = 'admin.certificate.';
    private $route = 'certificate.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Certificate::paginate(10)]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'certificate_name' => 'required|max:190',
            'certificate_name_english' => 'required|max:190',
        ]);

        (new Certificate($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Certificate $certificate)
    {

    }


    public function edit(Certificate $certificate)
    {
        return view($this->view . 'edit', ['data' => $certificate]);
    }


    public function update(Certificate $certificate)
    {
        $this->validate(request(), [
            'certificate_name' => 'required|max:190',
            'certificate_name_english' => 'required|max:190',
        ]);

        $certificate->update(request()->all());
        return redirect()->route($this->route . 'index');
    }


    public function destroy(Certificate $certificate)
    {
        $certificate->delete();
        return redirect()->route($this->route . 'index');
    }
}
