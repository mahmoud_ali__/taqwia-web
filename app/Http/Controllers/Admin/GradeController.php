<?php

namespace App\Http\Controllers\Admin;
use App\Models\Grade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradeController extends Controller
{

    private $view = 'admin.grade.';
    private $route = 'grade.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Grade::paginate()]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'grade_name' => 'required|max:190',
            'grade_name_english' => 'required|max:190',
        ]);

        (new Grade($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Grade $grade)
    {
        //
    }

    public function edit(Grade $grade)
    {
        return view($this->view . 'edit', ['data' => $grade]);
    }

    public function update(Request $request, Grade $grade)
    {
        $this->validate($request, [
            'grade_name' => 'required|max:190',
            'grade_name_english' => 'required|max:190',
        ]);
        $grade->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Grade $grade)
    {
        $grade->delete();
        return redirect()->route($this->route . 'index');
    }

}
