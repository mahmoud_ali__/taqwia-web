<?php

namespace App\Http\Controllers\Admin;

use App\Classes\FileUpload;
use App\Models\Contact;
use App\Models\LandPage;
use App\Models\LandPage1;
use App\Models\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandPageController extends Controller
{
    private $view = 'admin.landpage.';

    public function index()
    {
        if (request()->method() === 'GET') {
            return view('admin.landpage.home', ['data' => LandPage::first()]);
        } else if (request()->method() === 'POST') {
            $this->validate(request(), [
                'home_title' => 'required|max:190',
                'home_title_english' => 'required|max:190',
                'home_description' => 'required',
                'home_description_english' => 'required',
                'google_store_link' => 'required|max:190',
                'apple_store_link' => 'required|max:190',
                'home_background' => 'image',
            ]);

            $input = request()->all();
            if (request()->hasFile('home_background')) {
                $file = new FileUpload('home_background');
                $input['home_background'] = $file->getFileName();
            }

            LandPage::first()->update($input);
            return redirect()->route('landpage.home');
        }
    }

    public function subscribe()
    {
        return view($this->view . 'subscribe', ['data' => Subscribe::paginate()]);
    }

    public function contact()
    {
        return view($this->view . 'contact', ['data' => Contact::paginate()]);
    }

    public function feature()
    {
        if (request()->method() === 'GET') {
            return view('admin.landpage.feature', ['data' => LandPage::first()]);
        } else if (request()->method() === 'POST') {

            $this->validate(request(), [
                'feature_title1' => 'required|max:190',
                'feature_title1_english' => 'required|max:190',
                'feature_description1' => 'required|max:190',
                'feature_description1_english' => 'required|max:190',
                'feature_image1' => 'image',
                'feature_title2' => 'required|max:190',
                'feature_title2_english' => 'required|max:190',
                'feature_description2' => 'required|max:190',
                'feature_description2_english' => 'required|max:190',
                'feature_image2' => 'image',
                'feature_title3' => 'required|max:190',
                'feature_title3_english' => 'required|max:190',
                'feature_description3' => 'required|max:190',
                'feature_description3_english' => 'required|max:190',
                'feature_image3' => 'image',
            ]);

            $input = request()->all();
            if (request()->hasFile('feature_image1')) {
                $file = new FileUpload('feature_image1');
                $input['feature_image1'] = $file->getFileName();
            }

            if (request()->hasFile('feature_image2')) {
                $file = new FileUpload('feature_image2');
                $input['feature_image2'] = $file->getFileName();
            }

            if (request()->hasFile('feature_image3')) {
                $file = new FileUpload('feature_image3');
                $input['feature_image3'] = $file->getFileName();
            }

            LandPage::first()->update($input);
            return redirect()->route('landpage.feature');
        }
    }

    public function general()
    {
        if (request()->method() === 'GET') {
            $data = LandPage::join('landpage1', 'landpage1.landpage_id', 'landpage.id')->first();
            return view('admin.landpage.general', ['data' => $data]);
        } else if (request()->method() === 'POST') {
            $this->validate(request(), [
                'screenshot_title' => 'required|max:190',
                'screenshot_title_english' => 'required|max:190',
                'screenshot_description' => 'required',
                'screenshot_description_english' => 'required',
                'team_title' => 'required|max:190',
                'team_title_english' => 'required|max:190',
                'team_description' => 'required',
                'team_description_english' => 'required',
                'total_user' => 'required',
                'loyal_customer' => 'required',
                'total_achivement' => 'required',
                'app_ratting' => 'required',
                'testimonial_title' => 'required|max:190',
                'testimonial_title_english' => 'required|max:190',
                'testimonial_description' => 'required',
                'testimonial_description_english' => 'required',
                'faq_title' => 'required|max:190',
                'faq_title_english' => 'required|max:190',
                'faq_description' => 'required',
                'faq_description_english' => 'required',
                'download_title' => 'required|max:190',
                'download_title_english' => 'required|max:190',
                'download_description' => 'required',
                'download_description_english' => 'required',
            ]);
            LandPage::first()->update(request()->all());
            LandPage1::first()->update(request()->all());
            return redirect()->route('landpage.general');
        }
    }

    public function awesomefeature()
    {
        if (request()->method() === 'GET') {
            return view('admin.landpage.awesome_feature', ['data' => LandPage::first()]);
        } else if (request()->method() === 'POST') {

            $this->validate(request(), [
                'awesome_feature_title' => 'required|max:190',
                'awesome_feature_title_english' => 'required|max:190',
                'awesome_feature_description' => 'required|max:190',
                'awesome_feature_description_english' => 'required|max:190',
                'awesome_feature_video' => 'required|max:50',

                'awesome_feature_title1' => 'required|max:190',
                'awesome_feature_title1_english' => 'required|max:190',
                'awesome_feature_description1' => 'required|max:190',
                'awesome_feature_description1_english' => 'required|max:190',
                'awesome_feature_image1' => 'image',

                'awesome_feature_title2' => 'required|max:190',
                'awesome_feature_title2_english' => 'required|max:190',
                'awesome_feature_description2' => 'required|max:190',
                'awesome_feature_description2_english' => 'required|max:190',
                'awesome_feature_image2' => 'image',

                'awesome_feature_title3' => 'required|max:190',
                'awesome_feature_title3_english' => 'required|max:190',
                'awesome_feature_description3' => 'required|max:190',
                'awesome_feature_description3_english' => 'required|max:190',
                'awesome_feature_image3' => 'image',

                'awesome_feature_title4' => 'required|max:190',
                'awesome_feature_title4_english' => 'required|max:190',
                'awesome_feature_description4' => 'required|max:190',
                'awesome_feature_description4_english' => 'required|max:190',
                'awesome_feature_image4' => 'image',

                'awesome_feature_title5' => 'required|max:190',
                'awesome_feature_title5_english' => 'required|max:190',
                'awesome_feature_description5' => 'required|max:190',
                'awesome_feature_description5_english' => 'required|max:190',
                'awesome_feature_image5' => 'image',

                'awesome_feature_title6' => 'required|max:190',
                'awesome_feature_title6_english' => 'required|max:190',
                'awesome_feature_description6' => 'required|max:190',
                'awesome_feature_description6_english' => 'required|max:190',
                'awesome_feature_image6' => 'image',

            ]);

            $input = request()->all();
            if (request()->hasFile('awesome_feature_image1')) {
                $file = new FileUpload('awesome_feature_image1');
                $input['awesome_feature_image1'] = $file->getFileName();
            }

            if (request()->hasFile('awesome_feature_image2')) {
                $file = new FileUpload('awesome_feature_image2');
                $input['awesome_feature_image2'] = $file->getFileName();
            }

            if (request()->hasFile('awesome_feature_image3')) {
                $file = new FileUpload('awesome_feature_image3');
                $input['awesome_feature_image3'] = $file->getFileName();
            }

            if (request()->hasFile('awesome_feature_image4')) {
                $file = new FileUpload('awesome_feature_image4');
                $input['awesome_feature_image4'] = $file->getFileName();
            }

            if (request()->hasFile('awesome_feature_image5')) {
                $file = new FileUpload('awesome_feature_image5');
                $input['awesome_feature_image5'] = $file->getFileName();
            }

            if (request()->hasFile('awesome_feature_image6')) {
                $file = new FileUpload('awesome_feature_image6');
                $input['awesome_feature_image6'] = $file->getFileName();
            }

            LandPage::first()->update($input);
            return redirect()->route('landpage.awesomefeature');
        }
    }

    public function about()
    {
        if (request()->method() === 'GET') {
            $data = LandPage::join('landpage1', 'landpage1.landpage_id', 'landpage.id')->first();
            return view('admin.landpage.about', ['data' => $data]);
        } else if (request()->method() === 'POST') {

            $this->validate(request(), [
                'about_title' => 'required|max:190',
                'about_title_english' => 'required|max:190',

                'about_description' => 'required|max:190',
                'about_description_english' => 'required|max:190',

                'about_title1' => 'required|max:190',
                'about_title1_english' => 'required|max:190',

                'about_description1' => 'required|max:250',
                'about_description1_english' => 'required|max:250',
                'about_image1' => 'image',

                'about_title2' => 'required|max:190',
                'about_title2_english' => 'required|max:190',

                'about_description2' => 'required|max:250',
                'about_description2_english' => 'required|max:250',
                'about_image2' => 'image',

                'about_title3' => 'required|max:190',
                'about_title3_english' => 'required|max:190',

                'about_description3' => 'required|max:250',
                'about_description3_english' => 'required|max:250',
                'about_image3' => 'image',
            ]);

            $input = request()->all();
            if (request()->hasFile('about_image1')) {
                $file = new FileUpload('about_image1');
                $input['about_image1'] = $file->getFileName();
            }

            if (request()->hasFile('about_image2')) {
                $file = new FileUpload('about_image2');
                $input['about_image2'] = $file->getFileName();
            }

            if (request()->hasFile('about_image3')) {
                $file = new FileUpload('about_image3');
                $input['about_image3'] = $file->getFileName();
            }

            LandPage::first()->update($input);
            LandPage1::first()->update($input);
            return redirect()->route('landpage.about');
        }
    }


}
