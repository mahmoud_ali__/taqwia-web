<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CategoryController extends Controller
{

    private $view = 'admin.category.';
    private $route = 'category.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Category::paginate(10)]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required|max:190',
            'category_name_english' => 'required|max:190'
        ]);

        (new Category($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function show(Category $category)
    {

    }


    public function edit(Category $category)
    {
        return view($this->view . 'edit', ['data' => $category]);
    }


    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'category_name' => 'required|max:190',
            'category_name_english' => 'required|max:190',
        ]);
        $category->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route($this->route . 'index');
    }
}
