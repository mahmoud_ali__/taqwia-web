<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{

    private $view = 'admin.slider.';
    private $route = 'slider.';

    public function index()
    {
        return view($this->view . 'index', ['data' => Slider::paginate(5)]);
    }

    public function create()
    {
        return view($this->view . 'create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image'
        ]);

        (new Slider($request->all()))->save();
        return redirect()->route($this->route . 'index');
    }

    public function edit(Slider $slider)
    {
        return view($this->view . 'edit', ['data' => $slider]);
    }

    public function update(Request $request, Slider $slider)
    {
        $this->validate($request, [
            'image' => 'required|image'
        ]);
        $slider->update($request->all());
        return redirect()->route($this->route . 'index');
    }

    public function destroy(Slider $slider)
    {
        $slider->delete();
        return redirect()->route($this->route . 'index');
    }

}
