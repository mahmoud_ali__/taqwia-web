<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\LandPage;
use App\Models\Screenshot;
use App\Models\Setting;
use App\Models\Team;
use App\Models\Testimonials;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    
    public function index()
    {
//        return SubjectRequest::with('student', 'teacher', 'subject')->get();
        return redirect('/');
    }
    public function __invoke()
    {
        return view('landpage', [
            'data' => LandPage::join('landpage1', 'landpage1.landpage_id', 'landpage.id')->first(),
            'screenshot' => Screenshot::all(),
            'team' => Team::all(),
            'testimonials' => Testimonials::all(),
            'faq' => Faq::all(),
            'setting' => Setting::first(),
        ]);
    }
        public function terms()
    {
        $terms = 'terms_english';
        if (session('user_lang') == 'ar')
            $terms = 'terms';

            return view('auth.terms', [
                'setting' => Setting::pluck($terms)->first()
        ]);
    }
    

}
