<?php

use Illuminate\Http\Request;

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api'], function () {
    Route::post('buy', 'GeneralController@buy')->name('api.login');
    Route::post('login', 'LoginController@login')->name('api.login');
    Route::post('edit', 'LoginController@editUserData')->name('api.edit');
    Route::post('contact', 'LoginController@contact')->name('api.edit');
    Route::post('register', 'LoginController@register')->name('api.register');
    Route::get('slider', 'GeneralController@slider')->name('api.slider');
    Route::get('user/{student}', 'GeneralController@user');
    Route::get('getCategorySubject', 'GeneralController@getCategorySubject');
    Route::get('country', 'GeneralController@getCountry')->name('api.country');
    Route::get('subject', 'GeneralController@getSubject')->name('api.subject');
    Route::get('education', 'GeneralController@education')->name('api.education');
    Route::get('grade', 'GeneralController@grade')->name('api.grade');
    Route::get('relation', 'GeneralController@relation')->name('api.relation');
    Route::get('setting', 'GeneralController@setting')->name('api.setting');
    Route::get('package', 'GeneralController@package')->name('api.package');
    Route::get('teacher', 'GeneralController@teacher')->name('api.teacher');
    Route::get('teacher/{subjectId}', 'GeneralController@teacherOfSubject')->name('api.teacherOfSubject');
    Route::get('teacher/{userId}/show', 'GeneralController@teachershow');


Route::get('getReviews/{Id}', 'GeneralController@getReviews');
    Route::get('teacher_subject/{userId}/{subjectId}', 'GeneralController@teacher_subject');
    Route::post('book', 'GeneralController@book');
    Route::post('review', 'GeneralController@review');
});

