<?php

Route::get('userLang/{lang}', function ($lang) {
    session()->put('user_lang', $lang);
    return redirect()->back();
})->where('lang', 'ar|en');

Route::get('terms', 'HomeController@terms');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
Route::get('/payment/{id}', 'SubjectRequestController@payment');
Route::get('/response', 'SubjectRequestController@response');
});
Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function () {
    Route::get('/', 'HomeController@index')->name('admin.home');
    Route::resource('lecture', 'LectureController');
    Route::get('subject-request', 'SubjectRequestController@index')->name('subject.request');



    Route::group(['middleware' => 'teacher'], function () {
        Route::get('review', 'HomeController@getReviews')->name('admin.review');
        Route::get('online', 'HomeController@online')->name('admin.online');
        Route::get('offline', 'HomeController@offline')->name('admin.offline');
        Route::get('subject-request/accept/{id}', 'SubjectRequestController@accept')->name('subject.accept');
        Route::get('subject-request/refuse/{id}', 'SubjectRequestController@refuse')->name('subject.refuse');

    });

    Route::group(['middleware' => 'admin'], function () {
        Route::match(['get', 'post'], '/setting', 'HomeController@setting')->name('admin.setting');
        Route::resource('student', 'StudentController');
        Route::resource('subject', 'SubjectController');
        Route::resource('education', 'EducationLevelController');
        Route::resource('slider', 'SliderController');
        Route::resource('grade', 'GradeController');
        Route::resource('relation', 'RelationController');
        Route::resource('religion', 'ReligionController');
        Route::resource('job', 'JobController');
        Route::get('student/{student}/active/', 'StudentController@active')->name('student.active');
        Route::get('student/{student}/inactive/', 'StudentController@inactive')->name('student.inactive');
        Route::resource('teacher', 'TeacherController');
        Route::resource('certificate', 'CertificateController');
        Route::resource('category', 'CategoryController');
        Route::resource('subcategory', 'SubCategoryController');
        Route::resource('package', 'PackageController');
        Route::get('teacher/{teacher}/active/', 'TeacherController@active')->name('teacher.active');
        Route::get('teacher/{teacher}/inactive/', 'TeacherController@inactive')->name('teacher.inactive');
        Route::group(['prefix' => 'landpage'], function () {
            Route::match(['get', 'post'], '/', 'LandPageController@index')->name('landpage.home');
            Route::match(['get', 'post'], '/feature', 'LandPageController@feature')->name('landpage.feature');
            Route::match(['get', 'post'], '/awesomefeature', 'LandPageController@awesomefeature')->name('landpage.awesomefeature');
            Route::match(['get', 'post'], '/about', 'LandPageController@about')->name('landpage.about');
            Route::match(['get', 'post'], '/general', 'LandPageController@general')->name('landpage.general');
            Route::get('/contact', 'LandPageController@contact')->name('landpage.contact');
            Route::get('/subscribe', 'LandPageController@subscribe')->name('landpage.subscribe');
            Route::resource('screenshot', 'ScreenshotController');
            Route::resource('team', 'TeamController');
            Route::resource('testimonials', 'TestimonialsController');
            Route::resource('faq', 'FaqController');
        });
    });
    Route::get('teacher/{teacher}/edit', 'TeacherController@edit')->name('teacher.edit');
    Route::put('teacher/{teacher}', 'TeacherController@update')->name('teacher.update');
});

Route::post('/ajax', 'AjaxController@index')->middleware('auth');
Route::get('/', 'HomeController');
Route::post('/subscribe', 'SubscribeController@subscribe');
Route::post('/contact', 'SubscribeController@contact');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');



